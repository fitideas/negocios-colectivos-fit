package co.com.accion.negocios;

import co.com.accion.negocios.dto.ClientesDtoFull;
import co.com.accion.negocios.dto.EventoDTO;
import co.com.accion.negocios.dto.PaginationDTO;
import co.com.accion.negocios.dto.ResponseMsg;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EventosByNegocioTests {

    private Gson gson = new Gson();

    /**
     * Verifica si el error producido al ingresar un codigo de negocio inválido es el esperado
     */
    @Test
    public void dadoCodigoNegocioMalFormateado_entoncesBadRequestYResponseMsg() {
        try {
            String codigoNegocioMalFormateado = "a587";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios/" + codigoNegocioMalFormateado + "/eventos");
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 400", HttpStatus.BAD_REQUEST.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4003", "AF4003", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un codigo de negocio inválido es el esperado
     */
    @Test
    public void dadoSeqEventoMalFormateado_entoncesBadRequestYResponseMsg() {
        try {
            String codigoNegocioMalFormateado = "32270";
            String seqEventoMalFormateado = "32a270";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios/" + codigoNegocioMalFormateado + "/eventos/" + seqEventoMalFormateado);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 400", HttpStatus.BAD_REQUEST.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4003", "AF4003", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un codigo de negocio inválido es el esperado
     */
    @Test
    public void dadoTipoEventoMalFormateado_entoncesBadRequestYResponseMsg() {
        try {
            String codigoNegocio = "32270";
            String tipoEvento = "3a";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios/" + codigoNegocio + "/eventos?tipoEvento="+tipoEvento);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 400", HttpStatus.BAD_REQUEST.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4003", "AF4003", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un codigo de negocio inválido es el esperado
     */
    @Test
    public void dadoNroDocumentoMalFormateado_entoncesBadRequestYResponseMsg() {
        try {
            String codigoNegocio = "32270";
            String nroDocumento = "3a";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios/" + codigoNegocio + "/eventos?nroDocumento="+nroDocumento);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 400", HttpStatus.BAD_REQUEST.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4003", "AF4003", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un codigo de negocio inexistente es el esperado
     */
    @Test
    public void dadoCodigoNegocioInexistente_entoncesNotFoundYResponseMsg() {
        try {
            String codigoNegocioInexistente = "6666666666666666";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios/" + codigoNegocioInexistente + "/eventos");
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 204", HttpStatus.NO_CONTENT.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un codigo de negocio inexistente es el esperado
     */
    @Test
    public void dadoSeqEventoInexistente_entoncesNotFoundYResponseMsg() {
        try {
            String codigoNegocio = "32270";
            String seqEvento = "666666666666666";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios/" + codigoNegocio + "/eventos/" + seqEvento);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 204", HttpStatus.NO_CONTENT.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un codigo de negocio inexistente es el esperado
     */
    @Test
    public void dadoTipoEventoInexistente_entoncesNotFoundYResponseMsg() {
        try {
            String codigoNegocio = "32270";
            String tipoEvento = "632";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios/" + codigoNegocio + "/eventos?tipoEvento="+tipoEvento);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 204", HttpStatus.NO_CONTENT.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un codigo de negocio inexistente es el esperado
     */
    @Test
    public void dadoNroDocumentoInexistente_entoncesNotFoundYResponseMsg() {
        try {
            String codigoNegocio = "32270";
            String nroDocumento = "63002";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios/" + codigoNegocio + "/eventos?nroDocumento="+nroDocumento);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 204", HttpStatus.NO_CONTENT.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código válido produce una lista de resultados
     */
    @Test
    public void dadoCodigoCorrecto_entoncesEventos() {
        try {
            String codigoNegocio = "32270";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios/" + codigoNegocio + "/eventos");
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<EventoDTO> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de eventos", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código válido produce una lista de resultados
     */
    @Test
    public void dadoSeqEventoCorrecto_entoncesEvento() {
        try {
            String codigoNegocio = "9979";
            String seqEvento = "66669";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios/" + codigoNegocio + "/eventos/" + seqEvento);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<EventoDTO> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de eventos", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código válido produce una lista de resultados
     */
    @Test
    public void dadoNroDocumentoCorrecto_entoncesEventos() {
        try {
            String codigoNegocio = "66787";
            String nroDocumento = "800108240";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios/" + codigoNegocio + "/eventos?nroDocumento="+nroDocumento);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<EventoDTO> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de eventos", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código válido produce una lista de resultados
     */
    @Test
    public void dadoCodigoYTipoEventoCorrecto_entoncesEventos() {
        try {
            String codigoNegocio = "32270";
            String tipoEvento = "3";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios/" + codigoNegocio + "/eventos?tipoEvento="+tipoEvento);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<EventoDTO> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de eventos", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código válido produce una lista de resultados
     */
    @Test
    public void dadoCodigoCorrectoPaginacion_entoncesEventosPaginacion() {
        try {
            String codigoNegocio = "32270";
            long start = 0;
            long limit = 2;
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios/" + codigoNegocio + "/eventos?start="+start+"&limit="+limit);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            PaginationDTO resultado = gson.fromJson(body, PaginationDTO.class);
            assertFalse("Se esperaba una lista no vacía de eventos", resultado.getResults().isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código válido produce una lista de resultados
     */
    @Test
    public void dadoCodigoYTipoEventoCorrectoPaginacion_entoncesEventosPaginacion() {
        try {
            String codigoNegocio = "32270";
            String tipoEvento = "3";
            long start = 0;
            long limit = 2;
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios/" + codigoNegocio + "/eventos?start="+start+"&limit="+limit+"&tipoEvento="+tipoEvento);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            PaginationDTO resultado = gson.fromJson(body, PaginationDTO.class);
            assertFalse("Se esperaba una lista no vacía de eventos", resultado.getResults().isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código válido produce una lista de resultados
     */
    @Test
    public void dadoCodigoYNroDocumentoCorrectoPaginacion_entoncesEventosPaginacion() {
        try {
            String codigoNegocio = "66787";
            String nroDocumento = "800108240";
            long start = 0;
            long limit = 2;
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios/" + codigoNegocio + "/eventos?start="+start+"&limit="+limit+"&nroDocumento="+nroDocumento);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            PaginationDTO resultado = gson.fromJson(body, PaginationDTO.class);
            assertFalse("Se esperaba una lista no vacía de eventos", resultado.getResults().isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

}
