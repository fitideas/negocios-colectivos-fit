package co.com.accion.negocios;

import co.com.accion.negocios.dto.ClientesDtoFull;
import co.com.accion.negocios.dto.EncargoDto;
import co.com.accion.negocios.dto.PaginationDTO;
import co.com.accion.negocios.dto.ResponseMsg;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest
public class VerificarEncargoBeneficiarioTests {

    private Gson gson = new Gson();

    /**
     * Verifica si el error producido al ingresar un codigo de negocio inválido es el esperado
     */
    @Test
    public void dadoNroEncargoMalFormateado_entoncesBadRequestYResponseMsg() {
        try {
            String nroEncargoMalFormateado = "41z92";
            String documentoFormateado = "6861717";
            String codigoNegocioFormateado = "36640";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/encargos/"+nroEncargoMalFormateado+"?codigoNegocio=" + codigoNegocioFormateado + "&documento=" + documentoFormateado);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 400", HttpStatus.BAD_REQUEST.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4003", "AF4003", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un codigo de negocio inválido es el esperado
     */
    @Test
    public void dadoDocumentoMalFormateado_entoncesBadRequestYResponseMsg() {
        try {
            String nroEncargoFormateado = "4192";
            String documentoMalFormateado = "686z1717";
            String codigoNegocioFormateado = "36640";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/encargos/"+nroEncargoFormateado+"?codigoNegocio=" + codigoNegocioFormateado + "&documento=" + documentoMalFormateado);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 400", HttpStatus.BAD_REQUEST.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4003", "AF4003", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un codigo de negocio inválido es el esperado
     */
    @Test
    public void dadoCodigoNegocioMalFormateado_entoncesBadRequestYResponseMsg() {
        try {
            String nroEncargoFormateado = "4192";
            String documentoFormateado = "6861717";
            String codigoNegocioMalFormateado = "36z640";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/encargos/"+nroEncargoFormateado+"?codigoNegocio=" + codigoNegocioMalFormateado + "&documento=" + documentoFormateado);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 400", HttpStatus.BAD_REQUEST.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4003", "AF4003", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un codigo de negocio inexistente es el esperado
     */
    @Test
    public void dadoNroEncargoInexistente_entoncesNotFoundYResponseMsg() {
        try {
            String nroEncargo = "6666666666666666";
            String documento = "6861717";
            String codigoNegocio = "36640";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/encargos/"+nroEncargo+"?codigoNegocio=" + codigoNegocio + "&documento=" + documento);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 204", HttpStatus.NO_CONTENT.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un codigo de negocio inexistente es el esperado
     */
    @Test
    public void dadoDocumentoInexistente_entoncesNotFoundYResponseMsg() {
        try {
            String nroEncargo = "4192";
            String documento = "6666666666666666";
            String codigoNegocio = "36640";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/encargos/"+nroEncargo+"?codigoNegocio=" + codigoNegocio + "&documento=" + documento);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 204", HttpStatus.NO_CONTENT.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un codigo de negocio inexistente es el esperado
     */
    @Test
    public void dadoCodigoNegocioInexistente_entoncesNotFoundYResponseMsg() {
        try {
            String nroEncargo = "4192";
            String documento = "6861717";
            String codigoNegocio = "6666666666666666";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/encargos/"+nroEncargo+"?codigoNegocio=" + codigoNegocio + "&documento=" + documento);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 204", HttpStatus.NO_CONTENT.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código válido produce un resultado
     */
    @Test
    public void dadoParametrosCorrecto_entoncesEncargo() {
        try {
            String nroEncargo = "4192";
            String documento = "6861717";
            String codigoNegocio = "36640";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/encargos/"+nroEncargo+"?codigoNegocio=" + codigoNegocio + "&documento=" + documento);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            System.out.println(body);
            EncargoDto resultados = gson.fromJson(body, EncargoDto.class);
            assertFalse("Se esperaba un resultado no nulo", resultados==null);
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

}
