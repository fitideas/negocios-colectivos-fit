package co.com.accion.negocios;

import co.com.accion.negocios.dto.ClientesDtoFull;
import co.com.accion.negocios.dto.PaginationDTO;
import co.com.accion.negocios.dto.ResponseMsg;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HistorialBeneficiariosByNegocioTests {

    private Gson gson = new Gson();

    /**
     * Verifica si el error producido al ingresar un codigo de negocio inválido es el esperado
     */
    @Test
    public void dadoCodigoNegocioMalFormateado_entoncesBadRequestYResponseMsg() {
        try {
            String codigoNegocioMalFormateado = "a587";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios/" + codigoNegocioMalFormateado + "/historialBeneficiarios");
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 400", HttpStatus.BAD_REQUEST.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4003", "AF4003", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un codigo de negocio inexistente es el esperado
     */
    @Test
    public void dadoCodigoNegocioInexistente_entoncesNotFoundYResponseMsg() {
        try {
            String codigoNegocioInexistente = "6666666666666666";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios/" + codigoNegocioInexistente + "/historialBeneficiarios");
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 204", HttpStatus.NO_CONTENT.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código válido produce una lista de resultados
     */
    @Test
    public void dadoCodigoCorrecto_entoncesBeneficiarios() {
        try {
            String codigoNegocio = "3";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios/" + codigoNegocio + "/historialBeneficiarios");
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<ClientesDtoFull> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de historialBeneficiarios", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código válido produce una lista de resultados
     */
    @Test
    public void dadoCodigoCorrectoPaginacion_entoncesBeneficiariosPaginacion() {
        try {
            String codigoNegocio = "3";
            long start = 0;
            long limit = 2;
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios/" + codigoNegocio + "/historialBeneficiarios?start="+start+"&limit="+limit);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            PaginationDTO resultado = gson.fromJson(body, PaginationDTO.class);
            assertFalse("Se esperaba una lista no vacía de historialBeneficiarios", resultado.getResults().isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

}
