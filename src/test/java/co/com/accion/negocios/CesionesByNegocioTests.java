package co.com.accion.negocios;

import co.com.accion.negocios.dto.ClientesDtoFull;
import co.com.accion.negocios.dto.PaginationDTO;
import co.com.accion.negocios.dto.ResponseMsg;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CesionesByNegocioTests {

    private Gson gson = new Gson();

    /**
     * Verifica si el error producido al ingresar un codigo de negocio inválido es el esperado
     */
    @Test
    public void dadoCodigoNegocioMalFormateado_entoncesBadRequestYResponseMsg() {
        try {
            String codigoNegocioMalFormateado = "a587";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/cesiones?codigoNegocio=" + codigoNegocioMalFormateado );
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 400", HttpStatus.BAD_REQUEST.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4003", "AF4003", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un identificación inválido es el esperado
     */
    @Test
    public void dadoIdentificacionMalFormateado_entoncesBadRequestYResponseMsg() {
        try {
            String codigoNegocio = "39339";
            String identificacionMalFormateada = "900762197z";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/cesiones?codigoNegocio=" + codigoNegocio + "&identificacion=" + identificacionMalFormateada);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 400", HttpStatus.BAD_REQUEST.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4003", "AF4003", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un identificación inválido es el esperado
     */
    @Test
    public void dadoFechaMalFormateado_entoncesBadRequestYResponseMsg() {
        try {
            String codigoNegocio = "39339";
            String startDate = "01-02-19998";
            String endDate = "01-02-19999";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/cesiones?codigoNegocio=" + codigoNegocio + "&startDate=" + startDate +"&endDate=" + endDate);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 500", HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF5004", "AF5004", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un codigo de negocio inexistente es el esperado
     */
    @Test
    public void dadoCodigoNegocioInexistente_entoncesNotFoundYResponseMsg() {
        try {
            String codigoNegocioInexistente = "6666666666666666";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/cesiones?codigoNegocio=" + codigoNegocioInexistente );
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 204", HttpStatus.NO_CONTENT.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un identificación inexistente es el esperado
     */
    @Test
    public void dadoIdentificacionInexistente_entoncesNotFoundYResponseMsg() {
        try {
            String identificacionInexistente = "6666666666666666";
            String codigoNegocio = "39339";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/cesiones?codigoNegocio=" + codigoNegocio + "&identificacion=" + identificacionInexistente );
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 204", HttpStatus.NO_CONTENT.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un identificación inexistente es el esperado
     */
    @Test
    public void dadoRangoFechaInexistente_entoncesNotFoundYResponseMsg() {
        try {
            String codigoNegocio = "39339";
            String startDate = "01-02-1998";
            String endDate = "02-02-1998";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/cesiones?codigoNegocio=" + codigoNegocio + "&startDate=" + startDate +"&endDate=" + endDate);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 204", HttpStatus.NO_CONTENT.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código válido produce una lista de resultados
     */
    @Test
    public void dadoCodigoCorrecto_entoncesCesiones() {
        try {
            String codigoNegocio = "39339";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/cesiones?codigoNegocio=" + codigoNegocio );
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<ClientesDtoFull> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de cesiones", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código válido produce una lista de resultados
     */
    @Test
    public void dadoCodigoCorrectoYFecha_entoncesCesiones() {
        try {
            String codigoNegocio = "39339";
            String startDate = "01-01-2014";
            String endDate = "31-12-2019";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/cesiones?codigoNegocio=" + codigoNegocio + "&startDate=" + startDate +"&endDate=" + endDate);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<ClientesDtoFull> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de cesiones", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código e identificación válido produce una lista de resultados
     */
    @Test
    public void dadoCodigoYIdentificacionCorrecto_entoncesCesiones() {
        try {
            String codigoNegocio = "39339";
            String identificacion = "900762197";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/cesiones?codigoNegocio=" + codigoNegocio + "&identificacion=" + identificacion);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<ClientesDtoFull> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de cesiones", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código e identificación válido produce una lista de resultados
     */
    @Test
    public void dadoCodigoYIdentificacionYFechaCorrecto_entoncesCesiones() {
        try {
            String codigoNegocio = "39339";
            String identificacion = "900762197";
            String startDate = "01-01-2014";
            String endDate = "31-12-2019";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/cesiones?codigoNegocio=" + codigoNegocio + "&identificacion=" + identificacion + "&startDate=" + startDate +"&endDate=" + endDate);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<ClientesDtoFull> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de cesiones", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código válido produce una lista de resultados paginados
     */
    @Test
    public void dadoCodigoCorrectoPaginacion_entoncesCesionesPaginacion() {
        try {
            String codigoNegocio = "39339";
            long start = 0;
            long limit = 2;
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/cesiones?codigoNegocio=" + codigoNegocio + "&start="+start+"&limit="+limit);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            PaginationDTO resultado = gson.fromJson(body, PaginationDTO.class);
            assertFalse("Se esperaba una lista no vacía de cesiones", resultado.getResults().isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código válido produce una lista de resultados paginados
     */
    @Test
    public void dadoCodigoYFechaCorrectoPaginacion_entoncesCesionesPaginacion() {
        try {
            String codigoNegocio = "39339";
            String startDate = "01-01-2014";
            String endDate = "31-12-2019";
            long start = 0;
            long limit = 2;
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/cesiones?codigoNegocio=" + codigoNegocio + "&start="+start+"&limit="+limit + "&startDate=" + startDate +"&endDate=" + endDate);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            PaginationDTO resultado = gson.fromJson(body, PaginationDTO.class);
            assertFalse("Se esperaba una lista no vacía de cesiones", resultado.getResults().isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código e identificación válido produce una lista de resultados paginados
     */
    @Test
    public void dadoCodigoYIdentificacionCorrectoPaginacion_entoncesCesionesPaginacion() {
        try {
            String codigoNegocio = "39339";
            String identificacion = "900762197";
            long start = 0;
            long limit = 2;
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/cesiones?codigoNegocio=" + codigoNegocio + "&identificacion=" + identificacion + "&start=" + start + "&limit=" + limit);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<ClientesDtoFull> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de cesiones", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código e identificación válido produce una lista de resultados paginados
     */
    @Test
    public void dadoCodigoYIdentificacionYFechaCorrectoPaginacion_entoncesCesionesPaginacion() {
        try {
            String codigoNegocio = "39339";
            String identificacion = "900762197";
            String startDate = "01-01-2014";
            String endDate = "31-12-2019";
            long start = 0;
            long limit = 2;
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/cesiones?codigoNegocio=" + codigoNegocio + "&identificacion=" + identificacion + "&start=" + start + "&limit=" + limit + "&startDate=" + startDate +"&endDate=" + endDate);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<ClientesDtoFull> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de cesiones", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

}
