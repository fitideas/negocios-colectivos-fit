package co.com.accion.negocios;

import co.com.accion.negocios.dto.ClientesDtoFull;
import co.com.accion.negocios.dto.PaginationDTO;
import co.com.accion.negocios.dto.ResponseMsg;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BuscarNegocioTests {

    private Gson gson = new Gson();

    /**
     * Verifica si el error producido al ingresar un codigo de negocio inválido es el esperado
     */
    @Test
    public void dadoCodigoNegocioMalFormateado_entoncesBadRequestYResponseMsg() {
        try {
            String codigoNegocioMalFormateado = "a587";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios?codigoNegocio="+codigoNegocioMalFormateado);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 400", HttpStatus.BAD_REQUEST.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4003", "AF4003", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un codigo de negocio inexistente es el esperado
     */
    @Test
    public void dadoCodigoNegocioInexistente_entoncesNotFoundYResponseMsg() {
        try {
            String codigoNegocioInexistente = "6666666666666666";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios?codigoNegocio=" + codigoNegocioInexistente);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 204", HttpStatus.NO_CONTENT.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código válido produce una lista de resultados
     */
    @Test
    public void dadoCodigoCorrecto_entoncesNegocios() {
        try {
            String codigoNegocio = "518";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios?codigoNegocio=" + codigoNegocio );
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<ClientesDtoFull> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de negocios", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un codigo de negocio inexistente es el esperado
     */
    @Test
    public void dadoCodigoNegocioInexistenteExactMatch_entoncesNotFoundYResponseMsg() {
        try {
            String codigoNegocioInexistente = "6666666666666666";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios?codigoNegocio=" + codigoNegocioInexistente + "&exactMatch=true");
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 204", HttpStatus.NO_CONTENT.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código válido produce una lista de resultados
     */
    @Test
    public void dadoCodigoCorrectoExactMatch_entoncesNegocios() {
        try {
            String codigoNegocio = "518";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios?codigoNegocio=" + codigoNegocio + "&exactMatch=true");
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<ClientesDtoFull> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de negocios", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código válido produce una lista de resultados paginados
     */
    @Test
    public void dadoCodigoCorrectoPaginacion_entoncesNegociosPaginacion() {
        try {
            String codigoNegocio = "518";
            long start = 0;
            long limit = 2;
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios?codigoNegocio=" + codigoNegocio + "&start="+start+"&limit="+limit);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            PaginationDTO resultado = gson.fromJson(body, PaginationDTO.class);
            assertFalse("Se esperaba una lista no vacía de negocios", resultado.getResults().isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un nombre de negocio inexistente es el esperado
     */
    @Test
    public void dadoNombreInexistente_entoncesNotFoundYResponseMsg() {
        try {
            String nombreInexistente = "NOMBREINEXISTENTE";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios?nombreNegocio=" + nombreInexistente);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 204", HttpStatus.NO_CONTENT.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un nombre válido produce una lista de resultados
     */
    @Test
    public void dadoNombreCorrecto_entoncesNegocios() {
        try {
            String nombre = "industrial";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios?nombreNegocio=" + nombre );
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<ClientesDtoFull> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de negocios", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un nombre de negocio inexistente es el esperado
     */
    @Test
    public void dadoNombreInexistenteExactMatch_entoncesNotFoundYResponseMsg() {
        try {
            String nombreInexistente = "NOMBREINEXISTENTE";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios?nombreNegocio=" + nombreInexistente + "&exactMatch=true");
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 204", HttpStatus.NO_CONTENT.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un nombre válido produce una lista de resultados
     */
    @Test
    public void dadoNombreCorrectoExactMatch_entoncesNegocios() {
        try {
            String nombre = "OIKOS%20PARQUE%20INDUSTRIAL%20DE%20OCCIDENTE%20BODEGAS%20COLMENA";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios?nombreNegocio=" + nombre + "&exactMatch=true");
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<ClientesDtoFull> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de negocios", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un nombre válido produce una lista de resultados
     */
    @Test
    public void dadoNombreCorrectoPaginacion_entoncesNegociosPaginacion() {
        try {
            String nombre = "industrial";
            long start = 0;
            long limit = 2;
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios?nombreNegocio=" + nombre + "&start="+start+"&limit="+limit);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            PaginationDTO resultado = gson.fromJson(body, PaginationDTO.class);
            assertFalse("Se esperaba una lista no vacía de negocios", resultado.getResults().isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un codigo o nombre de negocio inexistente es el esperado
     */
    @Test
    public void dadoCodigoYNombreInexistente_entoncesNotFoundYResponseMsg() {
        try {
            String nombreInexistente = "NOMBREINEXISTENTE";
            String codigoInexistente = "6666666666666666";
            String nombreExiste = "industrial";
            String codigoExiste = "1";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios?nombreNegocio=" + nombreInexistente + "&codigoNegocio=" + codigoExiste);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 204", HttpStatus.NO_CONTENT.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
            request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios?nombreNegocio=" + nombreExiste + "&codigoNegocio=" + codigoInexistente);
            response = HttpClientBuilder.create().build().execute(request);
            body = EntityUtils.toString(response.getEntity());
            resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 404", HttpStatus.NOT_FOUND.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());

        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código y nombre válido produce una lista de resultados
     */
    @Test
    public void dadoCodigoYNombreCorrecto_entoncesNegocios() {
        try {
            String nombreExiste = "industrial";
            String codigoExiste = "1";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios?nombreNegocio=" + nombreExiste + "&codigoNegocio=" + codigoExiste);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<ClientesDtoFull> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de negocios", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un codigo o nombre de negocio inexistente es el esperado con ExactMatch
     */
    @Test
    public void dadoCodigoYNombreInexistenteExactMatch_entoncesNotFoundYResponseMsg() {
        try {
            String codigoExiste = "10347";
            String codigoInexistente = "6666666666666666";
            String nombreInexistente = "NOMBREINEXISTENTE";
            String nombreExiste = "OIKOS%20PARQUE%20INDUSTRIAL%20DE%20OCCIDENTE%20BODEGAS%20COLMENA";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios?nombreNegocio=" + nombreInexistente + "&codigoNegocio=" + codigoExiste + "&exactMatch=true");
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 204", HttpStatus.NO_CONTENT.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
            request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios?nombreNegocio=" + nombreExiste + "&codigoInexistente=" + codigoExiste + "&exactMatch=true");
            response = HttpClientBuilder.create().build().execute(request);
            body = EntityUtils.toString(response.getEntity());
            resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 404", HttpStatus.NOT_FOUND.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código y nombre válido produce un resultado con ExactMatch
     */
    @Test
    public void dadoCodigoYNombreCorrectoExactMatch_entoncesNegocios() {
        try {
            String codigo = "10347";
            String nombre = "OIKOS%20PARQUE%20INDUSTRIAL%20DE%20OCCIDENTE%20BODEGAS%20COLMENA";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios?nombreNegocio=" + nombre + "&exactMatch=true" + codigo + "&exactMatch=true");
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<ClientesDtoFull> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de negocios", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código y nombre válido produce una lista de resultados paginados
     */
    @Test
    public void dadoCodigoYNombreCorrectoPaginacion_entoncesNegociosPaginacion() {
        try {
            long start = 0;
            long limit = 2;
            String nombreExiste = "industrial";
            String codigoExiste = "1";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/negocios?nombreNegocio=" + nombreExiste + "&codigoNegocio=" + codigoExiste + "&start="+start+"&limit="+limit);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            PaginationDTO resultado = gson.fromJson(body, PaginationDTO.class);
            assertFalse("Se esperaba una lista no vacía de negocios", resultado.getResults().isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }


}
