package co.com.accion.negocios;

import co.com.accion.negocios.dto.ClientesDtoFull;
import co.com.accion.negocios.dto.PaginationDTO;
import co.com.accion.negocios.dto.ResponseMsg;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BuscarClienteTests {

    private Gson gson = new Gson();

    /**
     * Verifica si el error producido al ingresar un identificacion de negocio inválido es el esperado
     */
    @Test
    public void dadoIdentificacionMalFormateado_entoncesBadRequestYResponseMsg() {
        try {
            String identificacionMalFormateado = "a587";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/clientes?identificacion="+identificacionMalFormateado);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 400", HttpStatus.BAD_REQUEST.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4003", "AF4003", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un identificacion de negocio inexistente es el esperado
     */
    @Test
    public void dadoIdentificacionInexistente_entoncesNotFoundYResponseMsg() {
        try {
            String identificacionInexistente = "6666666666666666";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/clientes?identificacion=" + identificacionInexistente);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 204", HttpStatus.NO_CONTENT.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código válido produce una lista de resultados
     */
    @Test
    public void dadoIdentificacionCorrecto_entoncesClientes() {
        try {
            String identificacion = "805031";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/clientes?identificacion=" + identificacion );
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<ClientesDtoFull> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de clientes", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un identificacion de negocio inexistente es el esperado
     */
    @Test
    public void dadoIdentificacionInexistenteExactMatch_entoncesNotFoundYResponseMsg() {
        try {
            String identificacionInexistente = "6666666666666666";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/clientes?identificacion=" + identificacionInexistente + "&exactMatch=true");
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 204", HttpStatus.NO_CONTENT.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código válido produce una lista de resultados
     */
    @Test
    public void dadoIdentificacionCorrectoExactMatch_entoncesClientes() {
        try {
            String identificacion = "80503126";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/clientes?identificacion=" + identificacion + "&exactMatch=true");
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<ClientesDtoFull> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de clientes", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código válido produce una lista de resultados paginados
     */
    @Test
    public void dadoIdentificacionCorrectoPaginacion_entoncesClientesPaginacion() {
        try {
            String identificacion = "805";
            long start = 0;
            long limit = 2;
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/clientes?identificacion=" + identificacion + "&start="+start+"&limit="+limit);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            PaginationDTO resultado = gson.fromJson(body, PaginationDTO.class);
            assertFalse("Se esperaba una lista no vacía de clientes", resultado.getResults().isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un nombre de negocio inexistente es el esperado
     */
    @Test
    public void dadoNombreInexistente_entoncesNotFoundYResponseMsg() {
        try {
            String nombreInexistente = "NOMBREINEXISTENTE";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/clientes?nombre=" + nombreInexistente);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 204", HttpStatus.NO_CONTENT.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un nombre válido produce una lista de resultados
     */
    @Test
    public void dadoNombreCorrecto_entoncesClientes() {
        try {
            String nombre = "GRISALES%20MEDINA";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/clientes?nombre=" + nombre );
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<ClientesDtoFull> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de clientes", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un nombre de negocio inexistente es el esperado
     */
    @Test
    public void dadoNombreInexistenteExactMatch_entoncesNotFoundYResponseMsg() {
        try {
            String nombreInexistente = "NOMBREINEXISTENTE";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/clientes?nombre=" + nombreInexistente + "&exactMatch=true");
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 204", HttpStatus.NO_CONTENT.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un nombre válido produce una lista de resultados
     */
    @Test
    public void dadoNombreCorrectoExactMatch_entoncesClientes() {
        try {
            String nombre = "GRISALES%20MEDINA%20JAIRO%20ANDRES";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/clientes?nombre=" + nombre + "&exactMatch=true");
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<ClientesDtoFull> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de clientes", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un nombre válido produce una lista de resultados
     */
    @Test
    public void dadoNombreCorrectoPaginacion_entoncesClientesPaginacion() {
        try {
            String nombre = "Andres";
            long start = 0;
            long limit = 2;
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/clientes?nombre=" + nombre + "&start="+start+"&limit="+limit);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            PaginationDTO resultado = gson.fromJson(body, PaginationDTO.class);
            assertFalse("Se esperaba una lista no vacía de clientes", resultado.getResults().isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un identificacion o nombre de negocio inexistente es el esperado
     */
    @Test
    public void dadoIdentificacionYNombreInexistente_entoncesNotFoundYResponseMsg() {
        try {
            String nombreInexistente = "NOMBREINEXISTENTE";
            String identificacionInexistente = "6666666666666666";
            String nombreExiste = "Andres";
            String identificacionExiste = "8050";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/clientes?nombre=" + nombreInexistente + "&identificacion=" + identificacionExiste);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 404", HttpStatus.NOT_FOUND.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
            request = new HttpGet("http://localhost:8080/negocios-colectivos/api/clientes?nombre=" + nombreExiste + "&identificacion=" + identificacionInexistente);
            response = HttpClientBuilder.create().build().execute(request);
            body = EntityUtils.toString(response.getEntity());
            resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 404", HttpStatus.NOT_FOUND.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());

        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código y nombre válido produce una lista de resultados
     */
    @Test
    public void dadoIdentificacionYNombreCorrecto_entoncesClientes() {
        try {
            String nombreExiste = "Andres";
            String identificacionExiste = "8050";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/clientes?nombre=" + nombreExiste + "&identificacion=" + identificacionExiste);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<ClientesDtoFull> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de clientes", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si el error producido al ingresar un identificacion o nombre de negocio inexistente es el esperado con ExactMatch
     */
    @Test
    public void dadoIdentificacionYNombreInexistenteExactMatch_entoncesNotFoundYResponseMsg() {
        try {
            String identificacionExiste = "80503126";
            String identificacionInexistente = "6666666666666666";
            String nombreInexistente = "NOMBREINEXISTENTE";
            String nombreExiste = "GRISALES%20MEDINA%20JAIRO%20ANDRES";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/clientes?nombre=" + nombreInexistente + "&identificacion=" + identificacionExiste + "&exactMatch=true");
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            ResponseMsg resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 404", HttpStatus.NOT_FOUND.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
            request = new HttpGet("http://localhost:8080/negocios-colectivos/api/clientes?nombre=" + nombreExiste + "&identificacionInexistente=" + identificacionInexistente + "&exactMatch=true");
            response = HttpClientBuilder.create().build().execute(request);
            body = EntityUtils.toString(response.getEntity());
            resMsg = gson.fromJson(body, ResponseMsg.class);
            assertEquals("El código HTTP esperado era 404", HttpStatus.NOT_FOUND.value(), response.getStatusLine().getStatusCode());
            assertEquals("El código de error esperado era AF4001", "AF4001", resMsg.getCode());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código y nombre válido produce un resultado con ExactMatch
     */
    @Test
    public void dadoIdentificacionYNombreCorrectoExactMatch_entoncesClientes() {
        try {
            String identificacion = "80503126";
            String nombre = "GRISALES%20MEDINA%20JAIRO%20ANDRES";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/clientes?nombre=" + nombre + "&exactMatch=true" + identificacion + "&exactMatch=true");
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            List<ClientesDtoFull> resultados = gson.fromJson(body, List.class);
            assertFalse("Se esperaba una lista no vacía de clientes", resultados.isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si un código y nombre válido produce una lista de resultados paginados
     */
    @Test
    public void dadoIdentificacionYNombreCorrectoPaginacion_entoncesClientesPaginacion() {
        try {
            long start = 0;
            long limit = 2;
            String nombreExiste = "Andres";
            String identificacionExiste = "8050";
            HttpUriRequest request = new HttpGet("http://localhost:8080/negocios-colectivos/api/clientes?nombre=" + nombreExiste + "&identificacion=" + identificacionExiste + "&start="+start+"&limit="+limit);
            HttpResponse response = HttpClientBuilder.create().build().execute(request);
            String body = EntityUtils.toString(response.getEntity());
            PaginationDTO resultado = gson.fromJson(body, PaginationDTO.class);
            assertFalse("Se esperaba una lista no vacía de clientes", resultado.getResults().isEmpty());
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }


}
