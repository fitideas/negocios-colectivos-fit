package co.com.accion.negocios.repository;

import co.com.accion.negocios.dto.TipoEventoDto;
import co.com.accion.negocios.entities.AdmLibroAnotacionesEntity;
import co.com.accion.negocios.entities.AdmLibroAnotacionesEntityPK;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventosRepository extends PagingAndSortingRepository<AdmLibroAnotacionesEntity, AdmLibroAnotacionesEntityPK> {

    @Query(value = "SELECT * from ADM_LIBRO_ANOTACIONES where CODIGO_FIDEICOMISO = :codigo and TIPO_EVENTO in (18,19)", nativeQuery = true)
    Page<AdmLibroAnotacionesEntity> findAllByCodigoFideicomiso(Long codigo, Pageable page);

    @Query(value = "SELECT * from ADM_LIBRO_ANOTACIONES where CODIGO_FIDEICOMISO = :codigo AND NRO_DOCUMENTO = :nroDoc and TIPO_EVENTO in (18,19)", nativeQuery = true)
    Page<AdmLibroAnotacionesEntity> findAllByCodigoFideicomisoAndNroDocumento(Long codigo, Long nroDoc, Pageable page);

    @Query(value = "SELECT * from ADM_LIBRO_ANOTACIONES where CODIGO_FIDEICOMISO = :codigo AND TIPO_EVENTO = :tipo and TIPO_EVENTO in (18,19)", nativeQuery = true)
    Page<AdmLibroAnotacionesEntity> findAllByCodigoFideicomisoAndTipoEvento(Long codigo, Long tipo, Pageable page);

    @Query(value = "SELECT * from ADM_LIBRO_ANOTACIONES where CODIGO_FIDEICOMISO = :codigo AND TIPO_EVENTO = :tipo AND NRO_DOCUMENTO = :nroDoc and TIPO_EVENTO in (18,19)", nativeQuery = true)
    Page<AdmLibroAnotacionesEntity> findAllByCodigoFideicomisoAndTipoEventoAndNroDocumento(Long codigo, Long tipo, Long nroDoc, Pageable page);

    @Query(value = "SELECT * from ADM_LIBRO_ANOTACIONES where CODIGO_FIDEICOMISO = :codigo AND SEQ_EVENTO = :seqEvento AND ROWNUM = 1 and TIPO_EVENTO in (18,19)", nativeQuery = true)
    AdmLibroAnotacionesEntity findByCodigoFideicomisoAndSeqEvento(Long codigo, Long seqEvento);

    @Query(value = "SELECT NOMBRE AS nombre, TIPO_CALCULO_COMISION AS tipoCalculoComision, CLASE_EVENTO AS claseEvento FROM ADM_TIPOS_EVENTOS WHERE CODIGO = :codigo", nativeQuery = true)
    TipoEventoDto findTipoByCodigo(Long codigo);
}
