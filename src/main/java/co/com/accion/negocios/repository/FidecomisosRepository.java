package co.com.accion.negocios.repository;

import co.com.accion.negocios.dto.*;
import co.com.accion.negocios.entities.FideicomisosEntity;
import co.com.accion.negocios.entities.InmEncargosProyEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface FidecomisosRepository extends PagingAndSortingRepository<FideicomisosEntity, Long> {
    @Query(value = "SELECT * from FIDEICOMISOS where REGEXP_LIKE(NOMBRE_FIDEICOMISO, :nombreFideicomiso, 'i') AND CODIGO LIKE :codigo%", nativeQuery = true)
    Page<FideicomisosEntity> findByCodigoIsLikeAndNombreFideicomisoIsLike(Long codigo, String nombreFideicomiso, Pageable page);
    @Query(value = "SELECT * from FIDEICOMISOS where UPPER(NOMBRE_FIDEICOMISO) = UPPER(:nombreFideicomiso) AND CODIGO = :codigo", nativeQuery = true)
    Page<FideicomisosEntity> findByCodigoAndNombreFideicomiso(Long codigo, String nombreFideicomiso, Pageable page);
    @Query(value = "SELECT * from FIDEICOMISOS where CODIGO LIKE :codigo%", nativeQuery = true)
    Page<FideicomisosEntity> findByCodigoIsLike(Long codigo, Pageable page);
    @Query(value = "SELECT * from FIDEICOMISOS where REGEXP_LIKE(NOMBRE_FIDEICOMISO, :nombreFideicomiso, 'i')", nativeQuery = true)
    Page<FideicomisosEntity> findByNombreFideicomisoIsLike(String nombreFideicomiso, Pageable page);
    @Query(value = "SELECT * from FIDEICOMISOS where CODIGO = :codigo", nativeQuery = true)
    List<FideicomisosEntity> findByCodigo(Long codigo);
    @Query(value = "SELECT * from FIDEICOMISOS where UPPER(NOMBRE_FIDEICOMISO) = UPPER(:nombreFideicomiso)", nativeQuery = true)
    Page<FideicomisosEntity> findByNombreFideicomiso(String nombreFideicomiso, Pageable page);
    @Query(value ="SELECT F.nombre_fideicomiso AS nombreFideicomiso, F.codigo AS codigo, F.estado_fideicomiso AS estadoFideicomiso, B.porce_participa AS participacion " +
            "FROM BENEFICIARIO B JOIN CLIENTES C ON B.NIT_BENEFICIARIO = C.NIT JOIN FIDEICOMISOS F ON F.CODIGO = B.CODIGO_FIDEICOMISO\n" +
            "WHERE C.NIT = :idCliente",countQuery = "select count (F.codigo) FROM BENEFICIARIO B JOIN CLIENTES C ON B.NIT_BENEFICIARIO = C.NIT JOIN FIDEICOMISOS F ON F.CODIGO = B.CODIGO_FIDEICOMISO WHERE C.NIT = :idCliente ", nativeQuery = true)
    Page<NegocioDerechosDto> getNegociosByBeneficiario(Long idCliente, Pageable pageable);

    @Query(value = "(SELECT T.TIPO_RESPONSABLE AS tipoResponsable, RES.ID_RESPONSABLE AS idResponsable, RES.CODIGO_FIDEICOMISO AS codigoFideicomiso\n" +
            "FROM ADM_FIDEICOMISO_RESPONSABLES RES JOIN ADM_TIPOS_RESPONSABLES_FIDE T\n" +
            "ON RES.CODIGO_TIPO_RESPONSABLE = T.CODIGO_TIPO_RESPONSABLE\n" +
            "WHERE T.CODIGO_TIPO_RESPONSABLE <> 0 AND RES.CODIGO_FIDEICOMISO = :codigo\n" +
            ")\n" +
            "UNION\n" +
            "(\n" +
            "SELECT T.TIPO_RESPONSABLE AS tipoResponsable, RES.ID_RESPONSABLE AS idResponsable, RES.CODIGO_FIDEICOMISO AS codigoFideicomiso\n" +
            "FROM ADM_FIDEICOMISO_RESPONSABLES RES JOIN ADM_RESPONSABLES_GENERALES G ON G.ID_RESPONSABLE = RES.ID_RESPONSABLE JOIN ADM_TIPOS_RESPONSABLES_FIDE T\n" +
            "ON G.CODIGO_TIPO_RESPONSABLE = T.CODIGO_TIPO_RESPONSABLE\n" +
            "WHERE T.CODIGO_TIPO_RESPONSABLE <> 0 AND RES.CODIGO_FIDEICOMISO = :codigo\n" +
            ") UNION\n" +
            "(\n" +
            "SELECT T.TIPO_RESPONSABLE AS tipoResponsable, RES.ID_RESPONSABLE AS idResponsable, RES.CODIGO_FIDEICOMISO AS codigoFideicomiso\n" +
            "FROM ADM_FIDEICOMISO_RESPONSABLES RES JOIN ADM_RESPONSABLES_OFICINA O ON O.ID_RESPONSABLE = RES.ID_RESPONSABLE JOIN ADM_TIPOS_RESPONSABLES_FIDE T\n" +
            "ON O.CODIGO_TIPO_RESPONSABLE = T.CODIGO_TIPO_RESPONSABLE\n" +
            "WHERE T.CODIGO_TIPO_RESPONSABLE <> 0 AND RES.CODIGO_FIDEICOMISO = :codigo\n" +
            ")\n" +
            "union\n" +
            "SELECT 'ADMINISTRADOR' as tipoResponsable, NIT_FUNCIONARIO as idResponsable, codigo as codigoFideicomiso\n" +
            "FROM FIDEICOMISOS WHERE CODIGO = :codigo",
            countQuery = "SELECT COUNT(*) FROM((SELECT T.TIPO_RESPONSABLE AS tipoResponsable, RES.ID_RESPONSABLE AS idResponsable, RES.CODIGO_FIDEICOMISO AS codigoFideicomiso\n" +
                    "FROM ADM_FIDEICOMISO_RESPONSABLES RES JOIN ADM_TIPOS_RESPONSABLES_FIDE T\n" +
                    "ON RES.CODIGO_TIPO_RESPONSABLE = T.CODIGO_TIPO_RESPONSABLE\n" +
                    "WHERE T.CODIGO_TIPO_RESPONSABLE <> 0 AND RES.CODIGO_FIDEICOMISO = :codigo\n" +
                    ")\n" +
                    "UNION\n" +
                    "(\n" +
                    "SELECT T.TIPO_RESPONSABLE AS tipoResponsable, RES.ID_RESPONSABLE AS idResponsable, RES.CODIGO_FIDEICOMISO AS codigoFideicomiso\n" +
                    "FROM ADM_FIDEICOMISO_RESPONSABLES RES JOIN ADM_RESPONSABLES_GENERALES G ON G.ID_RESPONSABLE = RES.ID_RESPONSABLE JOIN ADM_TIPOS_RESPONSABLES_FIDE T\n" +
                    "ON G.CODIGO_TIPO_RESPONSABLE = T.CODIGO_TIPO_RESPONSABLE\n" +
                    "WHERE T.CODIGO_TIPO_RESPONSABLE <> 0 AND RES.CODIGO_FIDEICOMISO = :codigo\n" +
                    ") UNION\n" +
                    "(\n" +
                    "SELECT T.TIPO_RESPONSABLE AS tipoResponsable, RES.ID_RESPONSABLE AS idResponsable, RES.CODIGO_FIDEICOMISO AS codigoFideicomiso\n" +
                    "FROM ADM_FIDEICOMISO_RESPONSABLES RES JOIN ADM_RESPONSABLES_OFICINA O ON O.ID_RESPONSABLE = RES.ID_RESPONSABLE JOIN ADM_TIPOS_RESPONSABLES_FIDE T\n" +
                    "ON O.CODIGO_TIPO_RESPONSABLE = T.CODIGO_TIPO_RESPONSABLE\n" +
                    "WHERE T.CODIGO_TIPO_RESPONSABLE <> 0 AND RES.CODIGO_FIDEICOMISO = :codigo\n" +
                    ")\n" +
                    "union\n" +
                    "SELECT 'ADMINISTRADOR' as tipoResponsable, NIT_FUNCIONARIO as idResponsable, codigo as codigoFideicomiso\n" +
                    "FROM FIDEICOMISOS WHERE CODIGO = :codigo)",nativeQuery = true)
    Page<ResponsableFideicomisoDto> getResponsablesByCodigoFideicomiso (Long codigo, Pageable pageable);

    @Query(value = "SELECT B.CODIGO_FIDEICOMISO AS codigoFideicomiso, B.NIT_BENEFICIARIO AS numeroDocumento, F.NOMBRE_FIDEICOMISO AS nombreFideicomiso FROM BENEFICIARIO B " +
            "JOIN FIDEICOMISOS F ON B.CODIGO_FIDEICOMISO = F.CODIGO WHERE B.NIT_BENEFICIARIO = :nroDocumento " +
            "AND ROWNUM = 1", nativeQuery = true)
    BeneficiarioDto findNegocioByBeneficiario (Long nroDocumento);


    @Query(value= "select x.numeroEncargo as numeroEncargo, IP.NOMBRE_PROYECTO as nombreFidecomiso, x.estado as estado \n" +
            "from \n" +
            "(\n" +
            "SELECT case TO_CHAR(t.NIT_NEGOCIO) when '8001938488' then '001' else LPAD(TO_CHAR(t.NIT_NEGOCIO),3,'0')\n" +
            "       END ||\n" +
            "       LPAD(TO_CHAR(t.SUCURSAL),3,'0') ||\n" +
            "       LPAD(TO_CHAR(t.SECUENCIA),6,'0') AS numeroEncargo,\n" +
            "       --(SELECT IP.NOMBRE_PROYECTO FROM INM_TJTAS_REC_PROY TJ, INM_PROYECTOS IP WHERE TJ.NRO_TRJTA LIKE '%'||numeroEncargo||'%' AND IP.CODIGO_PROYECTO = TJ.CODIGO_PROYECTO) AS nombreFidecomiso,\n" +
            "       CASE TO_CHAR(t.ESTADO_CUENTA) WHEN '4' THEN 'INC' ELSE \n" +
            "        CASE TO_CHAR(t.ESTADO_CUENTA) WHEN '3' THEN 'INC' ELSE \n" +
            "          'ACT'\n" +
            "        END\n" +
            "       END AS estado\n" +
            "       FROM TALONARIO t\n" +
            "       WHERE\n" +
            "          t.nit_cliente = :beneficiario AND \n" +
            "          t.NIT_NEGOCIO IN ('301','302','304','8001938488')\n" +
            "       --order by nit_cliente desc\n" +
            "       ) X\n" +
            "       left join INM_TJTAS_REC_PROY TJ on replace(ltrim(replace(TJ.NRO_TRJTA,'0',' ')),' ','0') = replace(ltrim(replace(x.numeroEncargo,'0',' ')),' ','0')--lIKE '%'||x.numeroEncargo||'%'\n" +
            "       left join INM_PROYECTOS IP on TJ.CODIGO_PROYECTO = IP.CODIGO_PROYECTO",countQuery ="SELECT count(*)" +
            " FROM TALONARIO\n" +
            "WHERE \n" +
            "NIT_CLIENTE = :beneficiario AND \n" +
            "NIT_NEGOCIO IN (301,302,304,8001938488)", nativeQuery = true)
    Page<EncargosDto> findByEncargoBeneficiarioPago(Long beneficiario, Pageable pageable);



}
