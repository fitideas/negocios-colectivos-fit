package co.com.accion.negocios.repository;


import co.com.accion.negocios.dto.NroEncargo;
import co.com.accion.negocios.dto.NroSecuencia;
import co.com.accion.negocios.entities.InmEncargosProyEntity;
import co.com.accion.negocios.entities.InmEncargosProyEntityPK;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EncargosRepository extends PagingAndSortingRepository<InmEncargosProyEntity, InmEncargosProyEntityPK> {

    @Query(value = "SELECT * from INM_ENCARGOS_PROY where CODIGO_FIDEICOMISO = :codigo", nativeQuery = true)
    Page<InmEncargosProyEntity> findByCodigoFideicomiso(Long codigo, Pageable pageable);

    @Query(value = "SELECT * from INM_ENCARGOS_PROY where NRO_ENCARGO = :nro AND CODIGO_FIDEICOMISO = :codigo", nativeQuery = true)
    Page<InmEncargosProyEntity> findByNroEncargoAndCodigoFideicomiso(Long nro, Long codigo, Pageable page);

    @Query(value = "SELECT \n" +
            "CASE WHEN LENGTH(E.ID_FONDO) > 3 THEN '001'||E.COD_SUC||LPAD(E.NRO_ENCARGO, 6, '0')\n" +
            "        ELSE E.ID_FONDO||E.COD_SUC||LPAD(E.NRO_ENCARGO, 6, '0')\n" +
            "        END AS numeroEncargo\n" +
            "FROM    INM_ENCARGOS_PROY E\n" +
            "WHERE E.CODIGO_FIDEICOMISO = :codigo\n" +
            "and E.COD_SUC = :codigoSucursal " +
            "and E.NRO_ENCARGO = :nro", nativeQuery = true)
    NroEncargo findNroSecuenciaBySecuencia(Long codigo, Long codigoSucursal, long nro);

}
