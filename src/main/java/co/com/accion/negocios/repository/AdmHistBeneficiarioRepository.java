package co.com.accion.negocios.repository;


import co.com.accion.negocios.dto.AdmTipoBeneficiario;
import co.com.accion.negocios.entities.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdmHistBeneficiarioRepository extends PagingAndSortingRepository<AdmHistBeneficiarioEntity, AdmHistBeneficiarioEntityPK> {

    @Query(value = "SELECT * FROM ADM_HIST_BENEFICIARIO WHERE CODIGO_FIDEICOMISO = :codigo ORDER BY FECHA_CORTE DESC",nativeQuery = true)
    Page<AdmHistBeneficiarioEntity> findAllByCodigoFideicomiso(Long codigo, Pageable pageable);

    @Query(value = "SELECT ID_TIPO_BENEFICIARIO AS idTipoBeneficiario, DESCRIPCION AS descripcion, GENERA_CERTIFICADO AS generaCertificado" +
            " FROM ADM_TIPO_BENEFICIARIO WHERE ID_TIPO_BENEFICIARIO = :id AND ROWNUM = 1", nativeQuery = true)
    AdmTipoBeneficiario findByIdTipo(Long id);

}
