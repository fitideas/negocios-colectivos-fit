package co.com.accion.negocios.repository;

import co.com.accion.negocios.dto.ClientesBeneficiarioDto;
import co.com.accion.negocios.dto.ClientesDto;
import co.com.accion.negocios.dto.TipoDocumentoDto;
import co.com.accion.negocios.entities.ClientesEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import java.util.Collection;
import java.util.List;

@Repository
public interface ClientesRepository extends JpaRepository<ClientesEntity, Long> {

    @Query(value="SELECT  NVL(TV.NOMBRE, ' ') AS tipoVinculacion,\n" +
            "      NVL(CI.CORREO_NOTIFIACIONES, 0) AS envioCorrespondenciaTipo,\n" +
            "      NVL(CLI.NOMBRE_COMUN, ' ') AS nombreCompleto,\n" +
            "      NVL(CLI.RAZON_SOCIAL, ' ') AS razonSocial,\n" +
            "      CLI.FECHA_DILIGENCIAMIENTO AS fechaVinculacion,\n" +
            "      NVL(NVL(CR.NOMBRE,CIUD.NOMBRE), ' ') AS ciudadResidencia,\n" +
            "      NVL(S.NOMBRE, ' ') AS oficinaVinculacion,\n" +
            "      NVL(NVL(PR.NOMBRE, P.NOMBRE), ' ') AS paisResidencia,\n" +
            "      NVL(CLI.NIT, 0) AS numeroDocumento,\n" +
            "      NVL(TP.NOMBRE, ' ') AS naturalezaJuridicaVinculado,\n" +
            "      NVL(CI.NUM_TEL_CEL, NVL(CLI.TELEFONO,0)) AS telefono,\n" +
            "      NVL(CIVI.NOMBRE, ' ') AS ciudadVinculacion,\n" +
            "      NVL(CI.CORREO_NOTIFIACIONES,\n" +
            "      NVL(CLI.DIR_URL,' ')) AS envioCorrespondencia\n" +
            "FROM clientes CLI\n" +
            "INNER JOIN tipos_documentos tipoDocumento ON CLI.TIPO_DOCUMENTO = tipoDocumento.CODIGO\n" +
            "LEFT JOIN clientes_info_adi CI ON CLI.NIT = CI.NIT_CLIENTE\n" +
            "LEFT JOIN tipos_clase_vinculacion TV ON CI.CLASE_VINCULA = TV.CODIGO\n" +
            "LEFT JOIN ciudades CIUD ON CI.COD_CIUDAD_RES = CIUD.CODIGO\n" +
            "LEFT JOIN paises P ON CI.COD_PAIS_RES = P.CODIGO\n" +
            "LEFT JOIN (\n" +
            "     select DISTINCT SU.NOMBRE, SU.CODIGO_SUCURSAL FROM sucursales SU ) S ON S.CODIGO_SUCURSAL = CLI.COD_OFICINA_VINCULACION\n" +
            "LEFT JOIN ciudades CIVI ON CIVI.CODIGO = CLI.COD_CIUDAD_DILIGENCIAMIENTO\n" +
            "\n" +
            "LEFT JOIN (\n" +
            "       SELECT  MAX(nvl(dir.CODIGO_CIUDAD, dir2.codigo_ciudad)) CODIGO_CIUDAD, dir.NIT_CLIENTE\n" +
            "       FROM    direcciones dir full join direcciones dir2 on dir.nit_cliente = dir2.nit_cliente\n" +
            "               and dir.tipo_direccion = 2 and dir2.tipo_direccion = 3\n" +
            "       WHERE   (dir.tipo_direccion in (2,3)\n" +
            "               or dir2.tipo_direccion in (2,3))\n" +
            "       GROUP BY DIR.NIT_CLIENTE\n" +
            ") DR ON DR.NIT_CLIENTE = CLI.NIT\n" +
            "LEFT JOIN ciudades CR ON DR.codigo_ciudad = CR.codigo\n" +
            "LEFT JOIN division_politica DPR ON CR.codigo_depto = DPR.codigo\n" +
            "LEFT JOIN paises PR ON DPR.codigo_pais = PR.codigo\n" +
            "LEFT JOIN tipos_personas TP ON CLI.TIPO_PERSONA = TP.CODIGO " +
                "WHERE CLI.NIT = :cedula AND CLI.TIPO_PERSONA IN (1,2)", nativeQuery = true)
    ClientesDto getCliente(@Param("cedula") Long cedula);

    @Query(value = "SELECT NOMBRE AS descripcion, CODIGO AS id FROM tipos_documentos WHERE CODIGO = :id", nativeQuery = true)
    TipoDocumentoDto getTipoDocumento(@Param("id") Long id);

    @Query(value = "SELECT TIPO_DOCUMENTO AS ID FROM clientes WHERE NIT = :id", nativeQuery = true)
    TipoDocumentoDto getIdTipoDocumentoCliente(@Param("id") Long id);

    @Query(value="SELECT  NVL(TV.NOMBRE, ' ') AS tipoVinculacion,\n" +
            "      NVL(CI.CORREO_NOTIFIACIONES, 0) AS envioCorrespondenciaTipo,\n" +
            "      NVL(CLI.NOMBRE_COMUN, ' ') AS nombreCompleto,\n" +
            "      NVL(CLI.RAZON_SOCIAL, ' ') AS razonSocial,\n" +
            "      CLI.FECHA_DILIGENCIAMIENTO AS fechaVinculacion,\n" +
            "      NVL(NVL(CR.NOMBRE,CIUD.NOMBRE), ' ') AS ciudadResidencia,\n" +
            "      NVL(S.NOMBRE, ' ') AS oficinaVinculacion,\n" +
            "      NVL(NVL(PR.NOMBRE, P.NOMBRE), ' ') AS paisResidencia,\n" +
            "      NVL(CLI.NIT, 0) AS numeroDocumento,\n" +
            "      NVL(TP.NOMBRE, ' ') AS naturalezaJuridicaVinculado,\n" +
            "      NVL(CI.NUM_TEL_CEL, NVL(CLI.TELEFONO,0)) AS telefono,\n" +
            "      NVL(CIVI.NOMBRE, ' ') AS ciudadVinculacion,\n" +
            "      NVL(CI.CORREO_NOTIFIACIONES,\n" +
            "      NVL(CLI.DIR_URL,' ')) AS envioCorrespondencia\n" +
            "FROM clientes CLI\n" +
            "INNER JOIN tipos_documentos tipoDocumento ON CLI.TIPO_DOCUMENTO = tipoDocumento.CODIGO\n" +
            "LEFT JOIN clientes_info_adi CI ON CLI.NIT = CI.NIT_CLIENTE\n" +
            "LEFT JOIN tipos_clase_vinculacion TV ON CI.CLASE_VINCULA = TV.CODIGO\n" +
            "LEFT JOIN ciudades CIUD ON CI.COD_CIUDAD_RES = CIUD.CODIGO\n" +
            "LEFT JOIN paises P ON CI.COD_PAIS_RES = P.CODIGO\n" +
            "LEFT JOIN (\n" +
            "     select DISTINCT SU.NOMBRE, SU.CODIGO_SUCURSAL FROM sucursales SU ) S ON S.CODIGO_SUCURSAL = CLI.COD_OFICINA_VINCULACION\n" +
            "LEFT JOIN ciudades CIVI ON CIVI.CODIGO = CLI.COD_CIUDAD_DILIGENCIAMIENTO\n" +
            "\n" +
            "LEFT JOIN (\n" +
            "       SELECT  MAX(nvl(dir.CODIGO_CIUDAD, dir2.codigo_ciudad)) CODIGO_CIUDAD, dir.NIT_CLIENTE\n" +
            "       FROM    direcciones dir full join direcciones dir2 on dir.nit_cliente = dir2.nit_cliente\n" +
            "               and dir.tipo_direccion = 2 and dir2.tipo_direccion = 3\n" +
            "       WHERE   (dir.tipo_direccion in (2,3)\n" +
            "               or dir2.tipo_direccion in (2,3))\n" +
            "       GROUP BY DIR.NIT_CLIENTE\n" +
            ") DR ON DR.NIT_CLIENTE = CLI.NIT\n" +
            "LEFT JOIN ciudades CR ON DR.codigo_ciudad = CR.codigo\n" +
            "LEFT JOIN division_politica DPR ON CR.codigo_depto = DPR.codigo\n" +
            "LEFT JOIN paises PR ON DPR.codigo_pais = PR.codigo\n" +
            "LEFT JOIN tipos_personas TP ON CLI.TIPO_PERSONA = TP.CODIGO " +
            "WHERE REGEXP_LIKE(CLI.NOMBRE_COMUN, :nombre, 'i') AND CLI.TIPO_PERSONA IN (1,2)",  countQuery = "select count (*)  FROM clientes CLI where REGEXP_LIKE(CLI.NOMBRE_COMUN, :nombre, 'i')",nativeQuery = true)
    Page<ClientesDto> getClientesLikeNombre(String nombre, Pageable pageable);

    @Query(value="SELECT  NVL(TV.NOMBRE, ' ') AS tipoVinculacion,\n" +
            "      NVL(CI.CORREO_NOTIFIACIONES, 0) AS envioCorrespondenciaTipo,\n" +
            "      NVL(CLI.NOMBRE_COMUN, ' ') AS nombreCompleto,\n" +
            "      NVL(CLI.RAZON_SOCIAL, ' ') AS razonSocial,\n" +
            "      CLI.FECHA_DILIGENCIAMIENTO AS fechaVinculacion,\n" +
            "      NVL(NVL(CR.NOMBRE,CIUD.NOMBRE), ' ') AS ciudadResidencia,\n" +
            "      NVL(S.NOMBRE, ' ') AS oficinaVinculacion,\n" +
            "      NVL(NVL(PR.NOMBRE, P.NOMBRE), ' ') AS paisResidencia,\n" +
            "      NVL(CLI.NIT, 0) AS numeroDocumento,\n" +
            "      NVL(TP.NOMBRE, ' ') AS naturalezaJuridicaVinculado,\n" +
            "      NVL(CI.NUM_TEL_CEL, NVL(CLI.TELEFONO,0)) AS telefono,\n" +
            "      NVL(CIVI.NOMBRE, ' ') AS ciudadVinculacion,\n" +
            "      NVL(CI.CORREO_NOTIFIACIONES,\n" +
            "      NVL(CLI.DIR_URL,' ')) AS envioCorrespondencia\n" +
            "FROM clientes CLI\n" +
            "INNER JOIN tipos_documentos tipoDocumento ON CLI.TIPO_DOCUMENTO = tipoDocumento.CODIGO\n" +
            "LEFT JOIN clientes_info_adi CI ON CLI.NIT = CI.NIT_CLIENTE\n" +
            "LEFT JOIN tipos_clase_vinculacion TV ON CI.CLASE_VINCULA = TV.CODIGO\n" +
            "LEFT JOIN ciudades CIUD ON CI.COD_CIUDAD_RES = CIUD.CODIGO\n" +
            "LEFT JOIN paises P ON CI.COD_PAIS_RES = P.CODIGO\n" +
            "LEFT JOIN (\n" +
            "     select DISTINCT SU.NOMBRE, SU.CODIGO_SUCURSAL FROM sucursales SU ) S ON S.CODIGO_SUCURSAL = CLI.COD_OFICINA_VINCULACION\n" +
            "LEFT JOIN ciudades CIVI ON CIVI.CODIGO = CLI.COD_CIUDAD_DILIGENCIAMIENTO\n" +
            "\n" +
            "LEFT JOIN (\n" +
            "       SELECT  MAX(nvl(dir.CODIGO_CIUDAD, dir2.codigo_ciudad)) CODIGO_CIUDAD, dir.NIT_CLIENTE\n" +
            "       FROM    direcciones dir full join direcciones dir2 on dir.nit_cliente = dir2.nit_cliente\n" +
            "               and dir.tipo_direccion = 2 and dir2.tipo_direccion = 3\n" +
            "       WHERE   (dir.tipo_direccion in (2,3)\n" +
            "               or dir2.tipo_direccion in (2,3))\n" +
            "       GROUP BY DIR.NIT_CLIENTE\n" +
            ") DR ON DR.NIT_CLIENTE = CLI.NIT\n" +
            "LEFT JOIN ciudades CR ON DR.codigo_ciudad = CR.codigo\n" +
            "LEFT JOIN division_politica DPR ON CR.codigo_depto = DPR.codigo\n" +
            "LEFT JOIN paises PR ON DPR.codigo_pais = PR.codigo\n" +
            "LEFT JOIN tipos_personas TP ON CLI.TIPO_PERSONA = TP.CODIGO " +
            "WHERE UPPER(CLI.NOMBRE_COMUN) = UPPER(:nombre) AND CLI.TIPO_PERSONA IN (1,2)",  countQuery = "select count (*) FROM clientes CLI where UPPER(CLI.NOMBRE_COMUN) = UPPER(:nombre)",nativeQuery = true)
    Page<ClientesDto> getClientesByNombre(String nombre, Pageable pageable);

    @Query(value="SELECT  NVL(TV.NOMBRE, ' ') AS tipoVinculacion,\n" +
            "      NVL(CI.CORREO_NOTIFIACIONES, 0) AS envioCorrespondenciaTipo,\n" +
            "      NVL(CLI.NOMBRE_COMUN, ' ') AS nombreCompleto,\n" +
            "      NVL(CLI.RAZON_SOCIAL, ' ') AS razonSocial,\n" +
            "      CLI.FECHA_DILIGENCIAMIENTO AS fechaVinculacion,\n" +
            "      NVL(NVL(CR.NOMBRE,CIUD.NOMBRE), ' ') AS ciudadResidencia,\n" +
            "      NVL(S.NOMBRE, ' ') AS oficinaVinculacion,\n" +
            "      NVL(NVL(PR.NOMBRE, P.NOMBRE), ' ') AS paisResidencia,\n" +
            "      NVL(CLI.NIT, 0) AS numeroDocumento,\n" +
            "      NVL(TP.NOMBRE, ' ') AS naturalezaJuridicaVinculado,\n" +
            "      NVL(CI.NUM_TEL_CEL, NVL(CLI.TELEFONO,0)) AS telefono,\n" +
            "      NVL(CIVI.NOMBRE, ' ') AS ciudadVinculacion,\n" +
            "      NVL(CI.CORREO_NOTIFIACIONES,\n" +
            "      NVL(CLI.DIR_URL,' ')) AS envioCorrespondencia\n" +
            "FROM clientes CLI\n" +
            "INNER JOIN tipos_documentos tipoDocumento ON CLI.TIPO_DOCUMENTO = tipoDocumento.CODIGO\n" +
            "LEFT JOIN clientes_info_adi CI ON CLI.NIT = CI.NIT_CLIENTE\n" +
            "LEFT JOIN tipos_clase_vinculacion TV ON CI.CLASE_VINCULA = TV.CODIGO\n" +
            "LEFT JOIN ciudades CIUD ON CI.COD_CIUDAD_RES = CIUD.CODIGO\n" +
            "LEFT JOIN paises P ON CI.COD_PAIS_RES = P.CODIGO\n" +
            "LEFT JOIN (\n" +
            "     select DISTINCT SU.NOMBRE, SU.CODIGO_SUCURSAL FROM sucursales SU ) S ON S.CODIGO_SUCURSAL = CLI.COD_OFICINA_VINCULACION\n" +
            "LEFT JOIN ciudades CIVI ON CIVI.CODIGO = CLI.COD_CIUDAD_DILIGENCIAMIENTO\n" +
            "\n" +
            "LEFT JOIN (\n" +
            "       SELECT  MAX(nvl(dir.CODIGO_CIUDAD, dir2.codigo_ciudad)) CODIGO_CIUDAD, dir.NIT_CLIENTE\n" +
            "       FROM    direcciones dir full join direcciones dir2 on dir.nit_cliente = dir2.nit_cliente\n" +
            "               and dir.tipo_direccion = 2 and dir2.tipo_direccion = 3\n" +
            "       WHERE   (dir.tipo_direccion in (2,3)\n" +
            "               or dir2.tipo_direccion in (2,3))\n" +
            "       GROUP BY DIR.NIT_CLIENTE\n" +
            ") DR ON DR.NIT_CLIENTE = CLI.NIT\n" +
            "LEFT JOIN ciudades CR ON DR.codigo_ciudad = CR.codigo\n" +
            "LEFT JOIN division_politica DPR ON CR.codigo_depto = DPR.codigo\n" +
            "LEFT JOIN paises PR ON DPR.codigo_pais = PR.codigo\n" +
            "LEFT JOIN tipos_personas TP ON CLI.TIPO_PERSONA = TP.CODIGO " +
            "WHERE CLI.NIT LIKE :identificacion% AND CLI.TIPO_PERSONA IN (1,2)",  countQuery = "select count (CLI.USUARIO)  FROM clientes CLI WHERE CLI.NIT LIKE :identificacion%",nativeQuery = true)
    Page<ClientesDto> getClientesLikeIdentificacion(Long identificacion, Pageable pageable);

    @Query(value="SELECT  NVL(TV.NOMBRE, ' ') AS tipoVinculacion,\n" +
            "      NVL(CI.CORREO_NOTIFIACIONES, 0) AS envioCorrespondenciaTipo,\n" +
            "      NVL(CLI.NOMBRE_COMUN, ' ') AS nombreCompleto,\n" +
            "      NVL(CLI.RAZON_SOCIAL, ' ') AS razonSocial,\n" +
            "      CLI.FECHA_DILIGENCIAMIENTO AS fechaVinculacion,\n" +
            "      NVL(NVL(CR.NOMBRE,CIUD.NOMBRE), ' ') AS ciudadResidencia,\n" +
            "      NVL(S.NOMBRE, ' ') AS oficinaVinculacion,\n" +
            "      NVL(NVL(PR.NOMBRE, P.NOMBRE), ' ') AS paisResidencia,\n" +
            "      NVL(CLI.NIT, 0) AS numeroDocumento,\n" +
            "      NVL(TP.NOMBRE, ' ') AS naturalezaJuridicaVinculado,\n" +
            "      NVL(CI.NUM_TEL_CEL, NVL(CLI.TELEFONO,0)) AS telefono,\n" +
            "      NVL(CIVI.NOMBRE, ' ') AS ciudadVinculacion,\n" +
            "      NVL(CI.CORREO_NOTIFIACIONES,\n" +
            "      NVL(CLI.DIR_URL,' ')) AS envioCorrespondencia\n" +
            "FROM clientes CLI\n" +
            "INNER JOIN tipos_documentos tipoDocumento ON CLI.TIPO_DOCUMENTO = tipoDocumento.CODIGO\n" +
            "LEFT JOIN clientes_info_adi CI ON CLI.NIT = CI.NIT_CLIENTE\n" +
            "LEFT JOIN tipos_clase_vinculacion TV ON CI.CLASE_VINCULA = TV.CODIGO\n" +
            "LEFT JOIN ciudades CIUD ON CI.COD_CIUDAD_RES = CIUD.CODIGO\n" +
            "LEFT JOIN paises P ON CI.COD_PAIS_RES = P.CODIGO\n" +
            "LEFT JOIN (\n" +
            "     select DISTINCT SU.NOMBRE, SU.CODIGO_SUCURSAL FROM sucursales SU ) S ON S.CODIGO_SUCURSAL = CLI.COD_OFICINA_VINCULACION\n" +
            "LEFT JOIN ciudades CIVI ON CIVI.CODIGO = CLI.COD_CIUDAD_DILIGENCIAMIENTO\n" +
            "\n" +
            "LEFT JOIN (\n" +
            "       SELECT  MAX(nvl(dir.CODIGO_CIUDAD, dir2.codigo_ciudad)) CODIGO_CIUDAD, dir.NIT_CLIENTE\n" +
            "       FROM    direcciones dir full join direcciones dir2 on dir.nit_cliente = dir2.nit_cliente\n" +
            "               and dir.tipo_direccion = 2 and dir2.tipo_direccion = 3\n" +
            "       WHERE   (dir.tipo_direccion in (2,3)\n" +
            "               or dir2.tipo_direccion in (2,3))\n" +
            "       GROUP BY DIR.NIT_CLIENTE\n" +
            ") DR ON DR.NIT_CLIENTE = CLI.NIT\n" +
            "LEFT JOIN ciudades CR ON DR.codigo_ciudad = CR.codigo\n" +
            "LEFT JOIN division_politica DPR ON CR.codigo_depto = DPR.codigo\n" +
            "LEFT JOIN paises PR ON DPR.codigo_pais = PR.codigo\n" +
            "LEFT JOIN tipos_personas TP ON CLI.TIPO_PERSONA = TP.CODIGO " +
            "WHERE CLI.NIT LIKE :identificacion% AND REGEXP_LIKE(CLI.NOMBRE_COMUN, :nombre, 'i') AND CLI.TIPO_PERSONA IN (1,2)",  countQuery = "select count (CLI.USUARIO)  FROM clientes CLI WHERE CLI.NIT LIKE :identificacion% AND REGEXP_LIKE(CLI.NOMBRE_COMUN, :nombre, 'i')",nativeQuery = true)
    Page<ClientesDto> getClientesLikeIdentificacionYNombre(Long identificacion, String nombre, Pageable pageable);

    @Query(value="SELECT  NVL(TV.NOMBRE, ' ') AS tipoVinculacion,\n" +
            "      NVL(CI.CORREO_NOTIFIACIONES, 0) AS envioCorrespondenciaTipo,\n" +
            "      NVL(CLI.NOMBRE_COMUN, ' ') AS nombreCompleto,\n" +
            "      NVL(CLI.RAZON_SOCIAL, ' ') AS razonSocial,\n" +
            "      CLI.FECHA_DILIGENCIAMIENTO AS fechaVinculacion,\n" +
            "      NVL(NVL(CR.NOMBRE,CIUD.NOMBRE), ' ') AS ciudadResidencia,\n" +
            "      NVL(S.NOMBRE, ' ') AS oficinaVinculacion,\n" +
            "      NVL(NVL(PR.NOMBRE, P.NOMBRE), ' ') AS paisResidencia,\n" +
            "      NVL(CLI.NIT, 0) AS numeroDocumento,\n" +
            "      NVL(TP.NOMBRE, ' ') AS naturalezaJuridicaVinculado,\n" +
            "      NVL(CI.NUM_TEL_CEL, NVL(CLI.TELEFONO,0)) AS telefono,\n" +
            "      NVL(CIVI.NOMBRE, ' ') AS ciudadVinculacion,\n" +
            "      NVL(CI.CORREO_NOTIFIACIONES,\n" +
            "      NVL(CLI.DIR_URL,' ')) AS envioCorrespondencia\n" +
            "FROM clientes CLI\n" +
            "INNER JOIN tipos_documentos tipoDocumento ON CLI.TIPO_DOCUMENTO = tipoDocumento.CODIGO\n" +
            "LEFT JOIN clientes_info_adi CI ON CLI.NIT = CI.NIT_CLIENTE\n" +
            "LEFT JOIN tipos_clase_vinculacion TV ON CI.CLASE_VINCULA = TV.CODIGO\n" +
            "LEFT JOIN ciudades CIUD ON CI.COD_CIUDAD_RES = CIUD.CODIGO\n" +
            "LEFT JOIN paises P ON CI.COD_PAIS_RES = P.CODIGO\n" +
            "LEFT JOIN (\n" +
            "     select DISTINCT SU.NOMBRE, SU.CODIGO_SUCURSAL FROM sucursales SU ) S ON S.CODIGO_SUCURSAL = CLI.COD_OFICINA_VINCULACION\n" +
            "LEFT JOIN ciudades CIVI ON CIVI.CODIGO = CLI.COD_CIUDAD_DILIGENCIAMIENTO\n" +
            "\n" +
            "LEFT JOIN (\n" +
            "       SELECT  MAX(nvl(dir.CODIGO_CIUDAD, dir2.codigo_ciudad)) CODIGO_CIUDAD, dir.NIT_CLIENTE\n" +
            "       FROM    direcciones dir full join direcciones dir2 on dir.nit_cliente = dir2.nit_cliente\n" +
            "               and dir.tipo_direccion = 2 and dir2.tipo_direccion = 3\n" +
            "       WHERE   (dir.tipo_direccion in (2,3)\n" +
            "               or dir2.tipo_direccion in (2,3))\n" +
            "       GROUP BY DIR.NIT_CLIENTE\n" +
            ") DR ON DR.NIT_CLIENTE = CLI.NIT\n" +
            "LEFT JOIN ciudades CR ON DR.codigo_ciudad = CR.codigo\n" +
            "LEFT JOIN division_politica DPR ON CR.codigo_depto = DPR.codigo\n" +
            "LEFT JOIN paises PR ON DPR.codigo_pais = PR.codigo\n" +
            "LEFT JOIN tipos_personas TP ON CLI.TIPO_PERSONA = TP.CODIGO " +
            "WHERE CLI.NIT = :identificacion AND UPPER(CLI.NOMBRE_COMUN) = UPPER(:nombre) AND CLI.TIPO_PERSONA IN (1,2)",  countQuery = "select count (CLI.USUARIO)  FROM clientes CLI WHERE CLI.NIT = :identificacion AND UPPER(CLI.NOMBRE_COMUN) = UPPER(:nombre)",nativeQuery = true)
    Page<ClientesDto> getClientesByIdentificacionYNombre(Long identificacion, String nombre, Pageable pageable);

    @Query(value="SELECT NVL(TV.NOMBRE, ' ') AS tipoVinculacion," +
            "NVL(CI.TIPO_ENVIO, 0) AS envioCorrespondenciaTipo," +
            "NVL(C.NOMBRE_COMUN, ' ') AS nombreCompleto," +
            "NVL(C.RAZON_SOCIAL, ' ') AS razonSocial," +
            "C.FECHA_DILIGENCIAMIENTO AS fechaVinculacion," +
            "NVL(CIUD.NOMBRE, ' ') AS ciudadResidencia," +
            "NVL(S.NOMBRE, ' ') AS oficinaVinculacion," +
            "NVL(P.NOMBRE, ' ') AS paisResidencia," +
            "NVL(C.NIT, 0) AS numeroDocumento," +
            "NVL(TP.NOMBRE, ' ') AS naturalezaJuridicaVinculado," +
            "NVL(CI.NUM_TEL_CEL, 0) AS telefono," +
            "NVL(S.NOMBRE_CIUDAD, ' ') AS ciudadVinculacion," +
            "NVL(CI.CORREO_NOTIFIACIONES, ' ') AS email " +
            "FROM BENEFICIARIO B JOIN CLIENTES C ON B.NIT_BENEFICIARIO = C.NIT JOIN FIDEICOMISOS F ON F.CODIGO = B.CODIGO_FIDEICOMISO\n" +
            "LEFT JOIN clientes_info_adi CI ON C.NIT = CI.NIT_CLIENTE " +
            "LEFT JOIN tipos_clase_vinculacion TV ON CI.CLASE_VINCULA = TV.CODIGO " +
            "LEFT JOIN ciudades CIUD ON CI.COD_CIUDAD_RES = CIUD.CODIGO " +
            "LEFT JOIN paises P ON CI.COD_PAIS_RES = P.CODIGO " +
            "LEFT JOIN (SELECT SUC.NIT_CLIENTE AS NIT_CLIENTE, SUC.CODIGO_SUCURSAL AS CODIGO_SUCURSAL, SUC.NOMBRE AS NOMBRE, CID.NOMBRE AS NOMBRE_CIUDAD FROM sucursales SUC INNER JOIN CIUDADES CID ON SUC.CODIGO_CIUDAD = CID.CODIGO) S ON S.CODIGO_SUCURSAL = C.COD_OFICINA_VINCULACION AND S.NIT_CLIENTE = C.NIT " +
            "LEFT JOIN tipos_personas TP ON C.TIPO_PERSONA = TP.CODIGO " +
            "INNER JOIN tipos_documentos tipoDocumento ON C.TIPO_DOCUMENTO = tipoDocumento.CODIGO " +
            "WHERE F.CODIGO = :codigoNegocio AND C.TIPO_PERSONA IN (1,2)"
            , countQuery = "select count(*) FROM BENEFICIARIO B JOIN CLIENTES C ON B.NIT_BENEFICIARIO = C.NIT JOIN FIDEICOMISOS F ON F.CODIGO = B.CODIGO_FIDEICOMISO WHERE F.CODIGO = :codigoNegocio", nativeQuery = true)
    Page<ClientesDto> getBeneficiariosPorNegocio(Long codigoNegocio, Pageable pageable);


    @Query(value="SELECT NVL(TV.NOMBRE, ' ') AS tipoVinculacion, NVL(B.PORCE_PARTICIPA, 0) AS participacion," +
            "NVL(CI.TIPO_ENVIO, 0) AS envioCorrespondenciaTipo," +
            "NVL(C.NOMBRE_COMUN, ' ') AS nombreCompleto," +
            "NVL(C.RAZON_SOCIAL, ' ') AS razonSocial," +
            "C.FECHA_DILIGENCIAMIENTO AS fechaVinculacion," +
            "NVL(CIUD.NOMBRE, ' ') AS ciudadResidencia," +
            "NVL(S.NOMBRE, ' ') AS oficinaVinculacion," +
            "NVL(P.NOMBRE, ' ') AS paisResidencia," +
            "NVL(C.NIT, 0) AS numeroDocumento," +
            "NVL(TP.NOMBRE, ' ') AS naturalezaJuridicaVinculado," +
            "NVL(CI.NUM_TEL_CEL, NVL(C.TELEFONO,0)) AS telefono," +
            "NVL(S.NOMBRE_CIUDAD, ' ') AS ciudadVinculacion," +
            "NVL(CI.CORREO_NOTIFIACIONES, NVL(C.DIRECCION_ENVIO,' ')) AS email " +
            "FROM BENEFICIARIO B JOIN CLIENTES C ON B.NIT_BENEFICIARIO = C.NIT JOIN FIDEICOMISOS F ON F.CODIGO = B.CODIGO_FIDEICOMISO\n" +
            "LEFT JOIN clientes_info_adi CI ON C.NIT = CI.NIT_CLIENTE " +
            "LEFT JOIN tipos_clase_vinculacion TV ON CI.CLASE_VINCULA = TV.CODIGO " +
            "LEFT JOIN ciudades CIUD ON CI.COD_CIUDAD_RES = CIUD.CODIGO " +
            "LEFT JOIN paises P ON CI.COD_PAIS_RES = P.CODIGO " +
            "LEFT JOIN (SELECT SUC.NIT_CLIENTE AS NIT_CLIENTE, SUC.CODIGO_SUCURSAL AS CODIGO_SUCURSAL, SUC.NOMBRE AS NOMBRE, CID.NOMBRE AS NOMBRE_CIUDAD FROM sucursales SUC INNER JOIN CIUDADES CID ON SUC.CODIGO_CIUDAD = CID.CODIGO) S ON S.CODIGO_SUCURSAL = C.COD_OFICINA_VINCULACION AND S.NIT_CLIENTE = C.NIT " +
            "LEFT JOIN tipos_personas TP ON C.TIPO_PERSONA = TP.CODIGO " +
            "INNER JOIN tipos_documentos tipoDocumento ON C.TIPO_DOCUMENTO = tipoDocumento.CODIGO " +
            "WHERE F.CODIGO = :codigoNegocio AND C.TIPO_PERSONA IN (1,2)", countQuery = "select count(*) FROM BENEFICIARIO B JOIN CLIENTES C ON B.NIT_BENEFICIARIO = C.NIT JOIN FIDEICOMISOS F ON F.CODIGO = B.CODIGO_FIDEICOMISO WHERE F.CODIGO = :codigoNegocio", nativeQuery = true)
    Page<ClientesBeneficiarioDto> getBeneficiariosParticipacionPorNegocio(Long codigoNegocio, Pageable pageable);

    @Query(value = "SELECT COUNT (*) FROM CLIENTES WHERE NIT = :nroDoc",nativeQuery = true)
    int getCountCliente(Long nroDoc);

    @Query(value = "SELECT COUNT (*) FROM BENEFICIARIO WHERE NIT_BENEFICIARIO = :nroDoc",nativeQuery = true)
    int getCountBeneficiario(Long nroDoc);

    @Query(value = "SELECT DIRECCION FROM DIRECCIONES WHERE NIT_CLIENTE = :nitCliente " +
            "AND TIPO_DIRECCION = 5 AND SEQ_DIR = (SELECT MAX (SEQ_DIR)  " +
            "FROM DIRECCIONES WHERE NIT_CLIENTE = :nitCliente AND TIPO_DIRECCION = 5)", nativeQuery = true)
    String getEmailByCliente(Long nitCliente);



}
