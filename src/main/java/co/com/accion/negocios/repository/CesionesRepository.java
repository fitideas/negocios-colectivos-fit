package co.com.accion.negocios.repository;

import co.com.accion.negocios.dto.ClaseBeneficio;
import co.com.accion.negocios.dto.NegocioDerechosDto;
import co.com.accion.negocios.entities.AdmDetCesionesEntity;
import co.com.accion.negocios.entities.AdmDetCesionesEntityPK;
import co.com.accion.negocios.entities.FideicomisosEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CesionesRepository extends PagingAndSortingRepository<AdmDetCesionesEntity, AdmDetCesionesEntityPK> {
    @Query(value = "SELECT * from ADM_DET_CESIONES where ID_NEGOCIO = :idNegocio", nativeQuery = true)
    Page<AdmDetCesionesEntity> findAllByIdNegocio(Long idNegocio, Pageable page);

    @Query(value = "SELECT * from ADM_DET_CESIONES where ID_NEGOCIO = :idNegocio AND FECHA_REGISTRO >= to_date(:startDate,'DD-MM-YYYY') AND FECHA_REGISTRO <= to_date(:endDate, 'DD-MM-YYYY')", nativeQuery = true)
    Page<AdmDetCesionesEntity> findAllByIdNegocioAndDates(Long idNegocio, String startDate, String endDate, Pageable page);

    @Query(value = "SELECT * from ADM_DET_CESIONES where ID_NEGOCIO = :idNegocio AND (ID_CEDENTE = :identificacion OR ID_CESIONARIO = :identificacion)", nativeQuery = true)
    Page<AdmDetCesionesEntity> findAllByIdNegocioAndIdentificacion(Long idNegocio, Long identificacion, Pageable page);

    @Query(value = "SELECT * from ADM_DET_CESIONES where ID_NEGOCIO = :idNegocio AND (ID_CEDENTE = :identificacion OR ID_CESIONARIO = :identificacion) AND FECHA_REGISTRO >= to_date(:startDate,'DD-MM-YYYY') AND FECHA_REGISTRO <= to_date(:endDate, 'DD-MM-YYYY')", nativeQuery = true)
    Page<AdmDetCesionesEntity> findAllByIdNegocioAndIdentificacionAndDates(Long idNegocio, Long identificacion, String startDate, String endDate, Pageable page);

    @Query(value = "SELECT ID_CLASE_BENEFICIO AS idClaseBeneficio, DESCRIPCION AS descripcion FROM ADM_CLASE_BENEFICIO WHERE ID_CLASE_BENEFICIO = :idClase", nativeQuery = true)
    ClaseBeneficio findClaseBeneficioById(Long idClase);

}
