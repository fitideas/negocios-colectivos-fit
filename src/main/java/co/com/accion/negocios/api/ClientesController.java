package co.com.accion.negocios.api;

import co.com.accion.negocios.dto.*;
import co.com.accion.negocios.exception.ObjectNotFoundException;
import co.com.accion.negocios.repository.ClientesRepository;
import co.com.accion.negocios.repository.FidecomisosRepository;
import co.com.accion.negocios.util.ClientsDtoToFullConverter;
import co.com.accion.negocios.util.Pagination;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Optional;

import static org.springframework.http.ResponseEntity.ok;

@Api(description="Operaciones sobre los clientes")
@CrossOrigin("*")
@RestController
@RequestMapping("/negocios-colectivos/api/clientes")
public class ClientesController {

    @Autowired
    ClientesRepository clientesRepository;

    @Autowired
    FidecomisosRepository fidecomisosRepository;

    @ApiOperation(value = "Ver la lista de clientes correspondientes al nombre y/o identificación dados. La búsqueda puede ser por coincidencia exacta o parcial")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message= "OK" , response = ClientesDtoFull.class , responseContainer = "List"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ResponseMsg.class)
    })
    @GetMapping(value = "", produces = "application/json")
    public ResponseEntity getClientById(@RequestParam(required = false) Long identificacion, @RequestParam(required = false) String nombre, @RequestParam Optional<Long> limit, @RequestParam Optional<Long> start, @RequestParam(required = false, defaultValue = "false") boolean exactMatch) throws ObjectNotFoundException {
        ArrayList<Object> clientesDtoFull = new ArrayList<>();
        Page<ClientesDto> page =null;
        ClientesDtoFull clienteDtoFull ;
        String[] params = new String[3];
        String[] paramsValues = new String[3];

        if(!exactMatch) {
            if (identificacion != null && nombre != null) {
                params[0] = "identificacion";
                paramsValues[0] = String.valueOf(identificacion);
                params[1] = "nombre";
                paramsValues[1] = nombre;
                page = clientesRepository.getClientesLikeIdentificacionYNombre(identificacion, nombre, limit.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                for (ClientesDto clienteActual : page) {
                    TipoDocumentoDto tipoDocumento = clientesRepository.getTipoDocumento(clientesRepository.getIdTipoDocumentoCliente(Long.parseLong(clienteActual.getNumeroDocumento())).getId());
                    String email = clientesRepository.getEmailByCliente(Long.parseLong(clienteActual.getNumeroDocumento()));
                    clienteDtoFull = ClientsDtoToFullConverter.dtoToFull(clienteActual,email);
                    clienteDtoFull.setTipoDocumento(tipoDocumento);
                    clientesDtoFull.add(clienteDtoFull);
                }
            } else if (identificacion != null) {
                params[0] = "identificacion";
                paramsValues[0] = String.valueOf(identificacion);
                page = clientesRepository.getClientesLikeIdentificacion(identificacion, limit.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                for (ClientesDto clienteActual : page) {
                    TipoDocumentoDto tipoDocumento = clientesRepository.getTipoDocumento(clientesRepository.getIdTipoDocumentoCliente(Long.parseLong(clienteActual.getNumeroDocumento())).getId());
                    String email = clientesRepository.getEmailByCliente(Long.parseLong(clienteActual.getNumeroDocumento()));
                    clienteDtoFull = ClientsDtoToFullConverter.dtoToFull(clienteActual,email);
                    clienteDtoFull.setTipoDocumento(tipoDocumento);
                    clientesDtoFull.add(clienteDtoFull);
                }
            } else if (nombre != null && nombre != "") {
                params[0] = "nombre";
                paramsValues[0] = nombre;
                page = clientesRepository.getClientesLikeNombre(nombre, limit.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                for (ClientesDto clienteActual : page) {
                    TipoDocumentoDto tipoDocumento = clientesRepository.getTipoDocumento(clientesRepository.getIdTipoDocumentoCliente(Long.parseLong(clienteActual.getNumeroDocumento())).getId());
                    String email = clientesRepository.getEmailByCliente(Long.parseLong(clienteActual.getNumeroDocumento()));
                    clienteDtoFull = ClientsDtoToFullConverter.dtoToFull(clienteActual,email);
                    clienteDtoFull.setTipoDocumento(tipoDocumento);
                    clientesDtoFull.add(clienteDtoFull);
                }
            } else
                throw new ObjectNotFoundException("Usuario no encontrado");
        }
        else {
            params[0] = "exactMatch";
            paramsValues[0] = "true";
            if (identificacion != null && nombre != null) {
                params[1] = "identificacion";
                paramsValues[1] = String.valueOf(identificacion);
                params[2] = "nombre";
                paramsValues[2] = nombre;
                page = clientesRepository.getClientesByIdentificacionYNombre(identificacion, nombre, limit.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                for (ClientesDto clienteActual : page) {
                    TipoDocumentoDto tipoDocumento = clientesRepository.getTipoDocumento(clientesRepository.getIdTipoDocumentoCliente(Long.parseLong(clienteActual.getNumeroDocumento())).getId());
                    String email = clientesRepository.getEmailByCliente(Long.parseLong(clienteActual.getNumeroDocumento()));
                    clienteDtoFull = ClientsDtoToFullConverter.dtoToFull(clienteActual,email);
                    clienteDtoFull.setTipoDocumento(tipoDocumento);
                    clientesDtoFull.add(clienteDtoFull);
                }
            } else if (identificacion != null) {
                params[1] = "identificacion";
                paramsValues[1] = String.valueOf(identificacion);

                ClientesDto cliente = clientesRepository.getCliente(identificacion);

                if (cliente != null) {
                    TipoDocumentoDto tipoDocumento = clientesRepository.getTipoDocumento(clientesRepository.getIdTipoDocumentoCliente(identificacion).getId());
                    String email = clientesRepository.getEmailByCliente(Long.parseLong(cliente.getNumeroDocumento()));
                    clienteDtoFull = ClientsDtoToFullConverter.dtoToFull(cliente,email);
                    clienteDtoFull.setTipoDocumento(tipoDocumento);
                    clientesDtoFull.add(clienteDtoFull);
                } else
                    throw new ObjectNotFoundException("Usuario no encontrado");
            } else if (nombre != null && nombre != "") {
                params[1] = "nombre";
                paramsValues[1] = nombre;
                page = clientesRepository.getClientesByNombre(nombre, limit.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                for (ClientesDto clienteActual : page) {
                    TipoDocumentoDto tipoDocumento = clientesRepository.getTipoDocumento(clientesRepository.getIdTipoDocumentoCliente(Long.parseLong(clienteActual.getNumeroDocumento())).getId());
                    String email = clientesRepository.getEmailByCliente(Long.parseLong(clienteActual.getNumeroDocumento()));
                    clienteDtoFull = ClientsDtoToFullConverter.dtoToFull(clienteActual,email);
                    clienteDtoFull.setTipoDocumento(tipoDocumento);
                    clientesDtoFull.add(clienteDtoFull);
                }
            } else
                throw new ObjectNotFoundException("Usuario no encontrado");
        }

        PaginationDTO paginationDTO =null;
        if(limit.isPresent() && start.isPresent() && page!=null) {
            paginationDTO = Pagination.crearPaginationDTO(page,"/negocios-colectivos/api/clientes?", params , paramsValues, clientesDtoFull, limit.get(), start.get());
        }

        return new ResponseEntity<>(paginationDTO != null ? paginationDTO : clientesDtoFull, HttpStatus.OK);
    }

    @ApiOperation(value = "Ver la lista de negocios del beneficiario correspondiente a la identificación dada")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message= "OK" , response = NegocioDTO.class , responseContainer = "List"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ResponseMsg.class)
    })
    @GetMapping(value = "{idCliente}/negocios", produces = "application/json")
    public ResponseEntity getNegociosByBeneficiario(@PathVariable(value = "idCliente") Long idCliente, @RequestParam Optional<Long> limit, @RequestParam Optional<Long> start) throws ObjectNotFoundException {
        ArrayList negocios = new ArrayList<>();
        PaginationDTO paginationDTO =null;

        if(idCliente != null) {
            if(clientesRepository.getCliente(idCliente) != null) {
                Page<NegocioDerechosDto> page = fidecomisosRepository.getNegociosByBeneficiario(idCliente, limit.isPresent() && start.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                NegocioDTO negocio;
                for (NegocioDerechosDto negocioActual : page) {
                    int indexEspacio = negocioActual.getNombreFideicomiso().indexOf(' ');
                    negocio = new NegocioDTO(
                            indexEspacio == -1 ? negocioActual.getNombreFideicomiso() : negocioActual.getNombreFideicomiso().split(" ")[0],
                            String.valueOf(negocioActual.getCodigo()),
                            new NegocioDTO.EstadoNegocio(negocioActual.getEstadoFideicomiso(), ""),
                            negocioActual.getParticipacion(),
                            indexEspacio == -1 ? negocioActual.getNombreFideicomiso() : negocioActual.getNombreFideicomiso().substring(indexEspacio + 1)
                    );
                    negocios.add(negocio);
                }

                if(limit.isPresent() && start.isPresent())
                    paginationDTO = Pagination.crearPaginationDTO(page,"/negocios-colectivos/api/clientes/"+idCliente+"/negocios?", null , null, negocios, limit.get(), start.get());


            } else
                throw new ObjectNotFoundException("Usuario no encontrado");
        } else
            throw new ObjectNotFoundException("Usuario no encontrado");

        return new ResponseEntity<>(paginationDTO != null ? paginationDTO : negocios, HttpStatus.OK);


    }
}