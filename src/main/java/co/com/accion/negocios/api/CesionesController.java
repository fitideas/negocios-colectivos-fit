package co.com.accion.negocios.api;

import co.com.accion.negocios.dto.*;
import co.com.accion.negocios.entities.AdmDetCesionesEntity;
import co.com.accion.negocios.entities.FideicomisosEntity;
import co.com.accion.negocios.exception.ObjectNotFoundException;
import co.com.accion.negocios.repository.AdmHistBeneficiarioRepository;
import co.com.accion.negocios.repository.CesionesRepository;
import co.com.accion.negocios.repository.ClientesRepository;
import co.com.accion.negocios.repository.FidecomisosRepository;
import co.com.accion.negocios.util.ClientsDtoToFullConverter;
import co.com.accion.negocios.util.Pagination;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Api("Operaciones sobre las cesiones")
@CrossOrigin("*")
@RestController
@RequestMapping("/negocios-colectivos/api/cesiones")
public class CesionesController {

    @Autowired
    FidecomisosRepository fidecomisosRepository;

    @Autowired
    CesionesRepository cesionesRepository;

    @Autowired
    ClientesRepository clientesRepository;

    @Autowired
    AdmHistBeneficiarioRepository histBeneficiarioRepository;

    @ApiOperation(value = "Ver la lista de cesiones del negocio correspondiente al código dado", notes = "Si se proporciona una identificación, se mostrarán sólo las cesiones que involucren a la persona correspondiente.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message= "OK" ,response = CesionDTO.class , responseContainer = "List"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ResponseMsg.class)
    })
    @GetMapping(value = "", produces = "application/json")
    public ResponseEntity getCesionesByNegocio(@RequestParam Long codigoNegocio, @RequestParam Optional<Long> limit, @RequestParam Optional<Long> start,  @RequestParam Optional<Long> identificacion, @RequestParam Optional<String> startDate, @RequestParam Optional<String> endDate) throws ObjectNotFoundException {
        ArrayList cesiones = new ArrayList<>();
        PaginationDTO paginationDTO =null;
        Page<AdmDetCesionesEntity> page = null;
        String[] params = new String[4];
        String[] paramsValues = new String[4];
        if (codigoNegocio != null) {
            List<FideicomisosEntity> negocios = fidecomisosRepository.findByCodigo(codigoNegocio);
            if (!negocios.isEmpty()) {
                if(!identificacion.isPresent()) {
                    if(startDate.isPresent() && endDate.isPresent()) {
                        page = cesionesRepository.findAllByIdNegocioAndDates(codigoNegocio,startDate.get(),endDate.get(),limit.isPresent() && start.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                        params[1]="startDate";
                        params[2]="endDate";
                        paramsValues[1]=startDate.get();
                        paramsValues[2]=endDate.get();
                    } else
                        page = cesionesRepository.findAllByIdNegocio(codigoNegocio, limit.isPresent() && start.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                    params[0]="codigoNegocio";
                    paramsValues[0]=String.valueOf(codigoNegocio);
                } else {
                    if(startDate.isPresent() && endDate.isPresent()) {
                        page = cesionesRepository.findAllByIdNegocioAndIdentificacionAndDates(codigoNegocio,identificacion.get(),startDate.get(),endDate.get(),limit.isPresent() && start.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                        params[2]="startDate";
                        params[3]="endDate";
                        paramsValues[2]=startDate.get();
                        paramsValues[3]=endDate.get();
                    } else
                        page = cesionesRepository.findAllByIdNegocioAndIdentificacion(codigoNegocio, identificacion.get(), limit.isPresent() && start.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                    params[0]="codigoNegocio";
                    paramsValues[0]=String.valueOf(codigoNegocio);
                    params[1]="identificacion";
                    paramsValues[1]=String.valueOf(identificacion.get());
                }
                CesionDTO cesionDTO;
                ClientesDto propietarioNuevo, propietarioPrevio;
                AdmTipoBeneficiario tipoBeneficiario = null;
                ClaseBeneficio claseBeneficio;
                ClientesDtoFull propietarioNuevoFull = null, propietarioPrevioFull = null;
                TipoDocumentoDto tipoDocumento;
                for (AdmDetCesionesEntity cesionActual : page) {
                    propietarioNuevo = clientesRepository.getCliente(cesionActual.getIdCesionario());
                    if (propietarioNuevo != null) {
                        tipoDocumento = clientesRepository.getTipoDocumento(clientesRepository.getIdTipoDocumentoCliente(cesionActual.getIdCesionario()).getId());
                        String email = clientesRepository.getEmailByCliente(Long.parseLong(propietarioNuevo.getNumeroDocumento()));
                        propietarioNuevoFull = ClientsDtoToFullConverter.dtoToFull(propietarioNuevo,email);
                        propietarioNuevoFull.setTipoDocumento(tipoDocumento);
                    }
                    propietarioPrevio = clientesRepository.getCliente(cesionActual.getIdCedente());
                    if (propietarioPrevio != null) {
                        tipoDocumento = clientesRepository.getTipoDocumento(clientesRepository.getIdTipoDocumentoCliente(cesionActual.getIdCedente()).getId());
                        String email = clientesRepository.getEmailByCliente(Long.parseLong(propietarioPrevio.getNumeroDocumento()));
                        propietarioPrevioFull = ClientsDtoToFullConverter.dtoToFull(propietarioPrevio,email);
                        propietarioPrevioFull.setTipoDocumento(tipoDocumento);
                    }
                    tipoBeneficiario = cesionActual.getIdTipoBeneficiario() != null ?  histBeneficiarioRepository.findByIdTipo(cesionActual.getIdTipoBeneficiario()) : null;
                    claseBeneficio = cesionActual.getIdClaseBeneficio() != null ? cesionesRepository.findClaseBeneficioById(cesionActual.getIdClaseBeneficio()) : null;
                    cesionDTO = new CesionDTO(
                            String.valueOf(cesionActual.getIdNegocio()),
                            String.valueOf(cesionActual.getIdContrato()),
                            String.valueOf(cesionActual.getNroCesion()),
                            cesionActual.getPorceActualCedente(),
                            cesionActual.getPorceNuevoCedente(),
                            cesionActual.getPorceActualCedente() - cesionActual.getPorceNuevoCedente(),
                            cesionActual.getFechaRegistro(),
                            claseBeneficio != null ? new ClaseBeneficioDto(claseBeneficio.getIdClaseBeneficio(),claseBeneficio.getDescripcion()) : null,
                            tipoBeneficiario != null ? new TipoBeneficiarioDto(tipoBeneficiario.getIdTipoBeneficiario(),tipoBeneficiario.getDescripcion(),tipoBeneficiario.getGeneraCertificado()) : null,
                            propietarioNuevoFull,
                            propietarioPrevioFull

                    );
                    cesiones.add(cesionDTO);
                }


                if(cesiones.isEmpty())
                    throw new ObjectNotFoundException("No se ha encontrado ninguna cesión con los parámetros dados");

                if(limit.isPresent() && start.isPresent()) {
                    paginationDTO = Pagination.crearPaginationDTO(page,"/negocios-colectivos/api/cesiones?", params , paramsValues, cesiones, limit.get(), start.get());
                }

            } else
                throw new ObjectNotFoundException("No se ha encontrado ningún negocio con el código indicado");
        } else
            throw new ObjectNotFoundException("No se ha encontrado ningún negocio con el código indicado");


        return new ResponseEntity<>(paginationDTO != null ? paginationDTO : cesiones, HttpStatus.OK);

    }



}