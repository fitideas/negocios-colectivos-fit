package co.com.accion.negocios.api;

import co.com.accion.negocios.dto.*;
import co.com.accion.negocios.entities.AdmHistBeneficiarioEntity;
import co.com.accion.negocios.entities.AdmLibroAnotacionesEntity;
import co.com.accion.negocios.entities.FideicomisosEntity;
import co.com.accion.negocios.exception.ObjectNotFoundException;
import co.com.accion.negocios.repository.AdmHistBeneficiarioRepository;
import co.com.accion.negocios.repository.ClientesRepository;
import co.com.accion.negocios.repository.EventosRepository;
import co.com.accion.negocios.repository.FidecomisosRepository;
import co.com.accion.negocios.util.ClientsDtoToFullConverter;
import co.com.accion.negocios.util.Pagination;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Api(description="Operaciones sobre los negocios")
@CrossOrigin("*")
@RestController
@RequestMapping("/negocios-colectivos/api/negocios")
public class NegociosController {
    @Autowired
    FidecomisosRepository fidecomisosRepository;

    @Autowired
    ClientesRepository clientesRepository;

    @Autowired
    EventosRepository eventosRepository;

    @Autowired
    AdmHistBeneficiarioRepository histBeneficiarioRepository;

    @ApiOperation(value = "Ver la lista de clientes correspondientes al nombre y/o identificación dados. La búsqueda puede ser por coincidencia exacta o parcial")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message= "OK" , response = NegocioDTO.class , responseContainer = "List"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ResponseMsg.class)
    })
    @GetMapping(value = "", produces = "application/json" )
    public ResponseEntity getFidecomisostById(@RequestParam Optional<Long> limit, @RequestParam Optional<Long> start, @RequestParam Optional<Long> codigoNegocio, @RequestParam Optional<String> nombreNegocio, @RequestParam(required = false, defaultValue = "false") boolean exactMatch) throws ObjectNotFoundException {

        List<Object> negocios = new ArrayList<>();
        Page<FideicomisosEntity> page=null;
        String[] params = new String[3];
        String[] paramsValues = new String[3];

        AtomicReference<List<FideicomisosEntity>> fideicomisos = new AtomicReference<>();
        if (exactMatch) {
            params[0] = "exactMatch";
            paramsValues[0] = "true";
            if (nombreNegocio.isPresent() && codigoNegocio.isPresent()) {
                page =fidecomisosRepository.findByCodigoAndNombreFideicomiso(codigoNegocio.orElse(null), nombreNegocio.orElse(""), limit.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                fideicomisos.set(page.getContent());
                params[1] = "nombreNegocio";
                paramsValues[1] = nombreNegocio.get();
                params[2] = "codigoNegocio";
                paramsValues[2] = String.valueOf(codigoNegocio.get());
            } else if (nombreNegocio.isPresent()) {
                page = fidecomisosRepository.findByNombreFideicomiso(nombreNegocio.orElse(""),limit.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                fideicomisos.set(page.getContent());
                params[1] = "nombreNegocio";
                paramsValues[1] = nombreNegocio.get();
            } else if (codigoNegocio.isPresent()) {
                fideicomisos.set(fidecomisosRepository.findByCodigo(Long.valueOf(codigoNegocio.get())));
                params[1] = "codigoNegocio";
                paramsValues[1] = String.valueOf(codigoNegocio.get());
            }
        } else {
            page = fidecomisosRepository.findByCodigoIsLikeAndNombreFideicomisoIsLike(codigoNegocio.orElse(null), nombreNegocio.orElse(""), limit.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
            fideicomisos.set(page.getContent());
            if(nombreNegocio.isPresent() && codigoNegocio.isPresent()){
                page = fidecomisosRepository.findByCodigoIsLikeAndNombreFideicomisoIsLike(codigoNegocio.orElse(null), nombreNegocio.orElse(""), limit.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                fideicomisos.set(page.getContent());
                params[0] = "nombreNegocio";
                paramsValues[0] = nombreNegocio.get();
                params[1] = "codigoNegocio";
                paramsValues[1] = String.valueOf(codigoNegocio.get());
            }
            else if (nombreNegocio.isPresent()){
                page = fidecomisosRepository.findByNombreFideicomisoIsLike(nombreNegocio.orElse(""),
                        limit.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                fideicomisos.set(page.getContent());
                params[0] = "nombreNegocio";
                paramsValues[0] = nombreNegocio.get();
            }
            else if (codigoNegocio.isPresent()){
                page = fidecomisosRepository.findByCodigoIsLike(Long.valueOf(codigoNegocio.get()), limit.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                fideicomisos.set(page.getContent());
                params[0] = "codigoNegocio";
                paramsValues[0] = String.valueOf(codigoNegocio.get());
            }

        }

        fideicomisos.get().forEach(fideicomisosEntity -> {
            NegocioDTO negocioDTO = new NegocioDTO(
                    String.valueOf(fideicomisosEntity.getCodigo()),
                    fideicomisosEntity.getNombreFideicomiso().split(" ")[0],
                    new NegocioDTO.EstadoNegocio(fideicomisosEntity.getEstadoFideicomiso(), ""),
                    null,
                    fideicomisosEntity.getNombreFideicomiso()
            );
            negocios.add(negocioDTO);
        });

        if(fideicomisos.get().isEmpty() || negocios.isEmpty())
            throw new ObjectNotFoundException("Negocio no encontrado");

        PaginationDTO paginationDTO =null;
        if(limit.isPresent() && start.isPresent() && page != null)
            paginationDTO = Pagination.crearPaginationDTO(page,"/negocios-colectivos/api/negocios?", params , paramsValues, negocios, limit.get(), start.get());

        return new ResponseEntity<Object>(paginationDTO != null ? paginationDTO : negocios,HttpStatus.OK);
    }


    @ApiOperation(value = "Ver la lista de beneficiarios del negocio correspondiente al código dado")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message= "OK" , response = ClientesDtoFull.class , responseContainer = "List"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ResponseMsg.class)
    })
    @GetMapping( value = "{codigoNegocio}/beneficiarios", produces = "application/json")
    public ResponseEntity getBeneficiariosByNegocio(@PathVariable(value = "codigoNegocio") Long codigoNegocio, @RequestParam Optional<Long> limit, @RequestParam Optional<Long> start) throws ObjectNotFoundException {
        ArrayList beneficiarios = new ArrayList<>();
        PaginationDTO paginationDTO =null;

        if (codigoNegocio != null) {
            if (!fidecomisosRepository.findByCodigo(codigoNegocio).isEmpty()) {
                Page<ClientesBeneficiarioDto> page = clientesRepository.getBeneficiariosParticipacionPorNegocio(codigoNegocio, limit.isPresent() && start.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                ClientesDtoFull beneficiario = new ClientesDtoFull();
                for (ClientesBeneficiarioDto participacionActual : page) {
                    TipoDocumentoDto tipoDocumento = clientesRepository.getTipoDocumento(clientesRepository.getIdTipoDocumentoCliente(Long.parseLong(participacionActual.getNumeroDocumento())).getId());
                    beneficiario = ClientsDtoToFullConverter.beneficiarioDtoToFull(participacionActual);
                    beneficiario.setTipoDocumento(tipoDocumento);
                    beneficiarios.add(beneficiario);
                }

                if(limit.isPresent() && start.isPresent())
                    paginationDTO = Pagination.crearPaginationDTO(page,"/negocios-colectivos/api/negocios/"+codigoNegocio+"/beneficiarios?", null , null, beneficiarios, limit.get(), start.get());


            } else
                throw new ObjectNotFoundException("Negocio no encontrado");
        } else
            throw new ObjectNotFoundException("Negocio no encontrado");

        return new ResponseEntity<>(paginationDTO != null ? paginationDTO : beneficiarios, HttpStatus.OK);
    }

    @ApiOperation(value = "Ver la lista de beneficiarios del negocio correspondiente al código dado")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message= "OK" , response = HistBeneficiarioDto.class , responseContainer = "List"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ResponseMsg.class)
    })
    @GetMapping( value = "{codigoNegocio}/historialBeneficiarios", produces = "application/json")
    public ResponseEntity getHistorialBeneficiariosByNegocio(@PathVariable(value = "codigoNegocio") Long codigoNegocio, @RequestParam Optional<Long> limit, @RequestParam Optional<Long> start) throws ObjectNotFoundException {
        ArrayList beneficiarios = new ArrayList<>();
        PaginationDTO paginationDTO =null;

        if (codigoNegocio != null) {
            if (!fidecomisosRepository.findByCodigo(codigoNegocio).isEmpty()) {
                Page<AdmHistBeneficiarioEntity> page = histBeneficiarioRepository.findAllByCodigoFideicomiso(codigoNegocio, limit.isPresent() && start.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);

                for (AdmHistBeneficiarioEntity actual : page) {
                    AdmTipoBeneficiario tipoBeneficiario = histBeneficiarioRepository.findByIdTipo(actual.getIdTipoBeneficiario());
                    HistBeneficiarioDto beneficiario = new HistBeneficiarioDto(
                            actual.getCodigoFideicomiso(),
                            actual.getNitBeneficiario(),
                            actual.getPorceParticipa(),
                            actual.getUsuario(),
                            actual.getFechaRegistro(),
                            actual.getEstadoBeneficiario(),
                            actual.getVlrParticipa(),
                            actual.getObservaciones(),
                            new TipoBeneficiarioDto(
                                    tipoBeneficiario.getIdTipoBeneficiario(),
                                    tipoBeneficiario.getDescripcion(),
                                    tipoBeneficiario.getGeneraCertificado()
                            ),
                            actual.getIdClaseBeneficio(),
                            actual.getMnjRangos(),
                            actual.getFechaCorte()
                    );
                    beneficiarios.add(beneficiario);
                }
                if(beneficiarios.isEmpty())
                    throw new ObjectNotFoundException("No se han encontrado beneficiarios en el historial del negocio");

                if(limit.isPresent() && start.isPresent())
                    paginationDTO = Pagination.crearPaginationDTO(page,"/negocios-colectivos/api/negocios/"+codigoNegocio+"/historialBeneficiarios?", null , null, beneficiarios, limit.get(), start.get());


            } else
                throw new ObjectNotFoundException("Negocio no encontrado");
        } else
            throw new ObjectNotFoundException("Negocio no encontrado");

        return new ResponseEntity<>(paginationDTO != null ? paginationDTO : beneficiarios, HttpStatus.OK);
    }

    @ApiOperation(value = "Ver la lista de funcionarios del negocio correspondiente al código dado")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message= "OK" , response = ResponsableDTO.class , responseContainer = "List"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ResponseMsg.class)
    })
    @GetMapping( value = "{codigoNegocio}/funcionarios", produces = "application/json")
    public ResponseEntity getFuncionariosByNegocio(@PathVariable(value = "codigoNegocio") Long codigoNegocio, @RequestParam Optional<Long> limit, @RequestParam Optional<Long> start) throws ObjectNotFoundException {
        ArrayList funcionarios = new ArrayList<>();
        PaginationDTO paginationDTO =null;

        if (codigoNegocio != null) {
            if (!fidecomisosRepository.findByCodigo(codigoNegocio).isEmpty()) {
                Page<ResponsableFideicomisoDto> page = fidecomisosRepository.getResponsablesByCodigoFideicomiso(codigoNegocio, limit.isPresent() && start.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                ClientesDtoFull infoFuncionario;
                ResponsableDTO responsableDTO;
                TipoDocumentoDto tipoDocumento;
                ClientesDto funcionario;
                for (ResponsableFideicomisoDto responsable : page) {
                    tipoDocumento = clientesRepository.getTipoDocumento(clientesRepository.getIdTipoDocumentoCliente(Long.parseLong(String.valueOf(responsable.getIdResponsable()))).getId());
                    funcionario = clientesRepository.getCliente(responsable.getIdResponsable());
                    String email = clientesRepository.getEmailByCliente(Long.parseLong(funcionario.getNumeroDocumento()));
                    infoFuncionario = ClientsDtoToFullConverter.dtoToFull(funcionario,email);
                    infoFuncionario.setTipoDocumento(tipoDocumento);
                    responsableDTO = new ResponsableDTO(
                            String.valueOf(responsable.getCodigoFideicomiso()),
                            responsable.getTipoResponsable(),
                            infoFuncionario
                    );
                    funcionarios.add(responsableDTO);
                }

                if(limit.isPresent() && start.isPresent())
                    paginationDTO = Pagination.crearPaginationDTO(page,"/negocios-colectivos/api/negocios/"+codigoNegocio+"/funcionarios?", null , null, funcionarios, limit.get(), start.get());


            } else
                throw new ObjectNotFoundException("Negocio no encontrado");
        } else
            throw new ObjectNotFoundException("Negocio no encontrado");

        if(funcionarios.isEmpty())
            throw new ObjectNotFoundException("No se encontraron funcionarios para el negocio dado");

        return new ResponseEntity<>(paginationDTO != null ? paginationDTO : funcionarios, HttpStatus.OK);
    }

    @ApiOperation(value = "Ver la lista de eventos del negocio correspondiente al código dado")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message= "OK" , response = EventoDTO.class , responseContainer = "List"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ResponseMsg.class)
    })
    @GetMapping( value = "{codigoNegocio}/eventos", produces = "application/json")
    public ResponseEntity getEventosByNegocio(@PathVariable(value = "codigoNegocio") Long codigoNegocio, @RequestParam Optional<Long> tipoEvento, @RequestParam Optional<Long> nroDocumento, @RequestParam Optional<Long> limit, @RequestParam Optional<Long> start) throws ObjectNotFoundException {
        ArrayList eventos = new ArrayList<>();
        PaginationDTO paginationDTO =null;
        Page<AdmLibroAnotacionesEntity> page;
        String[] params = new String[2];
        String[] paramsValues = new String[2];
        if (codigoNegocio != null) {
            if (!fidecomisosRepository.findByCodigo(codigoNegocio).isEmpty()) {
                if(!tipoEvento.isPresent()) {
                    if(!nroDocumento.isPresent())
                        page = eventosRepository.findAllByCodigoFideicomiso(codigoNegocio, limit.isPresent() && start.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                    else {
                        if(clientesRepository.getCountCliente(nroDocumento.get()) > 0 || clientesRepository.getCountBeneficiario(nroDocumento.get()) > 0) {
                            page = eventosRepository.findAllByCodigoFideicomisoAndNroDocumento(codigoNegocio, nroDocumento.get(), limit.isPresent() && start.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                            params[0] = "nroDocumento";
                            paramsValues[0] = String.valueOf(nroDocumento.get());
                        } else {
                            throw new ObjectNotFoundException("Cliente no encontrado");
                        }
                    }

                } else {
                    if(!nroDocumento.isPresent()) {
                        page = eventosRepository.findAllByCodigoFideicomisoAndTipoEvento(codigoNegocio, tipoEvento.get(), limit.isPresent() && start.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                        params[0] = "tipoEvento";
                        paramsValues[0] = String.valueOf(tipoEvento.get());
                    } else {
                        if(clientesRepository.getCountCliente(nroDocumento.get()) > 0 || clientesRepository.getCountBeneficiario(nroDocumento.get()) > 0) {
                            page = eventosRepository.findAllByCodigoFideicomisoAndTipoEventoAndNroDocumento(codigoNegocio, tipoEvento.get(), nroDocumento.get(), limit.isPresent() && start.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                            params[0] = "tipoEvento";
                            paramsValues[0] = String.valueOf(tipoEvento.get());
                            params[1] = "nroDocumento";
                            paramsValues[1] = String.valueOf(nroDocumento.get());
                        } else {
                            throw new ObjectNotFoundException("Cliente no encontrado");
                        }
                    }
                }
                TipoEventoDto tipoEventoDto;
                EventoDTO eventoDTO;
                for (AdmLibroAnotacionesEntity evento : page) {
                    tipoEventoDto = eventosRepository.findTipoByCodigo(evento.getTipoEvento());

                    eventoDTO = new EventoDTO(
                            evento.getCodigoFideicomiso(),
                            String.valueOf(evento.getSeqEvento()),
                            evento.getFechaEvento(),
                            evento.getFechaRegistro(),
                            evento.getNombreFuncionario(),
                            String.valueOf(evento.getNroDocumento()),
                            tipoEventoDto.getNombre(),
                            tipoEventoDto.getClaseEvento(),
                            String.valueOf(tipoEventoDto.getTipoCalculoComision()),
                            evento.getEstadoEvento(),
                            String.valueOf(evento.getConcluciones())
                    );
                    eventos.add(eventoDTO);
                }

                if(limit.isPresent() && start.isPresent())
                    paginationDTO = Pagination.crearPaginationDTO(page,"/negocios-colectivos/api/negocios/"+codigoNegocio+"/funcionarios?", params , paramsValues, eventos, limit.get(), start.get());


            } else
                throw new ObjectNotFoundException("Negocio no encontrado");
        } else
            throw new ObjectNotFoundException("Negocio no encontrado");

        if(eventos.isEmpty())
            throw new ObjectNotFoundException("No se encontraron eventos con los parámetros dados");

        return new ResponseEntity<>(paginationDTO != null ? paginationDTO : eventos, HttpStatus.OK);
    }

    @ApiOperation(value = "Ver el evento del negocio correspondiente al código dado con la secuencia dada")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message= "OK" , response = EventoDTO.class),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ResponseMsg.class)
    })
    @GetMapping( value = "{codigoNegocio}/eventos/{secuenciaEvento}", produces = "application/json")
    public EventoDTO getEventosByNegocioAndSecuencia(@PathVariable(value = "codigoNegocio") Long codigoNegocio, @PathVariable(value = "secuenciaEvento") Long secuenciaEvento) throws ObjectNotFoundException {
        EventoDTO eventoDTO;
        if (codigoNegocio != null) {
            if (!fidecomisosRepository.findByCodigo(codigoNegocio).isEmpty()) {
                if(secuenciaEvento != null) {
                    AdmLibroAnotacionesEntity evento = eventosRepository.findByCodigoFideicomisoAndSeqEvento(codigoNegocio, secuenciaEvento);
                    TipoEventoDto tipoEventoDto;
                    if (evento != null) {
                        tipoEventoDto = eventosRepository.findTipoByCodigo(evento.getTipoEvento());

                        eventoDTO = new EventoDTO(
                                evento.getCodigoFideicomiso(),
                                String.valueOf(evento.getSeqEvento()),
                                evento.getFechaEvento(),
                                evento.getFechaRegistro(),
                                evento.getNombreFuncionario(),
                                String.valueOf(evento.getNroDocumento()),
                                tipoEventoDto.getNombre(),
                                tipoEventoDto.getClaseEvento(),
                                String.valueOf(tipoEventoDto.getTipoCalculoComision()),
                                evento.getEstadoEvento(),
                                String.valueOf(evento.getConcluciones())
                        );
                    } else
                        throw new ObjectNotFoundException("Evento no encontrado");
                } else
                    throw new ObjectNotFoundException("Evento no encontrado");
            } else
                throw new ObjectNotFoundException("Negocio no encontrado");
        } else
            throw new ObjectNotFoundException("Negocio no encontrado");

        return eventoDTO;
    }

}
