package co.com.accion.negocios.api;

import co.com.accion.negocios.dto.*;
import co.com.accion.negocios.entities.FideicomisosEntity;
import co.com.accion.negocios.entities.InmEncargosProyEntity;
import co.com.accion.negocios.exception.ObjectNotFoundException;
import co.com.accion.negocios.repository.ClientesRepository;
import co.com.accion.negocios.repository.EncargosRepository;
import co.com.accion.negocios.repository.FidecomisosRepository;
import co.com.accion.negocios.util.Pagination;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Api(description="Operaciones sobre los encargos")
@CrossOrigin("*")
@RestController
@RequestMapping("/negocios-colectivos/api/encargos")
public class EncargosController {

    @Autowired
    FidecomisosRepository fidecomisosRepository;

    @Autowired
    EncargosRepository encargosRepository;

    @Autowired
    ClientesRepository clientesRepository;

    @ApiOperation(value = "Ver la lista de encargos del negocio correspondiente al código dado")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message= "OK" , response = EncargoDto.class , responseContainer = "List"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ResponseMsg.class)
    })
    @GetMapping(value = "", produces = "application/json")
    public ResponseEntity getEncargosByNegocio(@RequestParam Optional<Long> codigoNegocio, @RequestParam Optional<Long> nroDocumento, @RequestParam Optional<Long> limit, @RequestParam Optional<Long> start) throws ObjectNotFoundException {
        ArrayList encargos = new ArrayList<>();
        PaginationDTO paginationDTO =null;
        NroSecuencia nroSecuencia;
        NroEncargo nroEncargo;
        String secuenciaCompleta;
        if(nroDocumento.isPresent()) {
            if (nroDocumento != null) {
                //BeneficiarioDto negocio = fidecomisosRepository.findNegocioByBeneficiario(nroDocumento.get());
                //if (negocio != null) {
                    //Page<InmEncargosProyEntity> page = encargosRepository.findByCodigoFideicomiso(negocio.getCodigoFideicomiso(), limit.isPresent() && start.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                    EncargoDto encargoDto;
                try{
                    Page<EncargosDto> page = fidecomisosRepository.findByEncargoBeneficiarioPago(nroDocumento.get(), limit.isPresent() && start.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);

                    for (EncargosDto encargo : page.getContent()){
                        encargoDto = new EncargoDto(
                                encargo.getNumeroEncargo(),
                                encargo.getNombreFidecomiso(),
                                encargo.getEstado()
                        );
                        encargos.add(encargoDto);
                    }

                    if (limit.isPresent() && start.isPresent()) {
                        String[] params = {"nroDocumento"};
                        String[] paramsValues = {String.valueOf(nroDocumento.get())};
                        paginationDTO = Pagination.crearPaginationDTO(page, "/negocios-colectivos/api/encargos?", params, paramsValues, encargos, limit.get(), start.get());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                    /*for (InmEncargosProyEntity encargoActual : page) {
                        nroEncargo = encargosRepository.findNroSecuenciaBySecuencia(encargoActual.getCodigoFideicomiso(),encargoActual.getCodSuc(), encargoActual.getNroEncargo());
                        //secuenciaCompleta=nroSecuencia != null ? nroSecuencia.getCiudad()+""+nroSecuencia.getSucursal()+""+nroSecuencia.getSecuencia():encargoActual.getNroEncargo()+"";
                        //System.out.println(nroEncargo.getNumeroEncargo());
                        encargoDto = new EncargoDto(
                                nroEncargo.getNumeroEncargo(),
                                negocio.getNombreFideicomiso(),
                                encargoActual.getEstado()
                        );
                        encargos.add(encargoDto);
                    }*/



                /*} else
                    throw new ObjectNotFoundException("Negocio no encontrado");*/
            } else
                throw new ObjectNotFoundException("Persona no encontrada");
        } else if(codigoNegocio.isPresent()) {
            if (codigoNegocio != null) {
                List<FideicomisosEntity> negocios = fidecomisosRepository.findByCodigo(codigoNegocio.get());
                if (!negocios.isEmpty()) {
                    String nombreFidecomiso = negocios.get(0).getNombreFideicomiso();
                    Page<InmEncargosProyEntity> page = encargosRepository.findByCodigoFideicomiso(codigoNegocio.get(), limit.isPresent() && start.isPresent() ? PageRequest.of(start.get().intValue(), limit.get().intValue()) : null);
                    EncargoDto encargoDto;
                    for (InmEncargosProyEntity encargoActual : page) {
                        nroEncargo = encargosRepository.findNroSecuenciaBySecuencia(encargoActual.getCodigoFideicomiso(),encargoActual.getCodSuc(), encargoActual.getNroEncargo());
                        //secuenciaCompleta=nroSecuencia != null ? nroSecuencia.getCiudad()+""+nroSecuencia.getSucursal()+""+nroSecuencia.getSecuencia():encargoActual.getNroEncargo()+"";
                        //System.out.println(nroEncargo.getNumeroEncargo());
                        encargoDto = new EncargoDto(
                                nroEncargo.getNumeroEncargo(),
                                nombreFidecomiso,
                                encargoActual.getEstado()
                        );
                        encargos.add(encargoDto);
                    }

                    if (limit.isPresent() && start.isPresent()) {
                        String[] params = {"codigoNegocio"};
                        String[] paramsValues = {String.valueOf(codigoNegocio.get())};
                        paginationDTO = Pagination.crearPaginationDTO(page, "/negocios-colectivos/api/encargos?", params, paramsValues, encargos, limit.get(), start.get());
                    }

                } else
                    throw new ObjectNotFoundException("Negocio no encontrado");
            } else
                throw new ObjectNotFoundException("Negocio no encontrado");
        }

        return new ResponseEntity<>(paginationDTO != null ? paginationDTO : encargos, HttpStatus.OK);

    }

    @ApiOperation(value = "Ver el estado del encargo correspondiente al beneficiario, negocio y número de encargo dados")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message= "OK" , response = EncargoDto.class),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found", response = ResponseMsg.class)
    })
    @GetMapping(value = "{nroEncargo}", produces = "application/json")
    public EncargoDto verificarEncargoBeneficiario(@RequestParam Long documento, @RequestParam Long codigoNegocio, @PathVariable(value = "nroEncargo") Long nroEncargo) throws ObjectNotFoundException {
        NroSecuencia nroSecuencia;
        NroEncargo nroEncargo1;
        String secuenciaCompleta;
        if (codigoNegocio != null && documento != null && nroEncargo != null) {
            List<FideicomisosEntity> negocios = fidecomisosRepository.findByCodigo(codigoNegocio);
            if (!negocios.isEmpty()) {
                Page<ClientesDto> beneficiarios = clientesRepository.getBeneficiariosPorNegocio(codigoNegocio, null);
                if(!beneficiarios.isEmpty()) {
                    boolean beneficiarioEsta = false;
                    for(ClientesDto cliente : beneficiarios){
                        if(cliente.getNumeroDocumento().equals(String.valueOf(documento)))
                            beneficiarioEsta=true;
                    }
                    if(beneficiarioEsta) {
                        String nombreFidecomiso = negocios.get(0).getNombreFideicomiso();
                        Page<InmEncargosProyEntity> encargosEntities = encargosRepository.findByNroEncargoAndCodigoFideicomiso(nroEncargo,codigoNegocio,null);
                        EncargoDto encargo;

                        for (InmEncargosProyEntity encargoActual : encargosEntities) {
                            nroEncargo1 = encargosRepository.findNroSecuenciaBySecuencia(encargoActual.getCodigoFideicomiso(),encargoActual.getCodSuc(), encargoActual.getNroEncargo());
                            //secuenciaCompleta=nroSecuencia != null ? nroSecuencia.getCiudad()+""+nroSecuencia.getSucursal()+""+nroSecuencia.getSecuencia():encargoActual.getNroEncargo()+"";
                            //System.out.println(nroEncargo1.getNumeroEncargo());
                            encargo = new EncargoDto(
                                    nroEncargo1.getNumeroEncargo(),
                                    nombreFidecomiso,
                                    encargoActual.getEstado()
                            );
                            return encargo;

                        }
                    } else
                        throw new ObjectNotFoundException("El número de documento no coincide con ningún beneficiario del negocio");
                } else
                    throw new ObjectNotFoundException("No hay beneficiarios para este negocio");
            } else
                throw new ObjectNotFoundException("Negocio no encontrado");
        } else
            throw new ObjectNotFoundException("No se han introducido todos los parámetros");

        throw new ObjectNotFoundException("No se ha encontrado el encargo");

    }



}