package co.com.accion.negocios.config;

import co.com.accion.negocios.dto.ResponseMsg;
import co.com.accion.negocios.exception.ObjectNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.GenericJDBCException;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@RestControllerAdvice
@Slf4j
public class RestExceptionHandler {

    @ExceptionHandler(ObjectNotFoundException.class)
    public ResponseEntity handleObjectNotFoundException(ObjectNotFoundException ex) {
        ResponseMsg responseMsg = new ResponseMsg("AF4001","No se ha encontrado el elemento", ex.getMessage());
        return new ResponseEntity(responseMsg, HttpStatus.NO_CONTENT);
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity handleMissingServletRequestParameterException(MissingServletRequestParameterException ex) {
        ResponseMsg responseMsg = new ResponseMsg("AF4002","No se han ingresado todos los parámetros", ex.getMessage());
        return new ResponseEntity(responseMsg, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex) {
        ResponseMsg responseMsg = new ResponseMsg("AF4003","Uno de los parámetros no tiene el formato esperado", ex.getMessage());
        return new ResponseEntity(responseMsg, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException ex) {
        ResponseMsg responseMsg = new ResponseMsg("AF4004","El método HTTP de la petición no es correcto", ex.getMessage());
        return new ResponseEntity(responseMsg, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity handleNullPointerException(NullPointerException ex) {
        ResponseMsg responseMsg = new ResponseMsg("AF5001","Error al realizar la operación", ex.getMessage());
        return new ResponseEntity(responseMsg, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(SQLGrammarException.class)
    public ResponseEntity handleSQLGrammarException(SQLGrammarException ex) {
        ResponseMsg responseMsg = new ResponseMsg("AF5002","Error en la consulta a la base de datos", "No se han podido obtener los datos");
        return new ResponseEntity(responseMsg, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(GenericJDBCException.class)
    public ResponseEntity handleGenericJDBCException(GenericJDBCException ex) {
        ResponseMsg responseMsg = new ResponseMsg("AF5003","Error al consultar la base de datos", "No se han podido obtener los datos");
        return new ResponseEntity(responseMsg, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity handleException(Exception ex) {
        ResponseMsg responseMsg = new ResponseMsg("AF5004","Error desconocido", "Ha ocurrido un error inesperado.");
        return new ResponseEntity(responseMsg, HttpStatus.INTERNAL_SERVER_ERROR);
    }



}