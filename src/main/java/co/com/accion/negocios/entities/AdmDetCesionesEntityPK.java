package co.com.accion.negocios.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class AdmDetCesionesEntityPK implements Serializable {
    private Long idNegocio;
    private String idContrato;
    private Long idCedente;
    private Long nroCesion;
    private Long seqDetalle;

    @Column(name = "ID_NEGOCIO")
    @Id
    public Long getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Long idNegocio) {
        this.idNegocio = idNegocio;
    }

    @Column(name = "ID_CONTRATO")
    @Id
    public String getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(String idContrato) {
        this.idContrato = idContrato;
    }

    @Column(name = "ID_CEDENTE")
    @Id
    public Long getIdCedente() {
        return idCedente;
    }

    public void setIdCedente(Long idCedente) {
        this.idCedente = idCedente;
    }

    @Column(name = "NRO_CESION")
    @Id
    public Long getNroCesion() {
        return nroCesion;
    }

    public void setNroCesion(Long nroCesion) {
        this.nroCesion = nroCesion;
    }

    @Column(name = "SEQ_DETALLE")
    @Id
    public Long getSeqDetalle() {
        return seqDetalle;
    }

    public void setSeqDetalle(Long seqDetalle) {
        this.seqDetalle = seqDetalle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdmDetCesionesEntityPK that = (AdmDetCesionesEntityPK) o;
        return Objects.equals(idNegocio, that.idNegocio) &&
                Objects.equals(idContrato, that.idContrato) &&
                Objects.equals(idCedente, that.idCedente) &&
                Objects.equals(nroCesion, that.nroCesion) &&
                Objects.equals(seqDetalle, that.seqDetalle);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idNegocio, idContrato, idCedente, nroCesion, seqDetalle);
    }
}
