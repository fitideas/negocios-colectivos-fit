package co.com.accion.negocios.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class InmEncargosProyEntityPK implements Serializable {
    private long codigoFideicomiso;
    private long codigoProyecto;
    private long codigoEtapa;
    private long idFondo;
    private long codSuc;
    private long nroEncargo;

    @Column(name = "CODIGO_FIDEICOMISO")
    @Id
    public long getCodigoFideicomiso() {
        return codigoFideicomiso;
    }

    public void setCodigoFideicomiso(long codigoFideicomiso) {
        this.codigoFideicomiso = codigoFideicomiso;
    }

    @Column(name = "CODIGO_PROYECTO")
    @Id
    public long getCodigoProyecto() {
        return codigoProyecto;
    }

    public void setCodigoProyecto(long codigoProyecto) {
        this.codigoProyecto = codigoProyecto;
    }

    @Column(name = "CODIGO_ETAPA")
    @Id
    public long getCodigoEtapa() {
        return codigoEtapa;
    }

    public void setCodigoEtapa(long codigoEtapa) {
        this.codigoEtapa = codigoEtapa;
    }

    @Column(name = "ID_FONDO")
    @Id
    public long getIdFondo() {
        return idFondo;
    }

    public void setIdFondo(long idFondo) {
        this.idFondo = idFondo;
    }

    @Column(name = "COD_SUC")
    @Id
    public long getCodSuc() {
        return codSuc;
    }

    public void setCodSuc(long codSuc) {
        this.codSuc = codSuc;
    }

    @Column(name = "NRO_ENCARGO")
    @Id
    public long getNroEncargo() {
        return nroEncargo;
    }

    public void setNroEncargo(long nroEncargo) {
        this.nroEncargo = nroEncargo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InmEncargosProyEntityPK that = (InmEncargosProyEntityPK) o;
        return codigoFideicomiso == that.codigoFideicomiso &&
                codigoProyecto == that.codigoProyecto &&
                codigoEtapa == that.codigoEtapa &&
                idFondo == that.idFondo &&
                codSuc == that.codSuc &&
                nroEncargo == that.nroEncargo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigoFideicomiso, codigoProyecto, codigoEtapa, idFondo, codSuc, nroEncargo);
    }
}
