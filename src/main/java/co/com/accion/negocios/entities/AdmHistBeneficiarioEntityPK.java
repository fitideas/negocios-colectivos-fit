package co.com.accion.negocios.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

public class AdmHistBeneficiarioEntityPK implements Serializable {
    private Long codigoFideicomiso;
    private Long nitBeneficiario;
    private Long idTipoBeneficiario;
    private Long idClaseBeneficio;
    private Date fechaCorte;

    @Column(name = "CODIGO_FIDEICOMISO")
    @Id
    public Long getCodigoFideicomiso() {
        return codigoFideicomiso;
    }

    public void setCodigoFideicomiso(Long codigoFideicomiso) {
        this.codigoFideicomiso = codigoFideicomiso;
    }

    @Column(name = "NIT_BENEFICIARIO")
    @Id
    public Long getNitBeneficiario() {
        return nitBeneficiario;
    }

    public void setNitBeneficiario(Long nitBeneficiario) {
        this.nitBeneficiario = nitBeneficiario;
    }

    @Column(name = "ID_TIPO_BENEFICIARIO")
    @Id
    public Long getIdTipoBeneficiario() {
        return idTipoBeneficiario;
    }

    public void setIdTipoBeneficiario(Long idTipoBeneficiario) {
        this.idTipoBeneficiario = idTipoBeneficiario;
    }

    @Column(name = "ID_CLASE_BENEFICIO")
    @Id
    public Long getIdClaseBeneficio() {
        return idClaseBeneficio;
    }

    public void setIdClaseBeneficio(Long idClaseBeneficio) {
        this.idClaseBeneficio = idClaseBeneficio;
    }

    @Column(name = "FECHA_CORTE")
    @Id
    public Date getFechaCorte() {
        return fechaCorte;
    }

    public void setFechaCorte(Date fechaCorte) {
        this.fechaCorte = fechaCorte;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdmHistBeneficiarioEntityPK that = (AdmHistBeneficiarioEntityPK) o;
        return Objects.equals(codigoFideicomiso, that.codigoFideicomiso) &&
                Objects.equals(nitBeneficiario, that.nitBeneficiario) &&
                Objects.equals(idTipoBeneficiario, that.idTipoBeneficiario) &&
                Objects.equals(idClaseBeneficio, that.idClaseBeneficio) &&
                Objects.equals(fechaCorte, that.fechaCorte);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigoFideicomiso, nitBeneficiario, idTipoBeneficiario, idClaseBeneficio, fechaCorte);
    }
}
