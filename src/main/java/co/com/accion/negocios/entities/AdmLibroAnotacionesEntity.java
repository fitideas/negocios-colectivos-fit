package co.com.accion.negocios.entities;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;

@Entity
@Table(name = "ADM_LIBRO_ANOTACIONES", schema = "ACCION")
@IdClass(AdmLibroAnotacionesEntityPK.class)
public class AdmLibroAnotacionesEntity {
    private String codigoFideicomiso;
    private long seqEvento;
    private Date fechaEvento;
    private String nombreFuncionario;
    private String aplicaRndCuentas;
    private Long tipoEvento;
    private String estadoEvento;
    private String usuario;
    private Date fechaRegistro;
    private String concluciones;
    private String aplica;
    private Integer nroDocumento;
    private Long terceroEvento;
    private Long valorEvento;
    private String cargoFuncionario;
    private String emailFuncionario;

    @Id
    @Column(name = "CODIGO_FIDEICOMISO")
    public String getCodigoFideicomiso() {
        return codigoFideicomiso;
    }

    public void setCodigoFideicomiso(String codigoFideicomiso) {
        this.codigoFideicomiso = codigoFideicomiso;
    }

    @Id
    @Column(name = "SEQ_EVENTO")
    public long getSeqEvento() {
        return seqEvento;
    }

    public void setSeqEvento(long seqEvento) {
        this.seqEvento = seqEvento;
    }

    @Basic
    @Column(name = "FECHA_EVENTO")
    public Date getFechaEvento() {
        return fechaEvento;
    }

    public void setFechaEvento(Date fechaEvento) {
        this.fechaEvento = fechaEvento;
    }

    @Basic
    @Column(name = "NOMBRE_FUNCIONARIO")
    public String getNombreFuncionario() {
        return nombreFuncionario;
    }

    public void setNombreFuncionario(String nombreFuncionario) {
        this.nombreFuncionario = nombreFuncionario;
    }

    @Basic
    @Column(name = "APLICA_RND_CUENTAS")
    public String getAplicaRndCuentas() {
        return aplicaRndCuentas;
    }

    public void setAplicaRndCuentas(String aplicaRndCuentas) {
        this.aplicaRndCuentas = aplicaRndCuentas;
    }

    @Basic
    @Column(name = "TIPO_EVENTO")
    public Long getTipoEvento() {
        return tipoEvento;
    }

    public void setTipoEvento(Long tipoEvento) {
        this.tipoEvento = tipoEvento;
    }

    @Basic
    @Column(name = "ESTADO_EVENTO")
    public String getEstadoEvento() {
        return estadoEvento;
    }

    public void setEstadoEvento(String estadoEvento) {
        this.estadoEvento = estadoEvento;
    }

    @Basic
    @Column(name = "USUARIO")
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    @Basic
    @Column(name = "FECHA_REGISTRO")
    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Basic
    @Column(name = "CONCLUCIONES")
    public String getConcluciones() {
        return concluciones;
    }

    public void setConcluciones(String concluciones) {
        this.concluciones = concluciones;
    }

    @Basic
    @Column(name = "APLICA")
    public String getAplica() {
        return aplica;
    }

    public void setAplica(String aplica) {
        this.aplica = aplica;
    }

    @Basic
    @Column(name = "NRO_DOCUMENTO")
    public Integer getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(Integer nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    @Basic
    @Column(name = "TERCERO_EVENTO")
    public Long getTerceroEvento() {
        return terceroEvento;
    }

    public void setTerceroEvento(Long terceroEvento) {
        this.terceroEvento = terceroEvento;
    }

    @Basic
    @Column(name = "VALOR_EVENTO")
    public Long getValorEvento() {
        return valorEvento;
    }

    public void setValorEvento(Long valorEvento) {
        this.valorEvento = valorEvento;
    }

    @Basic
    @Column(name = "CARGO_FUNCIONARIO")
    public String getCargoFuncionario() {
        return cargoFuncionario;
    }

    public void setCargoFuncionario(String cargoFuncionario) {
        this.cargoFuncionario = cargoFuncionario;
    }

    @Basic
    @Column(name = "EMAIL_FUNCIONARIO")
    public String getEmailFuncionario() {
        return emailFuncionario;
    }

    public void setEmailFuncionario(String emailFuncionario) {
        this.emailFuncionario = emailFuncionario;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdmLibroAnotacionesEntity that = (AdmLibroAnotacionesEntity) o;

        if (seqEvento != that.seqEvento) return false;
        if (codigoFideicomiso != null ? !codigoFideicomiso.equals(that.codigoFideicomiso) : that.codigoFideicomiso != null)
            return false;
        if (fechaEvento != null ? !fechaEvento.equals(that.fechaEvento) : that.fechaEvento != null) return false;
        if (nombreFuncionario != null ? !nombreFuncionario.equals(that.nombreFuncionario) : that.nombreFuncionario != null)
            return false;
        if (aplicaRndCuentas != null ? !aplicaRndCuentas.equals(that.aplicaRndCuentas) : that.aplicaRndCuentas != null)
            return false;
        if (tipoEvento != null ? !tipoEvento.equals(that.tipoEvento) : that.tipoEvento != null) return false;
        if (estadoEvento != null ? !estadoEvento.equals(that.estadoEvento) : that.estadoEvento != null) return false;
        if (usuario != null ? !usuario.equals(that.usuario) : that.usuario != null) return false;
        if (fechaRegistro != null ? !fechaRegistro.equals(that.fechaRegistro) : that.fechaRegistro != null)
            return false;
        if (concluciones != null ? !concluciones.equals(that.concluciones) : that.concluciones != null) return false;
        if (aplica != null ? !aplica.equals(that.aplica) : that.aplica != null) return false;
        if (nroDocumento != null ? !nroDocumento.equals(that.nroDocumento) : that.nroDocumento != null) return false;
        if (terceroEvento != null ? !terceroEvento.equals(that.terceroEvento) : that.terceroEvento != null)
            return false;
        if (valorEvento != null ? !valorEvento.equals(that.valorEvento) : that.valorEvento != null) return false;
        if (cargoFuncionario != null ? !cargoFuncionario.equals(that.cargoFuncionario) : that.cargoFuncionario != null)
            return false;
        if (emailFuncionario != null ? !emailFuncionario.equals(that.emailFuncionario) : that.emailFuncionario != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = codigoFideicomiso != null ? codigoFideicomiso.hashCode() : 0;
        result = 31 * result + (int) (seqEvento ^ (seqEvento >>> 32));
        result = 31 * result + (fechaEvento != null ? fechaEvento.hashCode() : 0);
        result = 31 * result + (nombreFuncionario != null ? nombreFuncionario.hashCode() : 0);
        result = 31 * result + (aplicaRndCuentas != null ? aplicaRndCuentas.hashCode() : 0);
        result = 31 * result + (tipoEvento != null ? tipoEvento.hashCode() : 0);
        result = 31 * result + (estadoEvento != null ? estadoEvento.hashCode() : 0);
        result = 31 * result + (usuario != null ? usuario.hashCode() : 0);
        result = 31 * result + (fechaRegistro != null ? fechaRegistro.hashCode() : 0);
        result = 31 * result + (concluciones != null ? concluciones.hashCode() : 0);
        result = 31 * result + (aplica != null ? aplica.hashCode() : 0);
        result = 31 * result + (nroDocumento != null ? nroDocumento.hashCode() : 0);
        result = 31 * result + (terceroEvento != null ? terceroEvento.hashCode() : 0);
        result = 31 * result + (valorEvento != null ? valorEvento.hashCode() : 0);
        result = 31 * result + (cargoFuncionario != null ? cargoFuncionario.hashCode() : 0);
        result = 31 * result + (emailFuncionario != null ? emailFuncionario.hashCode() : 0);
        return result;
    }
}
