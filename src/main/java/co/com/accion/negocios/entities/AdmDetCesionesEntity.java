package co.com.accion.negocios.entities;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "ADM_DET_CESIONES", schema = "ACCION", catalog = "")
@IdClass(AdmDetCesionesEntityPK.class)
public class AdmDetCesionesEntity {
    private Long idNegocio;
    private String idContrato;
    private Long idCedente;
    private Long nroCesion;
    private Long seqDetalle;
    private Long idCesionario;
    private double porceActualCedente;
    private Long valorActualCedente;
    private Long porceACeder;
    private Long valorACeder;
    private Long porceCesionario;
    private Long valorCesionario;
    private double porceNuevoCedente;
    private Long valorNuevoCedente;
    private Long codigoProyecto;
    private Long codigoEtapa;
    private String unidadInmb;
    private String nroTrjta;
    private Long vlrSaldoActual;
    private Long nroOficinaOrigen;
    private Long nroOficinaDestino;
    private Long nroEncargoOrigen;
    private Long idFondoOrigen;
    private Long nroEncargoDestino;
    private Long idFondoDestino;
    private Long valorUnidadFondo;
    private String nroRadicacion;
    private String nroOrdenCliente;
    private Date fechaOrdenCliente;
    private String estado;
    private String usuario;
    private Date fechaRegistro;
    private Long codigoTipoInm;
    private Long secuencialUnidad;
    private Integer nroPlanPagos;
    private String gtosCesion;
    private String ctosFinancieros;
    private String modifPlanPagos;
    private String nroTrjtaDestino;
    private Long idTipoBeneficiario;
    private Long idClaseBeneficio;

    @Id
    @Column(name = "ID_NEGOCIO")
    public Long getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Long idNegocio) {
        this.idNegocio = idNegocio;
    }

    @Id
    @Column(name = "ID_CONTRATO")
    public String getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(String idContrato) {
        this.idContrato = idContrato;
    }

    @Id
    @Column(name = "ID_CEDENTE")
    public Long getIdCedente() {
        return idCedente;
    }

    public void setIdCedente(Long idCedente) {
        this.idCedente = idCedente;
    }

    @Id
    @Column(name = "NRO_CESION")
    public Long getNroCesion() {
        return nroCesion;
    }

    public void setNroCesion(Long nroCesion) {
        this.nroCesion = nroCesion;
    }

    @Id
    @Column(name = "SEQ_DETALLE")
    public Long getSeqDetalle() {
        return seqDetalle;
    }

    public void setSeqDetalle(Long seqDetalle) {
        this.seqDetalle = seqDetalle;
    }

    @Basic
    @Column(name = "ID_CESIONARIO")
    public Long getIdCesionario() {
        return idCesionario;
    }

    public void setIdCesionario(Long idCesionario) {
        this.idCesionario = idCesionario;
    }

    @Basic
    @Column(name = "PORCE_ACTUAL_CEDENTE")
    public double getPorceActualCedente() {
        return porceActualCedente;
    }

    public void setPorceActualCedente(double porceActualCedente) {
        this.porceActualCedente = porceActualCedente;
    }

    @Basic
    @Column(name = "VALOR_ACTUAL_CEDENTE")
    public Long getValorActualCedente() {
        return valorActualCedente;
    }

    public void setValorActualCedente(Long valorActualCedente) {
        this.valorActualCedente = valorActualCedente;
    }

    @Basic
    @Column(name = "PORCE_A_CEDER")
    public Long getPorceACeder() {
        return porceACeder;
    }

    public void setPorceACeder(Long porceACeder) {
        this.porceACeder = porceACeder;
    }

    @Basic
    @Column(name = "VALOR_A_CEDER")
    public Long getValorACeder() {
        return valorACeder;
    }

    public void setValorACeder(Long valorACeder) {
        this.valorACeder = valorACeder;
    }

    @Basic
    @Column(name = "PORCE_CESIONARIO")
    public Long getPorceCesionario() {
        return porceCesionario;
    }

    public void setPorceCesionario(Long porceCesionario) {
        this.porceCesionario = porceCesionario;
    }

    @Basic
    @Column(name = "VALOR_CESIONARIO")
    public Long getValorCesionario() {
        return valorCesionario;
    }

    public void setValorCesionario(Long valorCesionario) {
        this.valorCesionario = valorCesionario;
    }

    @Basic
    @Column(name = "PORCE_NUEVO_CEDENTE")
    public double getPorceNuevoCedente() {
        return porceNuevoCedente;
    }

    public void setPorceNuevoCedente(double porceNuevoCedente) {
        this.porceNuevoCedente = porceNuevoCedente;
    }

    @Basic
    @Column(name = "VALOR_NUEVO_CEDENTE")
    public Long getValorNuevoCedente() {
        return valorNuevoCedente;
    }

    public void setValorNuevoCedente(Long valorNuevoCedente) {
        this.valorNuevoCedente = valorNuevoCedente;
    }

    @Basic
    @Column(name = "CODIGO_PROYECTO")
    public Long getCodigoProyecto() {
        return codigoProyecto;
    }

    public void setCodigoProyecto(Long codigoProyecto) {
        this.codigoProyecto = codigoProyecto;
    }

    @Basic
    @Column(name = "CODIGO_ETAPA")
    public Long getCodigoEtapa() {
        return codigoEtapa;
    }

    public void setCodigoEtapa(Long codigoEtapa) {
        this.codigoEtapa = codigoEtapa;
    }

    @Basic
    @Column(name = "UNIDAD_INMB")
    public String getUnidadInmb() {
        return unidadInmb;
    }

    public void setUnidadInmb(String unidadInmb) {
        this.unidadInmb = unidadInmb;
    }

    @Basic
    @Column(name = "NRO_TRJTA")
    public String getNroTrjta() {
        return nroTrjta;
    }

    public void setNroTrjta(String nroTrjta) {
        this.nroTrjta = nroTrjta;
    }

    @Basic
    @Column(name = "VLR_SALDO_ACTUAL")
    public Long getVlrSaldoActual() {
        return vlrSaldoActual;
    }

    public void setVlrSaldoActual(Long vlrSaldoActual) {
        this.vlrSaldoActual = vlrSaldoActual;
    }

    @Basic
    @Column(name = "NRO_OFICINA_ORIGEN")
    public Long getNroOficinaOrigen() {
        return nroOficinaOrigen;
    }

    public void setNroOficinaOrigen(Long nroOficinaOrigen) {
        this.nroOficinaOrigen = nroOficinaOrigen;
    }

    @Basic
    @Column(name = "NRO_OFICINA_DESTINO")
    public Long getNroOficinaDestino() {
        return nroOficinaDestino;
    }

    public void setNroOficinaDestino(Long nroOficinaDestino) {
        this.nroOficinaDestino = nroOficinaDestino;
    }

    @Basic
    @Column(name = "NRO_ENCARGO_ORIGEN")
    public Long getNroEncargoOrigen() {
        return nroEncargoOrigen;
    }

    public void setNroEncargoOrigen(Long nroEncargoOrigen) {
        this.nroEncargoOrigen = nroEncargoOrigen;
    }

    @Basic
    @Column(name = "ID_FONDO_ORIGEN")
    public Long getIdFondoOrigen() {
        return idFondoOrigen;
    }

    public void setIdFondoOrigen(Long idFondoOrigen) {
        this.idFondoOrigen = idFondoOrigen;
    }

    @Basic
    @Column(name = "NRO_ENCARGO_DESTINO")
    public Long getNroEncargoDestino() {
        return nroEncargoDestino;
    }

    public void setNroEncargoDestino(Long nroEncargoDestino) {
        this.nroEncargoDestino = nroEncargoDestino;
    }

    @Basic
    @Column(name = "ID_FONDO_DESTINO")
    public Long getIdFondoDestino() {
        return idFondoDestino;
    }

    public void setIdFondoDestino(Long idFondoDestino) {
        this.idFondoDestino = idFondoDestino;
    }

    @Basic
    @Column(name = "VALOR_UNIDAD_FONDO")
    public Long getValorUnidadFondo() {
        return valorUnidadFondo;
    }

    public void setValorUnidadFondo(Long valorUnidadFondo) {
        this.valorUnidadFondo = valorUnidadFondo;
    }

    @Basic
    @Column(name = "NRO_RADICACION")
    public String getNroRadicacion() {
        return nroRadicacion;
    }

    public void setNroRadicacion(String nroRadicacion) {
        this.nroRadicacion = nroRadicacion;
    }

    @Basic
    @Column(name = "NRO_ORDEN_CLIENTE")
    public String getNroOrdenCliente() {
        return nroOrdenCliente;
    }

    public void setNroOrdenCliente(String nroOrdenCliente) {
        this.nroOrdenCliente = nroOrdenCliente;
    }

    @Basic
    @Column(name = "FECHA_ORDEN_CLIENTE")
    public Date getFechaOrdenCliente() {
        return fechaOrdenCliente;
    }

    public void setFechaOrdenCliente(Date fechaOrdenCliente) {
        this.fechaOrdenCliente = fechaOrdenCliente;
    }

    @Basic
    @Column(name = "ESTADO")
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Basic
    @Column(name = "USUARIO")
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    @Basic
    @Column(name = "FECHA_REGISTRO")
    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Basic
    @Column(name = "CODIGO_TIPO_INM")
    public Long getCodigoTipoInm() {
        return codigoTipoInm;
    }

    public void setCodigoTipoInm(Long codigoTipoInm) {
        this.codigoTipoInm = codigoTipoInm;
    }

    @Basic
    @Column(name = "SECUENCIAL_UNIDAD")
    public Long getSecuencialUnidad() {
        return secuencialUnidad;
    }

    public void setSecuencialUnidad(Long secuencialUnidad) {
        this.secuencialUnidad = secuencialUnidad;
    }

    @Basic
    @Column(name = "NRO_PLAN_PAGOS")
    public Integer getNroPlanPagos() {
        return nroPlanPagos;
    }

    public void setNroPlanPagos(Integer nroPlanPagos) {
        this.nroPlanPagos = nroPlanPagos;
    }

    @Basic
    @Column(name = "GTOS_CESION")
    public String getGtosCesion() {
        return gtosCesion;
    }

    public void setGtosCesion(String gtosCesion) {
        this.gtosCesion = gtosCesion;
    }

    @Basic
    @Column(name = "CTOS_FINANCIEROS")
    public String getCtosFinancieros() {
        return ctosFinancieros;
    }

    public void setCtosFinancieros(String ctosFinancieros) {
        this.ctosFinancieros = ctosFinancieros;
    }

    @Basic
    @Column(name = "MODIF_PLAN_PAGOS")
    public String getModifPlanPagos() {
        return modifPlanPagos;
    }

    public void setModifPlanPagos(String modifPlanPagos) {
        this.modifPlanPagos = modifPlanPagos;
    }

    @Basic
    @Column(name = "NRO_TRJTA_DESTINO")
    public String getNroTrjtaDestino() {
        return nroTrjtaDestino;
    }

    public void setNroTrjtaDestino(String nroTrjtaDestino) {
        this.nroTrjtaDestino = nroTrjtaDestino;
    }

    @Basic
    @Column(name = "ID_TIPO_BENEFICIARIO")
    public Long getIdTipoBeneficiario() {
        return idTipoBeneficiario;
    }

    public void setIdTipoBeneficiario(Long idTipoBeneficiario) {
        this.idTipoBeneficiario = idTipoBeneficiario;
    }

    @Basic
    @Column(name = "ID_CLASE_BENEFICIO")
    public Long getIdClaseBeneficio() {
        return idClaseBeneficio;
    }

    public void setIdClaseBeneficio(Long idClaseBeneficio) {
        this.idClaseBeneficio = idClaseBeneficio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdmDetCesionesEntity that = (AdmDetCesionesEntity) o;
        return Objects.equals(idNegocio, that.idNegocio) &&
                Objects.equals(idContrato, that.idContrato) &&
                Objects.equals(idCedente, that.idCedente) &&
                Objects.equals(nroCesion, that.nroCesion) &&
                Objects.equals(seqDetalle, that.seqDetalle) &&
                Objects.equals(idCesionario, that.idCesionario) &&
                Objects.equals(porceActualCedente, that.porceActualCedente) &&
                Objects.equals(valorActualCedente, that.valorActualCedente) &&
                Objects.equals(porceACeder, that.porceACeder) &&
                Objects.equals(valorACeder, that.valorACeder) &&
                Objects.equals(porceCesionario, that.porceCesionario) &&
                Objects.equals(valorCesionario, that.valorCesionario) &&
                Objects.equals(porceNuevoCedente, that.porceNuevoCedente) &&
                Objects.equals(valorNuevoCedente, that.valorNuevoCedente) &&
                Objects.equals(codigoProyecto, that.codigoProyecto) &&
                Objects.equals(codigoEtapa, that.codigoEtapa) &&
                Objects.equals(unidadInmb, that.unidadInmb) &&
                Objects.equals(nroTrjta, that.nroTrjta) &&
                Objects.equals(vlrSaldoActual, that.vlrSaldoActual) &&
                Objects.equals(nroOficinaOrigen, that.nroOficinaOrigen) &&
                Objects.equals(nroOficinaDestino, that.nroOficinaDestino) &&
                Objects.equals(nroEncargoOrigen, that.nroEncargoOrigen) &&
                Objects.equals(idFondoOrigen, that.idFondoOrigen) &&
                Objects.equals(nroEncargoDestino, that.nroEncargoDestino) &&
                Objects.equals(idFondoDestino, that.idFondoDestino) &&
                Objects.equals(valorUnidadFondo, that.valorUnidadFondo) &&
                Objects.equals(nroRadicacion, that.nroRadicacion) &&
                Objects.equals(nroOrdenCliente, that.nroOrdenCliente) &&
                Objects.equals(fechaOrdenCliente, that.fechaOrdenCliente) &&
                Objects.equals(estado, that.estado) &&
                Objects.equals(usuario, that.usuario) &&
                Objects.equals(fechaRegistro, that.fechaRegistro) &&
                Objects.equals(codigoTipoInm, that.codigoTipoInm) &&
                Objects.equals(secuencialUnidad, that.secuencialUnidad) &&
                Objects.equals(nroPlanPagos, that.nroPlanPagos) &&
                Objects.equals(gtosCesion, that.gtosCesion) &&
                Objects.equals(ctosFinancieros, that.ctosFinancieros) &&
                Objects.equals(modifPlanPagos, that.modifPlanPagos) &&
                Objects.equals(nroTrjtaDestino, that.nroTrjtaDestino) &&
                Objects.equals(idTipoBeneficiario, that.idTipoBeneficiario) &&
                Objects.equals(idClaseBeneficio, that.idClaseBeneficio);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idNegocio, idContrato, idCedente, nroCesion, seqDetalle, idCesionario, porceActualCedente, valorActualCedente, porceACeder, valorACeder, porceCesionario, valorCesionario, porceNuevoCedente, valorNuevoCedente, codigoProyecto, codigoEtapa, unidadInmb, nroTrjta, vlrSaldoActual, nroOficinaOrigen, nroOficinaDestino, nroEncargoOrigen, idFondoOrigen, nroEncargoDestino, idFondoDestino, valorUnidadFondo, nroRadicacion, nroOrdenCliente, fechaOrdenCliente, estado, usuario, fechaRegistro, codigoTipoInm, secuencialUnidad, nroPlanPagos, gtosCesion, ctosFinancieros, modifPlanPagos, nroTrjtaDestino, idTipoBeneficiario, idClaseBeneficio);
    }
}
