package co.com.accion.negocios.entities;

import javax.persistence.*;
import java.sql.Time;
import java.util.Arrays;
import java.util.Objects;

@Entity
@Table(name = "CLIENTES", schema = "ACCION")
public class ClientesEntity {
    private long nit;
    private long tipoPersona;
    private Long numeroHijos;
    private String formato;
    private String retencion;
    private String sexo;
    private String nombre;
    private String apellido1;
    private String apellido2;
    private String razonSocial;
    private String abreviatura;
    private String nombreComun;
    private Time fechaNacimiento;
    private Time fechaConstitucion;
    private String direccionEnvio;
    private String descripcion;
    private Long telefono;
    private Time fechaUltCambio;
    private Long digitoChequeo;
    private String dirUrl;
    private String tipoSector;
    private String servidorPublico;
    private String manejaRecursosPublicos;
    private String usuario;
    private Long codigoCiuu;
    private String nombre2;
    private String operMonedaExtranjera;
    private String descriOperExtranjera;
    private Long tipoMonedaExtranj;
    private Time fechaUltActDocu;
    private String actualizoDocumentacion;
    private Long canalDistribucion;
    private Time fechaIngreso;
    private Long canalDistrNiv2;
    private Long canalDistrNiv3;
    private String tipoInversionista;
    private String tipoOcupacion;
    private Long codPaisExpDoc;
    private Long codDptoExpDoc;
    private Long codCiudadExpDoc;
    private Long codPaisNacmto;
    private Long codDptoNacmto;
    private Long codCiudadNacmto;
    private Long codPaisNacio1;
    private Long codDptoNacio1;
    private Long codCiudadNacio1;
    private Long codPaisNacio2;
    private Long codDptoNacio2;
    private Long codCiudadNacio2;
    private String nomEmpLabora;
    private String cargoEmpLabora;
    private Long codCiiuEmpLabora;
    private String contribuyenteIca;
    private String fatcaDiasPermaneciaUsa;
    private String fatcaResidenteEnUsa;
    private String fatcaNacionalidadUsa;
    private String fatcaContratoEntidadUsa;
    private String envioInfEmail;
    private String envioInfSms;
    private String descrFuenteIngresos;
    private String informacionConfirmada;
    private String nombFuncionarioConfirma;
    private String cargoFuncionarioConfirma;
    private Time fechaConfirmaDatos;
    private String nombreEntrevistador;
    private String resultadoEntrevista;
    private Time fechaEntrevista;
    private String descrLugarEntrevista;
    private Long codPaisEntrevista;
    private Long codDptoEntrevista;
    private Long codCiudadEntrevista;
    private String sarlafNombreFuncionario;
    private Time sarlafFecha;
    private String sarlafObservaciones;
    private String sarlafFirma;
    private Time fechaExpDoc;
    private String nroEscritura;
    private Time fechaEscritura;
    private String notariaEscritura;
    private Long codOficinaVinculacion;
    private String autorizaCentralesRiesgo;
    private Long tipoSociedad;
    private byte[] nombreArchivoFirma;
    private byte[] nombreArchivoFoto;
    private Long codCiudadDiligenciamiento;
    private Long codDptoDiligenciamiento;
    private Long codPaisDiligenciamiento;
    private Time fechaDiligenciamiento;
    private String fatcaNroTin;
    private String perfilRiesgo;

    @Id
    @Column(name = "NIT", nullable = false, precision = 0)
    public long getNit() {
        return nit;
    }

    public void setNit(long nit) {
        this.nit = nit;
    }

    @Basic
    @Column(name = "TIPO_PERSONA", nullable = true, precision = 0)
    public long getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(long tipoPersona) { this.tipoPersona = tipoPersona; }

    @Basic
    @Column(name = "NUMERO_HIJOS", nullable = true, precision = 0)
    public Long getNumeroHijos() {
        return numeroHijos;
    }

    public void setNumeroHijos(Long numeroHijos) {
        this.numeroHijos = numeroHijos;
    }

    @Basic
    @Column(name = "FORMATO", nullable = true, length = 20)
    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    @Basic
    @Column(name = "RETENCION", nullable = true, length = 1)
    public String getRetencion() {
        return retencion;
    }

    public void setRetencion(String retencion) {
        this.retencion = retencion;
    }

    @Basic
    @Column(name = "SEXO", nullable = true, length = 1)
    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    @Basic
    @Column(name = "NOMBRE", nullable = true, length = 50)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "APELLIDO1", nullable = true, length = 50)
    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    @Basic
    @Column(name = "APELLIDO2", nullable = true, length = 50)
    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    @Basic
    @Column(name = "RAZON_SOCIAL", nullable = true, length = 200)
    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    @Basic
    @Column(name = "ABREVIATURA", nullable = true, length = 50)
    public String getAbreviatura() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    @Basic
    @Column(name = "NOMBRE_COMUN", nullable = true, length = 200)
    public String getNombreComun() {
        return nombreComun;
    }

    public void setNombreComun(String nombreComun) {
        this.nombreComun = nombreComun;
    }

    @Basic
    @Column(name = "FECHA_NACIMIENTO", nullable = true)
    public Time getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Time fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Basic
    @Column(name = "FECHA_CONSTITUCION", nullable = true)
    public Time getFechaConstitucion() {
        return fechaConstitucion;
    }

    public void setFechaConstitucion(Time fechaConstitucion) {
        this.fechaConstitucion = fechaConstitucion;
    }

    @Basic
    @Column(name = "DIRECCION_ENVIO", nullable = true, length = 250)
    public String getDireccionEnvio() {
        return direccionEnvio;
    }

    public void setDireccionEnvio(String direccionEnvio) {
        this.direccionEnvio = direccionEnvio;
    }

    @Basic
    @Column(name = "DESCRIPCION", nullable = true, length = 256)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "TELEFONO", nullable = true, precision = 0)
    public Long getTelefono() {
        return telefono;
    }

    public void setTelefono(Long telefono) {
        this.telefono = telefono;
    }

    @Basic
    @Column(name = "FECHA_ULT_CAMBIO", nullable = true)
    public Time getFechaUltCambio() {
        return fechaUltCambio;
    }

    public void setFechaUltCambio(Time fechaUltCambio) {
        this.fechaUltCambio = fechaUltCambio;
    }

    @Basic
    @Column(name = "DIGITO_CHEQUEO", nullable = true, precision = 0)
    public Long getDigitoChequeo() {
        return digitoChequeo;
    }

    public void setDigitoChequeo(Long digitoChequeo) {
        this.digitoChequeo = digitoChequeo;
    }

    @Basic
    @Column(name = "DIR_URL", nullable = true, length = 60)
    public String getDirUrl() {
        return dirUrl;
    }

    public void setDirUrl(String dirUrl) {
        this.dirUrl = dirUrl;
    }

    @Basic
    @Column(name = "TIPO_SECTOR", nullable = true, length = 3)
    public String getTipoSector() {
        return tipoSector;
    }

    public void setTipoSector(String tipoSector) {
        this.tipoSector = tipoSector;
    }

    @Basic
    @Column(name = "SERVIDOR_PUBLICO", nullable = true, length = 2)
    public String getServidorPublico() {
        return servidorPublico;
    }

    public void setServidorPublico(String servidorPublico) {
        this.servidorPublico = servidorPublico;
    }

    @Basic
    @Column(name = "MANEJA_RECURSOS_PUBLICOS", nullable = true, length = 2)
    public String getManejaRecursosPublicos() {
        return manejaRecursosPublicos;
    }

    public void setManejaRecursosPublicos(String manejaRecursosPublicos) {
        this.manejaRecursosPublicos = manejaRecursosPublicos;
    }

    @Basic
    @Column(name = "USUARIO", nullable = true, length = 20)
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    @Basic
    @Column(name = "CODIGO_CIUU", nullable = true, precision = 0)
    public Long getCodigoCiuu() {
        return codigoCiuu;
    }

    public void setCodigoCiuu(Long codigoCiuu) {
        this.codigoCiuu = codigoCiuu;
    }

    @Basic
    @Column(name = "NOMBRE_2", nullable = true, length = 50)
    public String getNombre2() {
        return nombre2;
    }

    public void setNombre2(String nombre2) {
        this.nombre2 = nombre2;
    }

    @Basic
    @Column(name = "OPER_MONEDA_EXTRANJERA", nullable = true, length = 2)
    public String getOperMonedaExtranjera() {
        return operMonedaExtranjera;
    }

    public void setOperMonedaExtranjera(String operMonedaExtranjera) {
        this.operMonedaExtranjera = operMonedaExtranjera;
    }

    @Basic
    @Column(name = "DESCRI_OPER_EXTRANJERA", nullable = true, length = 500)
    public String getDescriOperExtranjera() {
        return descriOperExtranjera;
    }

    public void setDescriOperExtranjera(String descriOperExtranjera) {
        this.descriOperExtranjera = descriOperExtranjera;
    }

    @Basic
    @Column(name = "TIPO_MONEDA_EXTRANJ", nullable = true, precision = 0)
    public Long getTipoMonedaExtranj() {
        return tipoMonedaExtranj;
    }

    public void setTipoMonedaExtranj(Long tipoMonedaExtranj) {
        this.tipoMonedaExtranj = tipoMonedaExtranj;
    }

    @Basic
    @Column(name = "FECHA_ULT_ACT_DOCU", nullable = true)
    public Time getFechaUltActDocu() {
        return fechaUltActDocu;
    }

    public void setFechaUltActDocu(Time fechaUltActDocu) {
        this.fechaUltActDocu = fechaUltActDocu;
    }

    @Basic
    @Column(name = "ACTUALIZO_DOCUMENTACION", nullable = true, length = 2)
    public String getActualizoDocumentacion() {
        return actualizoDocumentacion;
    }

    public void setActualizoDocumentacion(String actualizoDocumentacion) {
        this.actualizoDocumentacion = actualizoDocumentacion;
    }

    @Basic
    @Column(name = "CANAL_DISTRIBUCION", nullable = true, precision = 0)
    public Long getCanalDistribucion() {
        return canalDistribucion;
    }

    public void setCanalDistribucion(Long canalDistribucion) {
        this.canalDistribucion = canalDistribucion;
    }

    @Basic
    @Column(name = "FECHA_INGRESO", nullable = true)
    public Time getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Time fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    @Basic
    @Column(name = "CANAL_DISTR_NIV_2", nullable = true, precision = 0)
    public Long getCanalDistrNiv2() {
        return canalDistrNiv2;
    }

    public void setCanalDistrNiv2(Long canalDistrNiv2) {
        this.canalDistrNiv2 = canalDistrNiv2;
    }

    @Basic
    @Column(name = "CANAL_DISTR_NIV_3", nullable = true, precision = 0)
    public Long getCanalDistrNiv3() {
        return canalDistrNiv3;
    }

    public void setCanalDistrNiv3(Long canalDistrNiv3) {
        this.canalDistrNiv3 = canalDistrNiv3;
    }

    @Basic
    @Column(name = "TIPO_INVERSIONISTA", nullable = true, length = 2)
    public String getTipoInversionista() {
        return tipoInversionista;
    }

    public void setTipoInversionista(String tipoInversionista) {
        this.tipoInversionista = tipoInversionista;
    }

    @Basic
    @Column(name = "TIPO_OCUPACION", nullable = true, length = 3)
    public String getTipoOcupacion() {
        return tipoOcupacion;
    }

    public void setTipoOcupacion(String tipoOcupacion) {
        this.tipoOcupacion = tipoOcupacion;
    }

    @Basic
    @Column(name = "COD_PAIS_EXP_DOC", nullable = true, precision = 0)
    public Long getCodPaisExpDoc() {
        return codPaisExpDoc;
    }

    public void setCodPaisExpDoc(Long codPaisExpDoc) {
        this.codPaisExpDoc = codPaisExpDoc;
    }

    @Basic
    @Column(name = "COD_DPTO_EXP_DOC", nullable = true, precision = 0)
    public Long getCodDptoExpDoc() {
        return codDptoExpDoc;
    }

    public void setCodDptoExpDoc(Long codDptoExpDoc) {
        this.codDptoExpDoc = codDptoExpDoc;
    }

    @Basic
    @Column(name = "COD_CIUDAD_EXP_DOC", nullable = true, precision = 0)
    public Long getCodCiudadExpDoc() {
        return codCiudadExpDoc;
    }

    public void setCodCiudadExpDoc(Long codCiudadExpDoc) {
        this.codCiudadExpDoc = codCiudadExpDoc;
    }

    @Basic
    @Column(name = "COD_PAIS_NACMTO", nullable = true, precision = 0)
    public Long getCodPaisNacmto() {
        return codPaisNacmto;
    }

    public void setCodPaisNacmto(Long codPaisNacmto) {
        this.codPaisNacmto = codPaisNacmto;
    }

    @Basic
    @Column(name = "COD_DPTO_NACMTO", nullable = true, precision = 0)
    public Long getCodDptoNacmto() {
        return codDptoNacmto;
    }

    public void setCodDptoNacmto(Long codDptoNacmto) {
        this.codDptoNacmto = codDptoNacmto;
    }

    @Basic
    @Column(name = "COD_CIUDAD_NACMTO", nullable = true, precision = 0)
    public Long getCodCiudadNacmto() {
        return codCiudadNacmto;
    }

    public void setCodCiudadNacmto(Long codCiudadNacmto) {
        this.codCiudadNacmto = codCiudadNacmto;
    }

    @Basic
    @Column(name = "COD_PAIS_NACIO_1", nullable = true, precision = 0)
    public Long getCodPaisNacio1() {
        return codPaisNacio1;
    }

    public void setCodPaisNacio1(Long codPaisNacio1) {
        this.codPaisNacio1 = codPaisNacio1;
    }

    @Basic
    @Column(name = "COD_DPTO_NACIO_1", nullable = true, precision = 0)
    public Long getCodDptoNacio1() {
        return codDptoNacio1;
    }

    public void setCodDptoNacio1(Long codDptoNacio1) {
        this.codDptoNacio1 = codDptoNacio1;
    }

    @Basic
    @Column(name = "COD_CIUDAD_NACIO_1", nullable = true, precision = 0)
    public Long getCodCiudadNacio1() {
        return codCiudadNacio1;
    }

    public void setCodCiudadNacio1(Long codCiudadNacio1) {
        this.codCiudadNacio1 = codCiudadNacio1;
    }

    @Basic
    @Column(name = "COD_PAIS_NACIO_2", nullable = true, precision = 0)
    public Long getCodPaisNacio2() {
        return codPaisNacio2;
    }

    public void setCodPaisNacio2(Long codPaisNacio2) {
        this.codPaisNacio2 = codPaisNacio2;
    }

    @Basic
    @Column(name = "COD_DPTO_NACIO_2", nullable = true, precision = 0)
    public Long getCodDptoNacio2() {
        return codDptoNacio2;
    }

    public void setCodDptoNacio2(Long codDptoNacio2) {
        this.codDptoNacio2 = codDptoNacio2;
    }

    @Basic
    @Column(name = "COD_CIUDAD_NACIO_2", nullable = true, precision = 0)
    public Long getCodCiudadNacio2() {
        return codCiudadNacio2;
    }

    public void setCodCiudadNacio2(Long codCiudadNacio2) {
        this.codCiudadNacio2 = codCiudadNacio2;
    }

    @Basic
    @Column(name = "NOM_EMP_LABORA", nullable = true, length = 100)
    public String getNomEmpLabora() {
        return nomEmpLabora;
    }

    public void setNomEmpLabora(String nomEmpLabora) {
        this.nomEmpLabora = nomEmpLabora;
    }

    @Basic
    @Column(name = "CARGO_EMP_LABORA", nullable = true, length = 100)
    public String getCargoEmpLabora() {
        return cargoEmpLabora;
    }

    public void setCargoEmpLabora(String cargoEmpLabora) {
        this.cargoEmpLabora = cargoEmpLabora;
    }

    @Basic
    @Column(name = "COD_CIIU_EMP_LABORA", nullable = true, precision = 0)
    public Long getCodCiiuEmpLabora() {
        return codCiiuEmpLabora;
    }

    public void setCodCiiuEmpLabora(Long codCiiuEmpLabora) {
        this.codCiiuEmpLabora = codCiiuEmpLabora;
    }

    @Basic
    @Column(name = "CONTRIBUYENTE_ICA", nullable = true, length = 2)
    public String getContribuyenteIca() {
        return contribuyenteIca;
    }

    public void setContribuyenteIca(String contribuyenteIca) {
        this.contribuyenteIca = contribuyenteIca;
    }

    @Basic
    @Column(name = "FATCA_DIAS_PERMANECIA_USA", nullable = true, length = 2)
    public String getFatcaDiasPermaneciaUsa() {
        return fatcaDiasPermaneciaUsa;
    }

    public void setFatcaDiasPermaneciaUsa(String fatcaDiasPermaneciaUsa) {
        this.fatcaDiasPermaneciaUsa = fatcaDiasPermaneciaUsa;
    }

    @Basic
    @Column(name = "FATCA_RESIDENTE_EN_USA", nullable = true, length = 2)
    public String getFatcaResidenteEnUsa() {
        return fatcaResidenteEnUsa;
    }

    public void setFatcaResidenteEnUsa(String fatcaResidenteEnUsa) {
        this.fatcaResidenteEnUsa = fatcaResidenteEnUsa;
    }

    @Basic
    @Column(name = "FATCA_NACIONALIDAD_USA", nullable = true, length = 2)
    public String getFatcaNacionalidadUsa() {
        return fatcaNacionalidadUsa;
    }

    public void setFatcaNacionalidadUsa(String fatcaNacionalidadUsa) {
        this.fatcaNacionalidadUsa = fatcaNacionalidadUsa;
    }

    @Basic
    @Column(name = "FATCA_CONTRATO_ENTIDAD_USA", nullable = true, length = 2)
    public String getFatcaContratoEntidadUsa() {
        return fatcaContratoEntidadUsa;
    }

    public void setFatcaContratoEntidadUsa(String fatcaContratoEntidadUsa) {
        this.fatcaContratoEntidadUsa = fatcaContratoEntidadUsa;
    }

    @Basic
    @Column(name = "ENVIO_INF_EMAIL", nullable = true, length = 2)
    public String getEnvioInfEmail() {
        return envioInfEmail;
    }

    public void setEnvioInfEmail(String envioInfEmail) {
        this.envioInfEmail = envioInfEmail;
    }

    @Basic
    @Column(name = "ENVIO_INF_SMS", nullable = true, length = 2)
    public String getEnvioInfSms() {
        return envioInfSms;
    }

    public void setEnvioInfSms(String envioInfSms) {
        this.envioInfSms = envioInfSms;
    }

    @Basic
    @Column(name = "DESCR_FUENTE_INGRESOS", nullable = true, length = 250)
    public String getDescrFuenteIngresos() {
        return descrFuenteIngresos;
    }

    public void setDescrFuenteIngresos(String descrFuenteIngresos) {
        this.descrFuenteIngresos = descrFuenteIngresos;
    }

    @Basic
    @Column(name = "INFORMACION_CONFIRMADA", nullable = true, length = 2)
    public String getInformacionConfirmada() {
        return informacionConfirmada;
    }

    public void setInformacionConfirmada(String informacionConfirmada) {
        this.informacionConfirmada = informacionConfirmada;
    }

    @Basic
    @Column(name = "NOMB_FUNCIONARIO_CONFIRMA", nullable = true, length = 100)
    public String getNombFuncionarioConfirma() {
        return nombFuncionarioConfirma;
    }

    public void setNombFuncionarioConfirma(String nombFuncionarioConfirma) {
        this.nombFuncionarioConfirma = nombFuncionarioConfirma;
    }

    @Basic
    @Column(name = "CARGO_FUNCIONARIO_CONFIRMA", nullable = true, length = 100)
    public String getCargoFuncionarioConfirma() {
        return cargoFuncionarioConfirma;
    }

    public void setCargoFuncionarioConfirma(String cargoFuncionarioConfirma) {
        this.cargoFuncionarioConfirma = cargoFuncionarioConfirma;
    }

    @Basic
    @Column(name = "FECHA_CONFIRMA_DATOS", nullable = true)
    public Time getFechaConfirmaDatos() {
        return fechaConfirmaDatos;
    }

    public void setFechaConfirmaDatos(Time fechaConfirmaDatos) {
        this.fechaConfirmaDatos = fechaConfirmaDatos;
    }

    @Basic
    @Column(name = "NOMBRE_ENTREVISTADOR", nullable = true, length = 100)
    public String getNombreEntrevistador() {
        return nombreEntrevistador;
    }

    public void setNombreEntrevistador(String nombreEntrevistador) {
        this.nombreEntrevistador = nombreEntrevistador;
    }

    @Basic
    @Column(name = "RESULTADO_ENTREVISTA", nullable = true, length = 250)
    public String getResultadoEntrevista() {
        return resultadoEntrevista;
    }

    public void setResultadoEntrevista(String resultadoEntrevista) {
        this.resultadoEntrevista = resultadoEntrevista;
    }

    @Basic
    @Column(name = "FECHA_ENTREVISTA", nullable = true)
    public Time getFechaEntrevista() {
        return fechaEntrevista;
    }

    public void setFechaEntrevista(Time fechaEntrevista) {
        this.fechaEntrevista = fechaEntrevista;
    }

    @Basic
    @Column(name = "DESCR_LUGAR_ENTREVISTA", nullable = true, length = 100)
    public String getDescrLugarEntrevista() {
        return descrLugarEntrevista;
    }

    public void setDescrLugarEntrevista(String descrLugarEntrevista) {
        this.descrLugarEntrevista = descrLugarEntrevista;
    }

    @Basic
    @Column(name = "COD_PAIS_ENTREVISTA", nullable = true, precision = 0)
    public Long getCodPaisEntrevista() {
        return codPaisEntrevista;
    }

    public void setCodPaisEntrevista(Long codPaisEntrevista) {
        this.codPaisEntrevista = codPaisEntrevista;
    }

    @Basic
    @Column(name = "COD_DPTO_ENTREVISTA", nullable = true, precision = 0)
    public Long getCodDptoEntrevista() {
        return codDptoEntrevista;
    }

    public void setCodDptoEntrevista(Long codDptoEntrevista) {
        this.codDptoEntrevista = codDptoEntrevista;
    }

    @Basic
    @Column(name = "COD_CIUDAD_ENTREVISTA", nullable = true, precision = 0)
    public Long getCodCiudadEntrevista() {
        return codCiudadEntrevista;
    }

    public void setCodCiudadEntrevista(Long codCiudadEntrevista) {
        this.codCiudadEntrevista = codCiudadEntrevista;
    }

    @Basic
    @Column(name = "SARLAF_NOMBRE_FUNCIONARIO", nullable = true, length = 100)
    public String getSarlafNombreFuncionario() {
        return sarlafNombreFuncionario;
    }

    public void setSarlafNombreFuncionario(String sarlafNombreFuncionario) {
        this.sarlafNombreFuncionario = sarlafNombreFuncionario;
    }

    @Basic
    @Column(name = "SARLAF_FECHA", nullable = true)
    public Time getSarlafFecha() {
        return sarlafFecha;
    }

    public void setSarlafFecha(Time sarlafFecha) {
        this.sarlafFecha = sarlafFecha;
    }

    @Basic
    @Column(name = "SARLAF_OBSERVACIONES", nullable = true, length = 2000)
    public String getSarlafObservaciones() {
        return sarlafObservaciones;
    }

    public void setSarlafObservaciones(String sarlafObservaciones) {
        this.sarlafObservaciones = sarlafObservaciones;
    }

    @Basic
    @Column(name = "SARLAF_FIRMA", nullable = true, length = 100)
    public String getSarlafFirma() {
        return sarlafFirma;
    }

    public void setSarlafFirma(String sarlafFirma) {
        this.sarlafFirma = sarlafFirma;
    }

    @Basic
    @Column(name = "FECHA_EXP_DOC", nullable = true)
    public Time getFechaExpDoc() {
        return fechaExpDoc;
    }

    public void setFechaExpDoc(Time fechaExpDoc) {
        this.fechaExpDoc = fechaExpDoc;
    }

    @Basic
    @Column(name = "NRO_ESCRITURA", nullable = true, length = 20)
    public String getNroEscritura() {
        return nroEscritura;
    }

    public void setNroEscritura(String nroEscritura) {
        this.nroEscritura = nroEscritura;
    }

    @Basic
    @Column(name = "FECHA_ESCRITURA", nullable = true)
    public Time getFechaEscritura() {
        return fechaEscritura;
    }

    public void setFechaEscritura(Time fechaEscritura) {
        this.fechaEscritura = fechaEscritura;
    }

    @Basic
    @Column(name = "NOTARIA_ESCRITURA", nullable = true, length = 100)
    public String getNotariaEscritura() {
        return notariaEscritura;
    }

    public void setNotariaEscritura(String notariaEscritura) {
        this.notariaEscritura = notariaEscritura;
    }

    @Basic
    @Column(name = "COD_OFICINA_VINCULACION", nullable = true, precision = 0)
    public Long getCodOficinaVinculacion() {
        return codOficinaVinculacion;
    }

    public void setCodOficinaVinculacion(Long codOficinaVinculacion) {
        this.codOficinaVinculacion = codOficinaVinculacion;
    }

    @Basic
    @Column(name = "AUTORIZA_CENTRALES_RIESGO", nullable = true, length = 2)
    public String getAutorizaCentralesRiesgo() {
        return autorizaCentralesRiesgo;
    }

    public void setAutorizaCentralesRiesgo(String autorizaCentralesRiesgo) {
        this.autorizaCentralesRiesgo = autorizaCentralesRiesgo;
    }

    @Basic
    @Column(name = "TIPO_SOCIEDAD", nullable = true, precision = 0)
    public Long getTipoSociedad() {
        return tipoSociedad;
    }

    public void setTipoSociedad(Long tipoSociedad) {
        this.tipoSociedad = tipoSociedad;
    }

    @Basic
    @Column(name = "NOMBRE_ARCHIVO_FIRMA", nullable = true)
    public byte[] getNombreArchivoFirma() {
        return nombreArchivoFirma;
    }

    public void setNombreArchivoFirma(byte[] nombreArchivoFirma) {
        this.nombreArchivoFirma = nombreArchivoFirma;
    }

    @Basic
    @Column(name = "NOMBRE_ARCHIVO_FOTO", nullable = true)
    public byte[] getNombreArchivoFoto() {
        return nombreArchivoFoto;
    }

    public void setNombreArchivoFoto(byte[] nombreArchivoFoto) {
        this.nombreArchivoFoto = nombreArchivoFoto;
    }

    @Basic
    @Column(name = "COD_CIUDAD_DILIGENCIAMIENTO", nullable = true, precision = 0)
    public Long getCodCiudadDiligenciamiento() {
        return codCiudadDiligenciamiento;
    }

    public void setCodCiudadDiligenciamiento(Long codCiudadDiligenciamiento) {
        this.codCiudadDiligenciamiento = codCiudadDiligenciamiento;
    }

    @Basic
    @Column(name = "COD_DPTO_DILIGENCIAMIENTO", nullable = true, precision = 0)
    public Long getCodDptoDiligenciamiento() {
        return codDptoDiligenciamiento;
    }

    public void setCodDptoDiligenciamiento(Long codDptoDiligenciamiento) {
        this.codDptoDiligenciamiento = codDptoDiligenciamiento;
    }

    @Basic
    @Column(name = "COD_PAIS_DILIGENCIAMIENTO", nullable = true, precision = 0)
    public Long getCodPaisDiligenciamiento() {
        return codPaisDiligenciamiento;
    }

    public void setCodPaisDiligenciamiento(Long codPaisDiligenciamiento) {
        this.codPaisDiligenciamiento = codPaisDiligenciamiento;
    }

    @Basic
    @Column(name = "FECHA_DILIGENCIAMIENTO", nullable = true)
    public Time getFechaDiligenciamiento() {
        return fechaDiligenciamiento;
    }

    public void setFechaDiligenciamiento(Time fechaDiligenciamiento) {
        this.fechaDiligenciamiento = fechaDiligenciamiento;
    }

    @Basic
    @Column(name = "FATCA_NRO_TIN", nullable = true, length = 12)
    public String getFatcaNroTin() {
        return fatcaNroTin;
    }

    public void setFatcaNroTin(String fatcaNroTin) {
        this.fatcaNroTin = fatcaNroTin;
    }

    @Basic
    @Column(name = "PERFIL_RIESGO", nullable = true, length = 3)
    public String getPerfilRiesgo() {
        return perfilRiesgo;
    }

    public void setPerfilRiesgo(String perfilRiesgo) {
        this.perfilRiesgo = perfilRiesgo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientesEntity that = (ClientesEntity) o;
        return nit == that.nit &&
                Objects.equals(numeroHijos, that.numeroHijos) &&
                Objects.equals(formato, that.formato) &&
                Objects.equals(retencion, that.retencion) &&
                Objects.equals(sexo, that.sexo) &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(apellido1, that.apellido1) &&
                Objects.equals(apellido2, that.apellido2) &&
                Objects.equals(razonSocial, that.razonSocial) &&
                Objects.equals(abreviatura, that.abreviatura) &&
                Objects.equals(nombreComun, that.nombreComun) &&
                Objects.equals(fechaNacimiento, that.fechaNacimiento) &&
                Objects.equals(fechaConstitucion, that.fechaConstitucion) &&
                Objects.equals(direccionEnvio, that.direccionEnvio) &&
                Objects.equals(descripcion, that.descripcion) &&
                Objects.equals(telefono, that.telefono) &&
                Objects.equals(fechaUltCambio, that.fechaUltCambio) &&
                Objects.equals(digitoChequeo, that.digitoChequeo) &&
                Objects.equals(dirUrl, that.dirUrl) &&
                Objects.equals(tipoSector, that.tipoSector) &&
                Objects.equals(servidorPublico, that.servidorPublico) &&
                Objects.equals(manejaRecursosPublicos, that.manejaRecursosPublicos) &&
                Objects.equals(usuario, that.usuario) &&
                Objects.equals(codigoCiuu, that.codigoCiuu) &&
                Objects.equals(nombre2, that.nombre2) &&
                Objects.equals(operMonedaExtranjera, that.operMonedaExtranjera) &&
                Objects.equals(descriOperExtranjera, that.descriOperExtranjera) &&
                Objects.equals(tipoMonedaExtranj, that.tipoMonedaExtranj) &&
                Objects.equals(fechaUltActDocu, that.fechaUltActDocu) &&
                Objects.equals(actualizoDocumentacion, that.actualizoDocumentacion) &&
                Objects.equals(canalDistribucion, that.canalDistribucion) &&
                Objects.equals(fechaIngreso, that.fechaIngreso) &&
                Objects.equals(canalDistrNiv2, that.canalDistrNiv2) &&
                Objects.equals(canalDistrNiv3, that.canalDistrNiv3) &&
                Objects.equals(tipoInversionista, that.tipoInversionista) &&
                Objects.equals(tipoOcupacion, that.tipoOcupacion) &&
                Objects.equals(codPaisExpDoc, that.codPaisExpDoc) &&
                Objects.equals(codDptoExpDoc, that.codDptoExpDoc) &&
                Objects.equals(codCiudadExpDoc, that.codCiudadExpDoc) &&
                Objects.equals(codPaisNacmto, that.codPaisNacmto) &&
                Objects.equals(codDptoNacmto, that.codDptoNacmto) &&
                Objects.equals(codCiudadNacmto, that.codCiudadNacmto) &&
                Objects.equals(codPaisNacio1, that.codPaisNacio1) &&
                Objects.equals(codDptoNacio1, that.codDptoNacio1) &&
                Objects.equals(codCiudadNacio1, that.codCiudadNacio1) &&
                Objects.equals(codPaisNacio2, that.codPaisNacio2) &&
                Objects.equals(codDptoNacio2, that.codDptoNacio2) &&
                Objects.equals(codCiudadNacio2, that.codCiudadNacio2) &&
                Objects.equals(nomEmpLabora, that.nomEmpLabora) &&
                Objects.equals(cargoEmpLabora, that.cargoEmpLabora) &&
                Objects.equals(codCiiuEmpLabora, that.codCiiuEmpLabora) &&
                Objects.equals(contribuyenteIca, that.contribuyenteIca) &&
                Objects.equals(fatcaDiasPermaneciaUsa, that.fatcaDiasPermaneciaUsa) &&
                Objects.equals(fatcaResidenteEnUsa, that.fatcaResidenteEnUsa) &&
                Objects.equals(fatcaNacionalidadUsa, that.fatcaNacionalidadUsa) &&
                Objects.equals(fatcaContratoEntidadUsa, that.fatcaContratoEntidadUsa) &&
                Objects.equals(envioInfEmail, that.envioInfEmail) &&
                Objects.equals(envioInfSms, that.envioInfSms) &&
                Objects.equals(descrFuenteIngresos, that.descrFuenteIngresos) &&
                Objects.equals(informacionConfirmada, that.informacionConfirmada) &&
                Objects.equals(nombFuncionarioConfirma, that.nombFuncionarioConfirma) &&
                Objects.equals(cargoFuncionarioConfirma, that.cargoFuncionarioConfirma) &&
                Objects.equals(fechaConfirmaDatos, that.fechaConfirmaDatos) &&
                Objects.equals(nombreEntrevistador, that.nombreEntrevistador) &&
                Objects.equals(resultadoEntrevista, that.resultadoEntrevista) &&
                Objects.equals(fechaEntrevista, that.fechaEntrevista) &&
                Objects.equals(descrLugarEntrevista, that.descrLugarEntrevista) &&
                Objects.equals(codPaisEntrevista, that.codPaisEntrevista) &&
                Objects.equals(codDptoEntrevista, that.codDptoEntrevista) &&
                Objects.equals(codCiudadEntrevista, that.codCiudadEntrevista) &&
                Objects.equals(sarlafNombreFuncionario, that.sarlafNombreFuncionario) &&
                Objects.equals(sarlafFecha, that.sarlafFecha) &&
                Objects.equals(sarlafObservaciones, that.sarlafObservaciones) &&
                Objects.equals(sarlafFirma, that.sarlafFirma) &&
                Objects.equals(fechaExpDoc, that.fechaExpDoc) &&
                Objects.equals(nroEscritura, that.nroEscritura) &&
                Objects.equals(fechaEscritura, that.fechaEscritura) &&
                Objects.equals(notariaEscritura, that.notariaEscritura) &&
                Objects.equals(codOficinaVinculacion, that.codOficinaVinculacion) &&
                Objects.equals(autorizaCentralesRiesgo, that.autorizaCentralesRiesgo) &&
                Objects.equals(tipoSociedad, that.tipoSociedad) &&
                Arrays.equals(nombreArchivoFirma, that.nombreArchivoFirma) &&
                Arrays.equals(nombreArchivoFoto, that.nombreArchivoFoto) &&
                Objects.equals(codCiudadDiligenciamiento, that.codCiudadDiligenciamiento) &&
                Objects.equals(codDptoDiligenciamiento, that.codDptoDiligenciamiento) &&
                Objects.equals(codPaisDiligenciamiento, that.codPaisDiligenciamiento) &&
                Objects.equals(fechaDiligenciamiento, that.fechaDiligenciamiento) &&
                Objects.equals(fatcaNroTin, that.fatcaNroTin) &&
                Objects.equals(perfilRiesgo, that.perfilRiesgo);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(nit, numeroHijos, formato, retencion, sexo, nombre, apellido1, apellido2, razonSocial, abreviatura, nombreComun, fechaNacimiento, fechaConstitucion, direccionEnvio, descripcion, telefono, fechaUltCambio, digitoChequeo, dirUrl, tipoSector, servidorPublico, manejaRecursosPublicos, usuario, codigoCiuu, nombre2, operMonedaExtranjera, descriOperExtranjera, tipoMonedaExtranj, fechaUltActDocu, actualizoDocumentacion, canalDistribucion, fechaIngreso, canalDistrNiv2, canalDistrNiv3, tipoInversionista, tipoOcupacion, codPaisExpDoc, codDptoExpDoc, codCiudadExpDoc, codPaisNacmto, codDptoNacmto, codCiudadNacmto, codPaisNacio1, codDptoNacio1, codCiudadNacio1, codPaisNacio2, codDptoNacio2, codCiudadNacio2, nomEmpLabora, cargoEmpLabora, codCiiuEmpLabora, contribuyenteIca, fatcaDiasPermaneciaUsa, fatcaResidenteEnUsa, fatcaNacionalidadUsa, fatcaContratoEntidadUsa, envioInfEmail, envioInfSms, descrFuenteIngresos, informacionConfirmada, nombFuncionarioConfirma, cargoFuncionarioConfirma, fechaConfirmaDatos, nombreEntrevistador, resultadoEntrevista, fechaEntrevista, descrLugarEntrevista, codPaisEntrevista, codDptoEntrevista, codCiudadEntrevista, sarlafNombreFuncionario, sarlafFecha, sarlafObservaciones, sarlafFirma, fechaExpDoc, nroEscritura, fechaEscritura, notariaEscritura, codOficinaVinculacion, autorizaCentralesRiesgo, tipoSociedad, codCiudadDiligenciamiento, codDptoDiligenciamiento, codPaisDiligenciamiento, fechaDiligenciamiento, fatcaNroTin, perfilRiesgo);
        result = 31 * result + Arrays.hashCode(nombreArchivoFirma);
        result = 31 * result + Arrays.hashCode(nombreArchivoFoto);
        return result;
    }
}
