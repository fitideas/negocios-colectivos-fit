package co.com.accion.negocios.entities;

import javax.persistence.*;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "FIDEICOMISOS", schema = "ACCION", catalog = "")
public class FideicomisosEntity {
    private long codigo;
    private String nombreFideicomiso;
    private Time fechaConstitucion;
    private Time fechaVencimiento;
    private Time fechaUltProrroga;
    private Long vigenciaTotalDias;
    private String numeroPagare;
    private Time fechaPagare;
    private Integer numeroEscritura;
    private Long notaria;
    private Integer numeroDocumento;
    private String tipoDocConstitucion;
    private String descripcion;
    private Long porceDeCubrimiento;
    private Long consecutivoCertificado;
    private Long consecutivoBienes;
    private Long porceComisionMes;
    private Long porceComisionTotal;
    private Long numeroSalariosMinimos;
    private Long comisionValorFijo;
    private Long comisionValorInicial;
    private Long periodoRendicionCuentas;
    private Long consecutivoAvaluos;
    private String estadoFideicomiso;
    private Long consecutivoVisitas;
    private String numeroConsecutivoExterno;
    private String numeroCarpetaArchivo;
    private String manejaContratos;
    private String nombreArchivoFirma1;
    private String nombreArchivoFirma2;
    private String nombreArchivoFirma3;
    private String nombreArchivoFirma4;
    private String responsableImpuestos;
    private String nombreArchivoWord;
    private String pathArchivoWord;
    private String usuario;
    private Time fechaRegistro;
    private Long consecutivoPagos;
    private Long valorFideicomiso;
    private Long numeroPagos;
    private String pagosMensual;
    private Long vlrComisionPagos;
    private Time fechaLiquidacion;
    private String generaFactura;
    private Long diasVencimientoFactura;
    private Long consecutivoAnexo;
    private Long nroCajaArchivo;
    private Long subtipoFideicomiso;
    private String copiaControlada;
    private String contabilizaAportesNegocio;
    private String contabilizaAportesSociedad;
    private String contabilizaPagosNegocio;
    private String contabilizaPagosSociedad;
    private String contabilizaFacturasNegocio;
    private String contabilizaFacturasSociedad;
    private String contabilizaRndsNegocio;
    private String contabilizaRndsSociedad;
    private String contabilizaCertNegocio;
    private String contabilizaCertSociedad;
    private Time fechaUltRndCuentas;
    private String modalidadContrato;
    private Long actividadEconomica;
    private String inmobiliario;
    private String sistemaCausacionCtb;
    private Long consecArchRecau;
    private Long consecLoteRecau;
    private String contabilizaPagosImptos;
    private Long codCcostosSociedad;
    private Long codCcostosNegocio;
    private Long seqConfirmaPagos;
    private String calculaIca;
    private Long sucNegocio;
    private String facturoFija;
    private Time fechaCorteCxcFija;
    private String facturoVariable;
    private Time fechaCorteCxcVariable;
    private String obligacionesCtrato;
    private String remuneracionCtrato;
    private String observaciones;
    private String objetoFideicomiso;
    private Long consecutivoCesiones;
    private Long consecutivoDesistimientos;
    private String objetoCtratoVinculacion;
    private String obligacioCtratoVinculacion;
    private Long idAuditor;
    private Long idOperador;
    private String nombreHotel;
    private Long idAdmonInmobiliario;
    private Long montoAutorizacion;

    @Id
    @Column(name = "CODIGO", nullable = false, precision = 0)
    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    @Basic
    @Column(name = "NOMBRE_FIDEICOMISO", nullable = true, length = 100)
    public String getNombreFideicomiso() {
        return nombreFideicomiso;
    }

    public void setNombreFideicomiso(String nombreFideicomiso) {
        this.nombreFideicomiso = nombreFideicomiso;
    }

    @Basic
    @Column(name = "FECHA_CONSTITUCION", nullable = true)
    public Time getFechaConstitucion() {
        return fechaConstitucion;
    }

    public void setFechaConstitucion(Time fechaConstitucion) {
        this.fechaConstitucion = fechaConstitucion;
    }

    @Basic
    @Column(name = "FECHA_VENCIMIENTO", nullable = true)
    public Time getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Time fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    @Basic
    @Column(name = "FECHA_ULT_PRORROGA", nullable = true)
    public Time getFechaUltProrroga() {
        return fechaUltProrroga;
    }

    public void setFechaUltProrroga(Time fechaUltProrroga) {
        this.fechaUltProrroga = fechaUltProrroga;
    }

    @Basic
    @Column(name = "VIGENCIA_TOTAL_DIAS", nullable = true, precision = 0)
    public Long getVigenciaTotalDias() {
        return vigenciaTotalDias;
    }

    public void setVigenciaTotalDias(Long vigenciaTotalDias) {
        this.vigenciaTotalDias = vigenciaTotalDias;
    }

    @Basic
    @Column(name = "NUMERO_PAGARE", nullable = true, length = 7)
    public String getNumeroPagare() {
        return numeroPagare;
    }

    public void setNumeroPagare(String numeroPagare) {
        this.numeroPagare = numeroPagare;
    }

    @Basic
    @Column(name = "FECHA_PAGARE", nullable = true)
    public Time getFechaPagare() {
        return fechaPagare;
    }

    public void setFechaPagare(Time fechaPagare) {
        this.fechaPagare = fechaPagare;
    }

    @Basic
    @Column(name = "NUMERO_ESCRITURA", nullable = true, precision = 0)
    public Integer getNumeroEscritura() {
        return numeroEscritura;
    }

    public void setNumeroEscritura(Integer numeroEscritura) {
        this.numeroEscritura = numeroEscritura;
    }

    @Basic
    @Column(name = "NOTARIA", nullable = true, precision = 0)
    public Long getNotaria() {
        return notaria;
    }

    public void setNotaria(Long notaria) {
        this.notaria = notaria;
    }

    @Basic
    @Column(name = "NUMERO_DOCUMENTO", nullable = true, precision = 0)
    public Integer getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(Integer numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    @Basic
    @Column(name = "TIPO_DOC_CONSTITUCION", nullable = true, length = 2)
    public String getTipoDocConstitucion() {
        return tipoDocConstitucion;
    }

    public void setTipoDocConstitucion(String tipoDocConstitucion) {
        this.tipoDocConstitucion = tipoDocConstitucion;
    }

    @Basic
    @Column(name = "DESCRIPCION", nullable = true, length = 256)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "PORCE_DE_CUBRIMIENTO", nullable = true, precision = 3)
    public Long getPorceDeCubrimiento() {
        return porceDeCubrimiento;
    }

    public void setPorceDeCubrimiento(Long porceDeCubrimiento) {
        this.porceDeCubrimiento = porceDeCubrimiento;
    }

    @Basic
    @Column(name = "CONSECUTIVO_CERTIFICADO", nullable = true, precision = 0)
    public Long getConsecutivoCertificado() {
        return consecutivoCertificado;
    }

    public void setConsecutivoCertificado(Long consecutivoCertificado) {
        this.consecutivoCertificado = consecutivoCertificado;
    }

    @Basic
    @Column(name = "CONSECUTIVO_BIENES", nullable = true, precision = 0)
    public Long getConsecutivoBienes() {
        return consecutivoBienes;
    }

    public void setConsecutivoBienes(Long consecutivoBienes) {
        this.consecutivoBienes = consecutivoBienes;
    }

    @Basic
    @Column(name = "PORCE_COMISION_MES", nullable = true, precision = 5)
    public Long getPorceComisionMes() {
        return porceComisionMes;
    }

    public void setPorceComisionMes(Long porceComisionMes) {
        this.porceComisionMes = porceComisionMes;
    }

    @Basic
    @Column(name = "PORCE_COMISION_TOTAL", nullable = true, precision = 5)
    public Long getPorceComisionTotal() {
        return porceComisionTotal;
    }

    public void setPorceComisionTotal(Long porceComisionTotal) {
        this.porceComisionTotal = porceComisionTotal;
    }

    @Basic
    @Column(name = "NUMERO_SALARIOS_MINIMOS", nullable = true, precision = 2)
    public Long getNumeroSalariosMinimos() {
        return numeroSalariosMinimos;
    }

    public void setNumeroSalariosMinimos(Long numeroSalariosMinimos) {
        this.numeroSalariosMinimos = numeroSalariosMinimos;
    }

    @Basic
    @Column(name = "COMISION_VALOR_FIJO", nullable = true, precision = 2)
    public Long getComisionValorFijo() {
        return comisionValorFijo;
    }

    public void setComisionValorFijo(Long comisionValorFijo) {
        this.comisionValorFijo = comisionValorFijo;
    }

    @Basic
    @Column(name = "COMISION_VALOR_INICIAL", nullable = true, precision = 2)
    public Long getComisionValorInicial() {
        return comisionValorInicial;
    }

    public void setComisionValorInicial(Long comisionValorInicial) {
        this.comisionValorInicial = comisionValorInicial;
    }

    @Basic
    @Column(name = "PERIODO_RENDICION_CUENTAS", nullable = true, precision = 0)
    public Long getPeriodoRendicionCuentas() {
        return periodoRendicionCuentas;
    }

    public void setPeriodoRendicionCuentas(Long periodoRendicionCuentas) {
        this.periodoRendicionCuentas = periodoRendicionCuentas;
    }

    @Basic
    @Column(name = "CONSECUTIVO_AVALUOS", nullable = true, precision = 0)
    public Long getConsecutivoAvaluos() {
        return consecutivoAvaluos;
    }

    public void setConsecutivoAvaluos(Long consecutivoAvaluos) {
        this.consecutivoAvaluos = consecutivoAvaluos;
    }

    @Basic
    @Column(name = "ESTADO_FIDEICOMISO", nullable = true, length = 1)
    public String getEstadoFideicomiso() {
        return estadoFideicomiso;
    }

    public void setEstadoFideicomiso(String estadoFideicomiso) {
        this.estadoFideicomiso = estadoFideicomiso;
    }

    @Basic
    @Column(name = "CONSECUTIVO_VISITAS", nullable = true, precision = 0)
    public Long getConsecutivoVisitas() {
        return consecutivoVisitas;
    }

    public void setConsecutivoVisitas(Long consecutivoVisitas) {
        this.consecutivoVisitas = consecutivoVisitas;
    }

    @Basic
    @Column(name = "NUMERO_CONSECUTIVO_EXTERNO", nullable = true, length = 20)
    public String getNumeroConsecutivoExterno() {
        return numeroConsecutivoExterno;
    }

    public void setNumeroConsecutivoExterno(String numeroConsecutivoExterno) {
        this.numeroConsecutivoExterno = numeroConsecutivoExterno;
    }

    @Basic
    @Column(name = "NUMERO_CARPETA_ARCHIVO", nullable = true, length = 7)
    public String getNumeroCarpetaArchivo() {
        return numeroCarpetaArchivo;
    }

    public void setNumeroCarpetaArchivo(String numeroCarpetaArchivo) {
        this.numeroCarpetaArchivo = numeroCarpetaArchivo;
    }

    @Basic
    @Column(name = "MANEJA_CONTRATOS", nullable = true, length = 2)
    public String getManejaContratos() {
        return manejaContratos;
    }

    public void setManejaContratos(String manejaContratos) {
        this.manejaContratos = manejaContratos;
    }

    @Basic
    @Column(name = "NOMBRE_ARCHIVO_FIRMA1", nullable = true, length = 20)
    public String getNombreArchivoFirma1() {
        return nombreArchivoFirma1;
    }

    public void setNombreArchivoFirma1(String nombreArchivoFirma1) {
        this.nombreArchivoFirma1 = nombreArchivoFirma1;
    }

    @Basic
    @Column(name = "NOMBRE_ARCHIVO_FIRMA2", nullable = true, length = 20)
    public String getNombreArchivoFirma2() {
        return nombreArchivoFirma2;
    }

    public void setNombreArchivoFirma2(String nombreArchivoFirma2) {
        this.nombreArchivoFirma2 = nombreArchivoFirma2;
    }

    @Basic
    @Column(name = "NOMBRE_ARCHIVO_FIRMA3", nullable = true, length = 20)
    public String getNombreArchivoFirma3() {
        return nombreArchivoFirma3;
    }

    public void setNombreArchivoFirma3(String nombreArchivoFirma3) {
        this.nombreArchivoFirma3 = nombreArchivoFirma3;
    }

    @Basic
    @Column(name = "NOMBRE_ARCHIVO_FIRMA4", nullable = true, length = 20)
    public String getNombreArchivoFirma4() {
        return nombreArchivoFirma4;
    }

    public void setNombreArchivoFirma4(String nombreArchivoFirma4) {
        this.nombreArchivoFirma4 = nombreArchivoFirma4;
    }

    @Basic
    @Column(name = "RESPONSABLE_IMPUESTOS", nullable = true, length = 1)
    public String getResponsableImpuestos() {
        return responsableImpuestos;
    }

    public void setResponsableImpuestos(String responsableImpuestos) {
        this.responsableImpuestos = responsableImpuestos;
    }

    @Basic
    @Column(name = "NOMBRE_ARCHIVO_WORD", nullable = true, length = 50)
    public String getNombreArchivoWord() {
        return nombreArchivoWord;
    }

    public void setNombreArchivoWord(String nombreArchivoWord) {
        this.nombreArchivoWord = nombreArchivoWord;
    }

    @Basic
    @Column(name = "PATH_ARCHIVO_WORD", nullable = true, length = 50)
    public String getPathArchivoWord() {
        return pathArchivoWord;
    }

    public void setPathArchivoWord(String pathArchivoWord) {
        this.pathArchivoWord = pathArchivoWord;
    }

    @Basic
    @Column(name = "USUARIO", nullable = true, length = 20)
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    @Basic
    @Column(name = "FECHA_REGISTRO", nullable = true)
    public Time getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Time fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Basic
    @Column(name = "CONSECUTIVO_PAGOS", nullable = true, precision = 0)
    public Long getConsecutivoPagos() {
        return consecutivoPagos;
    }

    public void setConsecutivoPagos(Long consecutivoPagos) {
        this.consecutivoPagos = consecutivoPagos;
    }

    @Basic
    @Column(name = "VALOR_FIDEICOMISO", nullable = true, precision = 2)
    public Long getValorFideicomiso() {
        return valorFideicomiso;
    }

    public void setValorFideicomiso(Long valorFideicomiso) {
        this.valorFideicomiso = valorFideicomiso;
    }

    @Basic
    @Column(name = "NUMERO_PAGOS", nullable = true, precision = 0)
    public Long getNumeroPagos() {
        return numeroPagos;
    }

    public void setNumeroPagos(Long numeroPagos) {
        this.numeroPagos = numeroPagos;
    }

    @Basic
    @Column(name = "PAGOS_MENSUAL", nullable = true, length = 2)
    public String getPagosMensual() {
        return pagosMensual;
    }

    public void setPagosMensual(String pagosMensual) {
        this.pagosMensual = pagosMensual;
    }

    @Basic
    @Column(name = "VLR_COMISION_PAGOS", nullable = true, precision = 2)
    public Long getVlrComisionPagos() {
        return vlrComisionPagos;
    }

    public void setVlrComisionPagos(Long vlrComisionPagos) {
        this.vlrComisionPagos = vlrComisionPagos;
    }

    @Basic
    @Column(name = "FECHA_LIQUIDACION", nullable = true)
    public Time getFechaLiquidacion() {
        return fechaLiquidacion;
    }

    public void setFechaLiquidacion(Time fechaLiquidacion) {
        this.fechaLiquidacion = fechaLiquidacion;
    }

    @Basic
    @Column(name = "GENERA_FACTURA", nullable = true, length = 2)
    public String getGeneraFactura() {
        return generaFactura;
    }

    public void setGeneraFactura(String generaFactura) {
        this.generaFactura = generaFactura;
    }

    @Basic
    @Column(name = "DIAS_VENCIMIENTO_FACTURA", nullable = true, precision = 0)
    public Long getDiasVencimientoFactura() {
        return diasVencimientoFactura;
    }

    public void setDiasVencimientoFactura(Long diasVencimientoFactura) {
        this.diasVencimientoFactura = diasVencimientoFactura;
    }

    @Basic
    @Column(name = "CONSECUTIVO_ANEXO", nullable = true, precision = 0)
    public Long getConsecutivoAnexo() {
        return consecutivoAnexo;
    }

    public void setConsecutivoAnexo(Long consecutivoAnexo) {
        this.consecutivoAnexo = consecutivoAnexo;
    }

    @Basic
    @Column(name = "NRO_CAJA_ARCHIVO", nullable = true, precision = 0)
    public Long getNroCajaArchivo() {
        return nroCajaArchivo;
    }

    public void setNroCajaArchivo(Long nroCajaArchivo) {
        this.nroCajaArchivo = nroCajaArchivo;
    }

    @Basic
    @Column(name = "SUBTIPO_FIDEICOMISO", nullable = true, precision = 0)
    public Long getSubtipoFideicomiso() {
        return subtipoFideicomiso;
    }

    public void setSubtipoFideicomiso(Long subtipoFideicomiso) {
        this.subtipoFideicomiso = subtipoFideicomiso;
    }

    @Basic
    @Column(name = "COPIA_CONTROLADA", nullable = true, length = 2)
    public String getCopiaControlada() {
        return copiaControlada;
    }

    public void setCopiaControlada(String copiaControlada) {
        this.copiaControlada = copiaControlada;
    }

    @Basic
    @Column(name = "CONTABILIZA_APORTES_NEGOCIO", nullable = true, length = 2)
    public String getContabilizaAportesNegocio() {
        return contabilizaAportesNegocio;
    }

    public void setContabilizaAportesNegocio(String contabilizaAportesNegocio) {
        this.contabilizaAportesNegocio = contabilizaAportesNegocio;
    }

    @Basic
    @Column(name = "CONTABILIZA_APORTES_SOCIEDAD", nullable = true, length = 2)
    public String getContabilizaAportesSociedad() {
        return contabilizaAportesSociedad;
    }

    public void setContabilizaAportesSociedad(String contabilizaAportesSociedad) {
        this.contabilizaAportesSociedad = contabilizaAportesSociedad;
    }

    @Basic
    @Column(name = "CONTABILIZA_PAGOS_NEGOCIO", nullable = true, length = 2)
    public String getContabilizaPagosNegocio() {
        return contabilizaPagosNegocio;
    }

    public void setContabilizaPagosNegocio(String contabilizaPagosNegocio) {
        this.contabilizaPagosNegocio = contabilizaPagosNegocio;
    }

    @Basic
    @Column(name = "CONTABILIZA_PAGOS_SOCIEDAD", nullable = true, length = 2)
    public String getContabilizaPagosSociedad() {
        return contabilizaPagosSociedad;
    }

    public void setContabilizaPagosSociedad(String contabilizaPagosSociedad) {
        this.contabilizaPagosSociedad = contabilizaPagosSociedad;
    }

    @Basic
    @Column(name = "CONTABILIZA_FACTURAS_NEGOCIO", nullable = true, length = 2)
    public String getContabilizaFacturasNegocio() {
        return contabilizaFacturasNegocio;
    }

    public void setContabilizaFacturasNegocio(String contabilizaFacturasNegocio) {
        this.contabilizaFacturasNegocio = contabilizaFacturasNegocio;
    }

    @Basic
    @Column(name = "CONTABILIZA_FACTURAS_SOCIEDAD", nullable = true, length = 2)
    public String getContabilizaFacturasSociedad() {
        return contabilizaFacturasSociedad;
    }

    public void setContabilizaFacturasSociedad(String contabilizaFacturasSociedad) {
        this.contabilizaFacturasSociedad = contabilizaFacturasSociedad;
    }

    @Basic
    @Column(name = "CONTABILIZA_RNDS_NEGOCIO", nullable = true, length = 2)
    public String getContabilizaRndsNegocio() {
        return contabilizaRndsNegocio;
    }

    public void setContabilizaRndsNegocio(String contabilizaRndsNegocio) {
        this.contabilizaRndsNegocio = contabilizaRndsNegocio;
    }

    @Basic
    @Column(name = "CONTABILIZA_RNDS_SOCIEDAD", nullable = true, length = 2)
    public String getContabilizaRndsSociedad() {
        return contabilizaRndsSociedad;
    }

    public void setContabilizaRndsSociedad(String contabilizaRndsSociedad) {
        this.contabilizaRndsSociedad = contabilizaRndsSociedad;
    }

    @Basic
    @Column(name = "CONTABILIZA_CERT_NEGOCIO", nullable = true, length = 2)
    public String getContabilizaCertNegocio() {
        return contabilizaCertNegocio;
    }

    public void setContabilizaCertNegocio(String contabilizaCertNegocio) {
        this.contabilizaCertNegocio = contabilizaCertNegocio;
    }

    @Basic
    @Column(name = "CONTABILIZA_CERT_SOCIEDAD", nullable = true, length = 2)
    public String getContabilizaCertSociedad() {
        return contabilizaCertSociedad;
    }

    public void setContabilizaCertSociedad(String contabilizaCertSociedad) {
        this.contabilizaCertSociedad = contabilizaCertSociedad;
    }

    @Basic
    @Column(name = "FECHA_ULT_RND_CUENTAS", nullable = true)
    public Time getFechaUltRndCuentas() {
        return fechaUltRndCuentas;
    }

    public void setFechaUltRndCuentas(Time fechaUltRndCuentas) {
        this.fechaUltRndCuentas = fechaUltRndCuentas;
    }

    @Basic
    @Column(name = "MODALIDAD_CONTRATO", nullable = true, length = 3)
    public String getModalidadContrato() {
        return modalidadContrato;
    }

    public void setModalidadContrato(String modalidadContrato) {
        this.modalidadContrato = modalidadContrato;
    }

    @Basic
    @Column(name = "ACTIVIDAD_ECONOMICA", nullable = true, precision = 0)
    public Long getActividadEconomica() {
        return actividadEconomica;
    }

    public void setActividadEconomica(Long actividadEconomica) {
        this.actividadEconomica = actividadEconomica;
    }

    @Basic
    @Column(name = "INMOBILIARIO", nullable = true, length = 3)
    public String getInmobiliario() {
        return inmobiliario;
    }

    public void setInmobiliario(String inmobiliario) {
        this.inmobiliario = inmobiliario;
    }

    @Basic
    @Column(name = "SISTEMA_CAUSACION_CTB", nullable = true, length = 3)
    public String getSistemaCausacionCtb() {
        return sistemaCausacionCtb;
    }

    public void setSistemaCausacionCtb(String sistemaCausacionCtb) {
        this.sistemaCausacionCtb = sistemaCausacionCtb;
    }

    @Basic
    @Column(name = "CONSEC_ARCH_RECAU", nullable = true, precision = 0)
    public Long getConsecArchRecau() {
        return consecArchRecau;
    }

    public void setConsecArchRecau(Long consecArchRecau) {
        this.consecArchRecau = consecArchRecau;
    }

    @Basic
    @Column(name = "CONSEC_LOTE_RECAU", nullable = true, precision = 0)
    public Long getConsecLoteRecau() {
        return consecLoteRecau;
    }

    public void setConsecLoteRecau(Long consecLoteRecau) {
        this.consecLoteRecau = consecLoteRecau;
    }

    @Basic
    @Column(name = "CONTABILIZA_PAGOS_IMPTOS", nullable = true, length = 2)
    public String getContabilizaPagosImptos() {
        return contabilizaPagosImptos;
    }

    public void setContabilizaPagosImptos(String contabilizaPagosImptos) {
        this.contabilizaPagosImptos = contabilizaPagosImptos;
    }

    @Basic
    @Column(name = "COD_CCOSTOS_SOCIEDAD", nullable = true, precision = 0)
    public Long getCodCcostosSociedad() {
        return codCcostosSociedad;
    }

    public void setCodCcostosSociedad(Long codCcostosSociedad) {
        this.codCcostosSociedad = codCcostosSociedad;
    }

    @Basic
    @Column(name = "COD_CCOSTOS_NEGOCIO", nullable = true, precision = 0)
    public Long getCodCcostosNegocio() {
        return codCcostosNegocio;
    }

    public void setCodCcostosNegocio(Long codCcostosNegocio) {
        this.codCcostosNegocio = codCcostosNegocio;
    }

    @Basic
    @Column(name = "SEQ_CONFIRMA_PAGOS", nullable = true, precision = 0)
    public Long getSeqConfirmaPagos() {
        return seqConfirmaPagos;
    }

    public void setSeqConfirmaPagos(Long seqConfirmaPagos) {
        this.seqConfirmaPagos = seqConfirmaPagos;
    }

    @Basic
    @Column(name = "CALCULA_ICA", nullable = true, length = 1)
    public String getCalculaIca() {
        return calculaIca;
    }

    public void setCalculaIca(String calculaIca) {
        this.calculaIca = calculaIca;
    }

    @Basic
    @Column(name = "SUC_NEGOCIO", nullable = true, precision = 0)
    public Long getSucNegocio() {
        return sucNegocio;
    }

    public void setSucNegocio(Long sucNegocio) {
        this.sucNegocio = sucNegocio;
    }

    @Basic
    @Column(name = "FACTURO_FIJA", nullable = true, length = 2)
    public String getFacturoFija() {
        return facturoFija;
    }

    public void setFacturoFija(String facturoFija) {
        this.facturoFija = facturoFija;
    }

    @Basic
    @Column(name = "FECHA_CORTE_CXC_FIJA", nullable = true)
    public Time getFechaCorteCxcFija() {
        return fechaCorteCxcFija;
    }

    public void setFechaCorteCxcFija(Time fechaCorteCxcFija) {
        this.fechaCorteCxcFija = fechaCorteCxcFija;
    }

    @Basic
    @Column(name = "FACTURO_VARIABLE", nullable = true, length = 2)
    public String getFacturoVariable() {
        return facturoVariable;
    }

    public void setFacturoVariable(String facturoVariable) {
        this.facturoVariable = facturoVariable;
    }

    @Basic
    @Column(name = "FECHA_CORTE_CXC_VARIABLE", nullable = true)
    public Time getFechaCorteCxcVariable() {
        return fechaCorteCxcVariable;
    }

    public void setFechaCorteCxcVariable(Time fechaCorteCxcVariable) {
        this.fechaCorteCxcVariable = fechaCorteCxcVariable;
    }

    @Basic
    @Column(name = "OBLIGACIONES_CTRATO", nullable = true)
    public String getObligacionesCtrato() {
        return obligacionesCtrato;
    }

    public void setObligacionesCtrato(String obligacionesCtrato) {
        this.obligacionesCtrato = obligacionesCtrato;
    }

    @Basic
    @Column(name = "REMUNERACION_CTRATO", nullable = true)
    public String getRemuneracionCtrato() {
        return remuneracionCtrato;
    }

    public void setRemuneracionCtrato(String remuneracionCtrato) {
        this.remuneracionCtrato = remuneracionCtrato;
    }

    @Basic
    @Column(name = "OBSERVACIONES", nullable = true)
    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Basic
    @Column(name = "OBJETO_FIDEICOMISO", nullable = true)
    public String getObjetoFideicomiso() {
        return objetoFideicomiso;
    }

    public void setObjetoFideicomiso(String objetoFideicomiso) {
        this.objetoFideicomiso = objetoFideicomiso;
    }

    @Basic
    @Column(name = "CONSECUTIVO_CESIONES", nullable = true, precision = 0)
    public Long getConsecutivoCesiones() {
        return consecutivoCesiones;
    }

    public void setConsecutivoCesiones(Long consecutivoCesiones) {
        this.consecutivoCesiones = consecutivoCesiones;
    }

    @Basic
    @Column(name = "CONSECUTIVO_DESISTIMIENTOS", nullable = true, precision = 0)
    public Long getConsecutivoDesistimientos() {
        return consecutivoDesistimientos;
    }

    public void setConsecutivoDesistimientos(Long consecutivoDesistimientos) {
        this.consecutivoDesistimientos = consecutivoDesistimientos;
    }

    @Basic
    @Column(name = "OBJETO_CTRATO_VINCULACION", nullable = true)
    public String getObjetoCtratoVinculacion() {
        return objetoCtratoVinculacion;
    }

    public void setObjetoCtratoVinculacion(String objetoCtratoVinculacion) {
        this.objetoCtratoVinculacion = objetoCtratoVinculacion;
    }

    @Basic
    @Column(name = "OBLIGACIO_CTRATO_VINCULACION", nullable = true)
    public String getObligacioCtratoVinculacion() {
        return obligacioCtratoVinculacion;
    }

    public void setObligacioCtratoVinculacion(String obligacioCtratoVinculacion) {
        this.obligacioCtratoVinculacion = obligacioCtratoVinculacion;
    }

    @Basic
    @Column(name = "ID_AUDITOR", nullable = true, precision = 0)
    public Long getIdAuditor() {
        return idAuditor;
    }

    public void setIdAuditor(Long idAuditor) {
        this.idAuditor = idAuditor;
    }

    @Basic
    @Column(name = "ID_OPERADOR", nullable = true, precision = 0)
    public Long getIdOperador() {
        return idOperador;
    }

    public void setIdOperador(Long idOperador) {
        this.idOperador = idOperador;
    }

    @Basic
    @Column(name = "NOMBRE_HOTEL", nullable = true, length = 100)
    public String getNombreHotel() {
        return nombreHotel;
    }

    public void setNombreHotel(String nombreHotel) {
        this.nombreHotel = nombreHotel;
    }

    @Basic
    @Column(name = "ID_ADMON_INMOBILIARIO", nullable = true, precision = 0)
    public Long getIdAdmonInmobiliario() {
        return idAdmonInmobiliario;
    }

    public void setIdAdmonInmobiliario(Long idAdmonInmobiliario) {
        this.idAdmonInmobiliario = idAdmonInmobiliario;
    }

    @Basic
    @Column(name = "MONTO_AUTORIZACION", nullable = true, precision = 0)
    public Long getMontoAutorizacion() {
        return montoAutorizacion;
    }

    public void setMontoAutorizacion(Long montoAutorizacion) {
        this.montoAutorizacion = montoAutorizacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FideicomisosEntity that = (FideicomisosEntity) o;
        return codigo == that.codigo &&
                Objects.equals(nombreFideicomiso, that.nombreFideicomiso) &&
                Objects.equals(fechaConstitucion, that.fechaConstitucion) &&
                Objects.equals(fechaVencimiento, that.fechaVencimiento) &&
                Objects.equals(fechaUltProrroga, that.fechaUltProrroga) &&
                Objects.equals(vigenciaTotalDias, that.vigenciaTotalDias) &&
                Objects.equals(numeroPagare, that.numeroPagare) &&
                Objects.equals(fechaPagare, that.fechaPagare) &&
                Objects.equals(numeroEscritura, that.numeroEscritura) &&
                Objects.equals(notaria, that.notaria) &&
                Objects.equals(numeroDocumento, that.numeroDocumento) &&
                Objects.equals(tipoDocConstitucion, that.tipoDocConstitucion) &&
                Objects.equals(descripcion, that.descripcion) &&
                Objects.equals(porceDeCubrimiento, that.porceDeCubrimiento) &&
                Objects.equals(consecutivoCertificado, that.consecutivoCertificado) &&
                Objects.equals(consecutivoBienes, that.consecutivoBienes) &&
                Objects.equals(porceComisionMes, that.porceComisionMes) &&
                Objects.equals(porceComisionTotal, that.porceComisionTotal) &&
                Objects.equals(numeroSalariosMinimos, that.numeroSalariosMinimos) &&
                Objects.equals(comisionValorFijo, that.comisionValorFijo) &&
                Objects.equals(comisionValorInicial, that.comisionValorInicial) &&
                Objects.equals(periodoRendicionCuentas, that.periodoRendicionCuentas) &&
                Objects.equals(consecutivoAvaluos, that.consecutivoAvaluos) &&
                Objects.equals(estadoFideicomiso, that.estadoFideicomiso) &&
                Objects.equals(consecutivoVisitas, that.consecutivoVisitas) &&
                Objects.equals(numeroConsecutivoExterno, that.numeroConsecutivoExterno) &&
                Objects.equals(numeroCarpetaArchivo, that.numeroCarpetaArchivo) &&
                Objects.equals(manejaContratos, that.manejaContratos) &&
                Objects.equals(nombreArchivoFirma1, that.nombreArchivoFirma1) &&
                Objects.equals(nombreArchivoFirma2, that.nombreArchivoFirma2) &&
                Objects.equals(nombreArchivoFirma3, that.nombreArchivoFirma3) &&
                Objects.equals(nombreArchivoFirma4, that.nombreArchivoFirma4) &&
                Objects.equals(responsableImpuestos, that.responsableImpuestos) &&
                Objects.equals(nombreArchivoWord, that.nombreArchivoWord) &&
                Objects.equals(pathArchivoWord, that.pathArchivoWord) &&
                Objects.equals(usuario, that.usuario) &&
                Objects.equals(fechaRegistro, that.fechaRegistro) &&
                Objects.equals(consecutivoPagos, that.consecutivoPagos) &&
                Objects.equals(valorFideicomiso, that.valorFideicomiso) &&
                Objects.equals(numeroPagos, that.numeroPagos) &&
                Objects.equals(pagosMensual, that.pagosMensual) &&
                Objects.equals(vlrComisionPagos, that.vlrComisionPagos) &&
                Objects.equals(fechaLiquidacion, that.fechaLiquidacion) &&
                Objects.equals(generaFactura, that.generaFactura) &&
                Objects.equals(diasVencimientoFactura, that.diasVencimientoFactura) &&
                Objects.equals(consecutivoAnexo, that.consecutivoAnexo) &&
                Objects.equals(nroCajaArchivo, that.nroCajaArchivo) &&
                Objects.equals(subtipoFideicomiso, that.subtipoFideicomiso) &&
                Objects.equals(copiaControlada, that.copiaControlada) &&
                Objects.equals(contabilizaAportesNegocio, that.contabilizaAportesNegocio) &&
                Objects.equals(contabilizaAportesSociedad, that.contabilizaAportesSociedad) &&
                Objects.equals(contabilizaPagosNegocio, that.contabilizaPagosNegocio) &&
                Objects.equals(contabilizaPagosSociedad, that.contabilizaPagosSociedad) &&
                Objects.equals(contabilizaFacturasNegocio, that.contabilizaFacturasNegocio) &&
                Objects.equals(contabilizaFacturasSociedad, that.contabilizaFacturasSociedad) &&
                Objects.equals(contabilizaRndsNegocio, that.contabilizaRndsNegocio) &&
                Objects.equals(contabilizaRndsSociedad, that.contabilizaRndsSociedad) &&
                Objects.equals(contabilizaCertNegocio, that.contabilizaCertNegocio) &&
                Objects.equals(contabilizaCertSociedad, that.contabilizaCertSociedad) &&
                Objects.equals(fechaUltRndCuentas, that.fechaUltRndCuentas) &&
                Objects.equals(modalidadContrato, that.modalidadContrato) &&
                Objects.equals(actividadEconomica, that.actividadEconomica) &&
                Objects.equals(inmobiliario, that.inmobiliario) &&
                Objects.equals(sistemaCausacionCtb, that.sistemaCausacionCtb) &&
                Objects.equals(consecArchRecau, that.consecArchRecau) &&
                Objects.equals(consecLoteRecau, that.consecLoteRecau) &&
                Objects.equals(contabilizaPagosImptos, that.contabilizaPagosImptos) &&
                Objects.equals(codCcostosSociedad, that.codCcostosSociedad) &&
                Objects.equals(codCcostosNegocio, that.codCcostosNegocio) &&
                Objects.equals(seqConfirmaPagos, that.seqConfirmaPagos) &&
                Objects.equals(calculaIca, that.calculaIca) &&
                Objects.equals(sucNegocio, that.sucNegocio) &&
                Objects.equals(facturoFija, that.facturoFija) &&
                Objects.equals(fechaCorteCxcFija, that.fechaCorteCxcFija) &&
                Objects.equals(facturoVariable, that.facturoVariable) &&
                Objects.equals(fechaCorteCxcVariable, that.fechaCorteCxcVariable) &&
                Objects.equals(obligacionesCtrato, that.obligacionesCtrato) &&
                Objects.equals(remuneracionCtrato, that.remuneracionCtrato) &&
                Objects.equals(observaciones, that.observaciones) &&
                Objects.equals(objetoFideicomiso, that.objetoFideicomiso) &&
                Objects.equals(consecutivoCesiones, that.consecutivoCesiones) &&
                Objects.equals(consecutivoDesistimientos, that.consecutivoDesistimientos) &&
                Objects.equals(objetoCtratoVinculacion, that.objetoCtratoVinculacion) &&
                Objects.equals(obligacioCtratoVinculacion, that.obligacioCtratoVinculacion) &&
                Objects.equals(idAuditor, that.idAuditor) &&
                Objects.equals(idOperador, that.idOperador) &&
                Objects.equals(nombreHotel, that.nombreHotel) &&
                Objects.equals(idAdmonInmobiliario, that.idAdmonInmobiliario) &&
                Objects.equals(montoAutorizacion, that.montoAutorizacion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo, nombreFideicomiso, fechaConstitucion, fechaVencimiento, fechaUltProrroga, vigenciaTotalDias, numeroPagare, fechaPagare, numeroEscritura, notaria, numeroDocumento, tipoDocConstitucion, descripcion, porceDeCubrimiento, consecutivoCertificado, consecutivoBienes, porceComisionMes, porceComisionTotal, numeroSalariosMinimos, comisionValorFijo, comisionValorInicial, periodoRendicionCuentas, consecutivoAvaluos, estadoFideicomiso, consecutivoVisitas, numeroConsecutivoExterno, numeroCarpetaArchivo, manejaContratos, nombreArchivoFirma1, nombreArchivoFirma2, nombreArchivoFirma3, nombreArchivoFirma4, responsableImpuestos, nombreArchivoWord, pathArchivoWord, usuario, fechaRegistro, consecutivoPagos, valorFideicomiso, numeroPagos, pagosMensual, vlrComisionPagos, fechaLiquidacion, generaFactura, diasVencimientoFactura, consecutivoAnexo, nroCajaArchivo, subtipoFideicomiso, copiaControlada, contabilizaAportesNegocio, contabilizaAportesSociedad, contabilizaPagosNegocio, contabilizaPagosSociedad, contabilizaFacturasNegocio, contabilizaFacturasSociedad, contabilizaRndsNegocio, contabilizaRndsSociedad, contabilizaCertNegocio, contabilizaCertSociedad, fechaUltRndCuentas, modalidadContrato, actividadEconomica, inmobiliario, sistemaCausacionCtb, consecArchRecau, consecLoteRecau, contabilizaPagosImptos, codCcostosSociedad, codCcostosNegocio, seqConfirmaPagos, calculaIca, sucNegocio, facturoFija, fechaCorteCxcFija, facturoVariable, fechaCorteCxcVariable, obligacionesCtrato, remuneracionCtrato, observaciones, objetoFideicomiso, consecutivoCesiones, consecutivoDesistimientos, objetoCtratoVinculacion, obligacioCtratoVinculacion, idAuditor, idOperador, nombreHotel, idAdmonInmobiliario, montoAutorizacion);
    }
}
