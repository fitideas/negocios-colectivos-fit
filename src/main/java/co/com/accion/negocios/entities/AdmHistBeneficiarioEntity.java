package co.com.accion.negocios.entities;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "ADM_HIST_BENEFICIARIO", schema = "ACCION", catalog = "")
@IdClass(AdmHistBeneficiarioEntityPK.class)
public class AdmHistBeneficiarioEntity {
    private Long codigoFideicomiso;
    private Long nitBeneficiario;
    private Long porceParticipa;
    private String usuario;
    private Date fechaRegistro;
    private String estadoBeneficiario;
    private Long vlrParticipa;
    private String observaciones;
    private Long idTipoBeneficiario;
    private Long idClaseBeneficio;
    private String mnjRangos;
    private Date fechaCorte;

    @Id
    @Column(name = "CODIGO_FIDEICOMISO")
    public Long getCodigoFideicomiso() {
        return codigoFideicomiso;
    }

    public void setCodigoFideicomiso(Long codigoFideicomiso) {
        this.codigoFideicomiso = codigoFideicomiso;
    }

    @Id
    @Column(name = "NIT_BENEFICIARIO")
    public Long getNitBeneficiario() {
        return nitBeneficiario;
    }

    public void setNitBeneficiario(Long nitBeneficiario) {
        this.nitBeneficiario = nitBeneficiario;
    }

    @Basic
    @Column(name = "PORCE_PARTICIPA")
    public Long getPorceParticipa() {
        return porceParticipa;
    }

    public void setPorceParticipa(Long porceParticipa) {
        this.porceParticipa = porceParticipa;
    }

    @Basic
    @Column(name = "USUARIO")
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    @Basic
    @Column(name = "FECHA_REGISTRO")
    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Basic
    @Column(name = "ESTADO_BENEFICIARIO")
    public String getEstadoBeneficiario() {
        return estadoBeneficiario;
    }

    public void setEstadoBeneficiario(String estadoBeneficiario) {
        this.estadoBeneficiario = estadoBeneficiario;
    }

    @Basic
    @Column(name = "VLR_PARTICIPA")
    public Long getVlrParticipa() {
        return vlrParticipa;
    }

    public void setVlrParticipa(Long vlrParticipa) {
        this.vlrParticipa = vlrParticipa;
    }

    @Basic
    @Column(name = "OBSERVACIONES")
    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Id
    @Column(name = "ID_TIPO_BENEFICIARIO")
    public Long getIdTipoBeneficiario() {
        return idTipoBeneficiario;
    }

    public void setIdTipoBeneficiario(Long idTipoBeneficiario) {
        this.idTipoBeneficiario = idTipoBeneficiario;
    }

    @Id
    @Column(name = "ID_CLASE_BENEFICIO")
    public Long getIdClaseBeneficio() {
        return idClaseBeneficio;
    }

    public void setIdClaseBeneficio(Long idClaseBeneficio) {
        this.idClaseBeneficio = idClaseBeneficio;
    }

    @Basic
    @Column(name = "MNJ_RANGOS")
    public String getMnjRangos() {
        return mnjRangos;
    }

    public void setMnjRangos(String mnjRangos) {
        this.mnjRangos = mnjRangos;
    }

    @Id
    @Column(name = "FECHA_CORTE")
    public Date getFechaCorte() {
        return fechaCorte;
    }

    public void setFechaCorte(Date fechaCorte) {
        this.fechaCorte = fechaCorte;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdmHistBeneficiarioEntity that = (AdmHistBeneficiarioEntity) o;
        return Objects.equals(codigoFideicomiso, that.codigoFideicomiso) &&
                Objects.equals(nitBeneficiario, that.nitBeneficiario) &&
                Objects.equals(porceParticipa, that.porceParticipa) &&
                Objects.equals(usuario, that.usuario) &&
                Objects.equals(fechaRegistro, that.fechaRegistro) &&
                Objects.equals(estadoBeneficiario, that.estadoBeneficiario) &&
                Objects.equals(vlrParticipa, that.vlrParticipa) &&
                Objects.equals(observaciones, that.observaciones) &&
                Objects.equals(idTipoBeneficiario, that.idTipoBeneficiario) &&
                Objects.equals(idClaseBeneficio, that.idClaseBeneficio) &&
                Objects.equals(mnjRangos, that.mnjRangos) &&
                Objects.equals(fechaCorte, that.fechaCorte);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigoFideicomiso, nitBeneficiario, porceParticipa, usuario, fechaRegistro, estadoBeneficiario, vlrParticipa, observaciones, idTipoBeneficiario, idClaseBeneficio, mnjRangos, fechaCorte);
    }
}
