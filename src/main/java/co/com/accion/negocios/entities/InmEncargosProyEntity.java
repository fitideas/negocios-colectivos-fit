package co.com.accion.negocios.entities;

import javax.persistence.*;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table(name = "INM_ENCARGOS_PROY", schema = "ACCION", catalog = "")
@IdClass(InmEncargosProyEntityPK.class)
public class InmEncargosProyEntity {
    private long codigoFideicomiso;
    private long codigoProyecto;
    private long codigoEtapa;
    private long idFondo;
    private long codSuc;
    private long nroEncargo;
    private String naturaleza;
    private String estado;
    private String usuario;
    private Time fechaRegistro;

    @Id
    @Column(name = "CODIGO_FIDEICOMISO")
    public long getCodigoFideicomiso() {
        return codigoFideicomiso;
    }

    public void setCodigoFideicomiso(long codigoFideicomiso) {
        this.codigoFideicomiso = codigoFideicomiso;
    }

    @Id
    @Column(name = "CODIGO_PROYECTO")
    public long getCodigoProyecto() {
        return codigoProyecto;
    }

    public void setCodigoProyecto(long codigoProyecto) {
        this.codigoProyecto = codigoProyecto;
    }

    @Id
    @Column(name = "CODIGO_ETAPA")
    public long getCodigoEtapa() {
        return codigoEtapa;
    }

    public void setCodigoEtapa(long codigoEtapa) {
        this.codigoEtapa = codigoEtapa;
    }

    @Id
    @Column(name = "ID_FONDO")
    public long getIdFondo() {
        return idFondo;
    }

    public void setIdFondo(long idFondo) {
        this.idFondo = idFondo;
    }

    @Id
    @Column(name = "COD_SUC")
    public long getCodSuc() {
        return codSuc;
    }

    public void setCodSuc(long codSuc) {
        this.codSuc = codSuc;
    }

    @Id
    @Column(name = "NRO_ENCARGO")
    public long getNroEncargo() {
        return nroEncargo;
    }

    public void setNroEncargo(long nroEncargo) {
        this.nroEncargo = nroEncargo;
    }

    @Basic
    @Column(name = "NATURALEZA")
    public String getNaturaleza() {
        return naturaleza;
    }

    public void setNaturaleza(String naturaleza) {
        this.naturaleza = naturaleza;
    }

    @Basic
    @Column(name = "ESTADO")
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Basic
    @Column(name = "USUARIO")
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    @Basic
    @Column(name = "FECHA_REGISTRO")
    public Time getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Time fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InmEncargosProyEntity that = (InmEncargosProyEntity) o;
        return codigoFideicomiso == that.codigoFideicomiso &&
                codigoProyecto == that.codigoProyecto &&
                codigoEtapa == that.codigoEtapa &&
                idFondo == that.idFondo &&
                codSuc == that.codSuc &&
                nroEncargo == that.nroEncargo &&
                Objects.equals(naturaleza, that.naturaleza) &&
                Objects.equals(estado, that.estado) &&
                Objects.equals(usuario, that.usuario) &&
                Objects.equals(fechaRegistro, that.fechaRegistro);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigoFideicomiso, codigoProyecto, codigoEtapa, idFondo, codSuc, nroEncargo, naturaleza, estado, usuario, fechaRegistro);
    }
}
