package co.com.accion.negocios.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class AdmLibroAnotacionesEntityPK implements Serializable {
    private String codigoFideicomiso;
    private long seqEvento;

    @Column(name = "CODIGO_FIDEICOMISO")
    @Id
    public String getCodigoFideicomiso() {
        return codigoFideicomiso;
    }

    public void setCodigoFideicomiso(String codigoFideicomiso) {
        this.codigoFideicomiso = codigoFideicomiso;
    }

    @Column(name = "SEQ_EVENTO")
    @Id
    public long getSeqEvento() {
        return seqEvento;
    }

    public void setSeqEvento(long seqEvento) {
        this.seqEvento = seqEvento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdmLibroAnotacionesEntityPK that = (AdmLibroAnotacionesEntityPK) o;

        if (seqEvento != that.seqEvento) return false;
        if (codigoFideicomiso != null ? !codigoFideicomiso.equals(that.codigoFideicomiso) : that.codigoFideicomiso != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = codigoFideicomiso != null ? codigoFideicomiso.hashCode() : 0;
        result = 31 * result + (int) (seqEvento ^ (seqEvento >>> 32));
        return result;
    }
}
