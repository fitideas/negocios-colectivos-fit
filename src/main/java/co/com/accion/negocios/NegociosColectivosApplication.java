package co.com.accion.negocios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {
        "co.com.accion.negocios"})
public class NegociosColectivosApplication {

    public static void main(String[] args) {
        SpringApplication.run(NegociosColectivosApplication.class, args);
    }

}
