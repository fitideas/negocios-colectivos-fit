package co.com.accion.negocios.exception;

import javassist.NotFoundException;

public class ObjectNotFoundException extends NotFoundException {
    public ObjectNotFoundException(String msg) {
        super(msg);
    }
}
