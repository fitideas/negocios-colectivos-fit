package co.com.accion.negocios.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@ApiModel(description = "Información de un beneficiario en el historial")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HistBeneficiarioDto {

    Long codigoFideicomiso;
    Long nitBeneficiario;
    Long porceParticipa;
    String usuario;
    Date fechaRegistro;
    String estadoBeneficiario;
    Long vlrParticipa;
    String observaciones;
    TipoBeneficiarioDto tipoBeneficiario;
    Long idClaseBeneficio;
    String mnjRangos;
    Date fechaCorte;

}
