package co.com.accion.negocios.dto;

public interface NroEncargo {

    String getNumeroEncargo();
    void setNumeroEncargo(String numeroEncargo);
}
