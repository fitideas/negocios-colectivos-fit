package co.com.accion.negocios.dto;

public interface TipoEventoDto {

    String getNombre();

    void setNombre(String nombre);

    String getTipoCalculoComision();

    void setTipoCalculoComision(String tipoCalculoComision);

    String getClaseEvento();

    void setClaseEvento(String claseEvento);

}
