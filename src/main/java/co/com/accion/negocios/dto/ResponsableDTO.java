package co.com.accion.negocios.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Funcionario de un negocio")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponsableDTO {

    private String codigoFideicomiso;

    private String tipoResponsable;

    private ClientesDtoFull informacion;

}
