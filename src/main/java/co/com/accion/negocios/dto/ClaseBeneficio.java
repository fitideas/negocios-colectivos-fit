package co.com.accion.negocios.dto;

public interface ClaseBeneficio {

    Long getIdClaseBeneficio();
    void setIdClaseBeneficio(Long idClaseBeneficio);

    String getDescripcion();
    void setDescripcion(String descripcion);
}
