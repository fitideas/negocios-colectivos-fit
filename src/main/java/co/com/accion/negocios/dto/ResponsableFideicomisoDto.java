package co.com.accion.negocios.dto;

public interface ResponsableFideicomisoDto {

    String getTipoResponsable();

    void setTipoResponsable(String tipoResponsable);

    long getIdResponsable();

    void setIdResponsable(long idResponsable);

    long getCodigoFideicomiso();

    void setCodigoFideicomiso(long codigoFideicomiso);
}
