package co.com.accion.negocios.dto;

import java.util.Date;

public interface ClientesBeneficiarioDto {

    String getTipoVinculacion();

    void setTipoVinculacion(String tipoVinculacion);

    String getEnviocorrespondenciaTipo();

    void setEnvioCorrespondenciaTipo(String enviocorrespondenciaTipo);

    String getNombreCompleto();

    void setNombreCompleto(String nombreCompleto);

    TipoDocumentoDto getTipoDocumento();

    void setTipoDocumento(TipoDocumentoDto tipoDocumento);

    String getRazonSocial();

    void setRazonSocial(String razonSocial);

    Date getFechaVinculacion();

    void setFechaVinculacion(Date fechaVinculacion);

    String getCiudadResidencia();

    void setCiudadResidencia(String ciudadResidencia);

    String getOficinaVinculacion();

    void setOficinaVinculacion(String oficinaVinculacion);

    String getPaisResidencia();

    void setPaisResidencia(String paisResidencia);

    String getNumeroDocumento();

    void setNumeroDocumento(String numeroDocumento);

    String getNaturalezaJuridicaVinculado();

    void setNaturalezaJuridicaVinculado(String naturalezaJuridicaVinculado);

    String getTelefono();

    void setTelefono(String telefono);

    String getCiudadVinculacion();

    void setCiudadVinculacion(String ciudadVinculacion);

    String getEmail();

    void setEmail(String email);

    double getParticipacion();

    void setParticipacion(double participacion);
}
