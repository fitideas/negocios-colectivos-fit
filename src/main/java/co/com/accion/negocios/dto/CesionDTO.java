package co.com.accion.negocios.dto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.sql.Time;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Información de una cesión y sus propietarios previo y nuevo")
public class CesionDTO {

    @ApiModelProperty(value = "Identificador del fideicomiso", dataType = "String", example = "184", required = true)
    private String idNegocio;
    @ApiModelProperty(value = "Id del contrato", dataType = "String", example = "0", required = true)
    private String idContrato;
    @ApiModelProperty(value = "Número de la cesión", dataType = "String", example = "345", required = true)
    private String nroCesion;
    @ApiModelProperty(value = "Porcentaje del cedente previo a la cesión", dataType = "double", example = "0.0", required = true)
    private double porceActualCedente;
    @ApiModelProperty(value = "Porcentaje del cedente luego de la cesión", dataType = "double", example = "0.0", required = true)
    private double porceNuevoCedente;
    @ApiModelProperty(value = "Diferencia entre el porcentaje actual y nuevo del cedente", dataType = "double", example = "0.0", required = true)
    private double diferenciaPorcentaje;
    @ApiModelProperty(value = "Fecha de registro de la cesión", dataType = "Date", example = "2009-12-21T05:00:00.000+0000", required = true)
    private Date fechaRegistro;
    @ApiModelProperty(value = "Clase del beneficio")
    private ClaseBeneficioDto claseBeneficio;
    @ApiModelProperty(value = "Tipo del beneficiario")
    private TipoBeneficiarioDto tipoBeneficiario;
    @ApiModelProperty(value = "Información completa del propietario nuevo", dataType = "ClientesDTOFull", required = true)
    private ClientesDtoFull propietarioNuevo;
    @ApiModelProperty(value = "Información completa del propietario previo", dataType = "ClientesDTOFull", required = true)
    private ClientesDtoFull propietarioPrevio;


}
