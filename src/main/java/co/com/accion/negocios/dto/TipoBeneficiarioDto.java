package co.com.accion.negocios.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

@ApiModel(description = "Tipo de beneficiario")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TipoBeneficiarioDto {
    private Long idTipoBeneficiario;
    private String descripcion;
    private String generaCertificado;
}
