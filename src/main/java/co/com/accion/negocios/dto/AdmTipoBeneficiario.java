package co.com.accion.negocios.dto;

public interface AdmTipoBeneficiario {

    Long getIdTipoBeneficiario();
    void setIdTipoBeneficiario(Long idTipoBeneficiario);

    String getDescripcion();
    void setDescripcion(String descripcion);

    String getGeneraCertificado();
    void setGeneraCertificado(String generaCertificado);
}
