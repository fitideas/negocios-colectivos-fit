package co.com.accion.negocios.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Clase de beneficio")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaseBeneficioDto {
    private Long idClaseBeneficio;
    private String descripcion;
}
