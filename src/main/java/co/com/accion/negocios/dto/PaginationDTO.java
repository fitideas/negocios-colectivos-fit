package co.com.accion.negocios.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@ApiModel(description = "Objeto para paginar")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaginationDTO {
    @ApiModelProperty(value = "Enlaces a las páginas previa y siguiente (si estas existen)", dataType = "HypermediaLinkDTO", required = true)
    private HypermediaLinkDTO _links;
    @ApiModelProperty(value = "Lista de objetos que componen la respuesta", dataType = "List<Object>", required = true)
    private List<Object> results;
    @ApiModelProperty(value = "Número de elementos en la página", dataType = "int", example = "2", required = true)
    private int size;
    @ApiModelProperty(value = "Número de páginas totales", dataType = "int", example = "2", required = true)
    private int totalPages;
    @ApiModelProperty(value = "Número de la página actual", dataType = "int", example = "1", required = true)
    private int start;
    @ApiModelProperty(value = "Número máximo de resultados en la página", dataType = "int", example = "5", required = true)
    private int limit;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class HypermediaLinkDTO {
        private String base;
        private String next;
        private String prev;
        private String self;
    }
}
