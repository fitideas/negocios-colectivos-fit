package co.com.accion.negocios.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public interface EncargosDto {

    String getNumeroEncargo();

    void setNumeroEncargo(String numeroEncargo);

    String getNombreFidecomiso();

    void setNombreFidecomiso(String nombreFidecomiso);

    String getEstado();

    void setEstado(String estado);

}
