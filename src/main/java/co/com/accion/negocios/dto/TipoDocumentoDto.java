package co.com.accion.negocios.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

public interface TipoDocumentoDto {

    @ApiModelProperty(value = "Identificador del tipo de documento", dataType = "String", example = "2", required = true)
    Long getId();

    void setId(String id);

    @ApiModelProperty(value = "Descripción del tipo de documento", dataType = "String", example = "NIT", required = true)
    String getDescripcion();

    void setDescripcion(String descripcion);
}
