package co.com.accion.negocios.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class KVDTO {
    @ApiModelProperty(value = "Identificador del estado", dataType = "String", example = "V", required = true)
    private String id;
    @ApiModelProperty(value = "Descripción del estado", dataType = "String", example = "VIGENTE", required = true)
    private String descripcion;
}
