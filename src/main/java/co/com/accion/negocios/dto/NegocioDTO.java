package co.com.accion.negocios.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@ApiModel(description = "Información básica de un negocio")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NegocioDTO {
    @ApiModelProperty(value = "Identificador numérico del negocio", dataType = "String", example = "74165", required = true)
    private String sfc;
    @ApiModelProperty(value = "Código del negocio", dataType = "String", example = "", required = true)
    private String codigo;
    @ApiModelProperty(value = "Descripción del estado del negocio", dataType = "EstadoNegocio", required = true)
    private EstadoNegocio estadoNegocio;
    @ApiModelProperty(value = "Porcentaje de derechos sobre el negocio", example = "0.0", dataType = "double")
    private Double totalDerechos;
    @ApiModelProperty(value = "Nombre del fideicomiso", example = "FA-4538 FIDEICOMISO CENTRO INDUSTRIAL CROACIA", dataType = "String", required = true)
    private String nombre;

    @Data
    @ApiModel(description = "Describe el estado de un negocio")
    public static class EstadoNegocio extends KVDTO {


        public EstadoNegocio(String codigo, String estadoFideicomiso) {
            super(codigo, estadoFideicomiso);
        }

        @Override
        public String getDescripcion() {
            return super.getId().equals("V") ? "VIGENTE" : "LIQUIDADO";
        }
    }
}
