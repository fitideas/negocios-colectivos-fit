package co.com.accion.negocios.dto;

public interface NroSecuencia {
    String getCiudad();
    void setCiudad(String ciudad);

    String getSucursal();
    void setSucursal(String sucursal);

    String getSecuencia();
    void setSecuencia(String secuencia);
}
