package co.com.accion.negocios.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Información básica de un encargo")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EncargoDto {

    @ApiModelProperty(value = "Número del encargo", dataType = "String", example = "17", required = true)
    private String numeroEncargo;
    @ApiModelProperty(value = "Nombre del fideicomiso asociado al encargo", dataType = "String", example = "FA-549 MEJIA", required = true)
    private String nombreFidecomiso;
    @ApiModelProperty(value = "Estado del encargo", dataType = "String", example = "ACT", required = true)
    private String estado;

}
