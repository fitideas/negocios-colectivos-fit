package co.com.accion.negocios.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@ApiModel(description = "Información de un evento")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EventoDTO {

    private String codigoFideicomiso;

    private String secuencialEvento;

    private Date fechaEvento;

    private Date fechaRegistro;

    private String nombreFuncionario;

    private String nroDocumento;

    private String tipoEvento;

    private String claseEvento;

    private String tipoCalculoComision;

    private String estado;

    private String conclusiones;

}
