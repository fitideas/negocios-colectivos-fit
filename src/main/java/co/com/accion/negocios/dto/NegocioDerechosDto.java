package co.com.accion.negocios.dto;

public interface NegocioDerechosDto {

    String getNombreFideicomiso();

    void setNombreFideicomiso(String nombreFideicomiso);

    long getCodigo();

    void setCodigo(long codigo);

    String getEstadoFideicomiso();

    void setEstadoFideicomiso(String estadoFideicomiso);

    double getParticipacion();

    void setParticipacion(double participacion);
}
