package co.com.accion.negocios.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel(description = "Información completa de un cliente")
public class ClientesDtoFull {

    @ApiModelProperty(value = "Tipo de vinculación del cliente", dataType = "String", example = "Beneficiario", required = true)
    private String tipoVinculacion;
    @ApiModelProperty(value = "Tipo de envío de correspondencia", dataType = "String", example = "1", required = true)
    private String envioCorrespondenciaTipo;
    @ApiModelProperty(value = "Nombre completo del cliente", dataType = "String", example = "ANDRES ORTIZ GOMEZ", required = true)
    private String nombreCompleto;
    @ApiModelProperty(value = "Tipo del documento del cliente", dataType = "TipoDocumentoDto", required = true)
    private TipoDocumentoDto tipoDocumento;
    @ApiModelProperty(value = "Razon social del cliente", dataType = "String", example = "A ESCOBAR U Y CIA S EN C")
    private String razonSocial;
    @ApiModelProperty(value = "Fecha de vinculacion del cliente", dataType = "Date", example = "2011-05-03T05:00:00.000+0000")
    private Date fechaVinculacion;
    @ApiModelProperty(value = "Ciudad de residencia del cliente", dataType = "String", example = "BOGOTA")
    private String ciudadResidencia;
    @ApiModelProperty(value = "Oficina de vinculación del cliente", dataType = "String", example = "OFICINA CALLE 93")
    private String oficinaVinculacion;
    @ApiModelProperty(value = "País de residencia del cliente", dataType = "String", example = "COLOMBIA")
    private String paisResidencia;
    @ApiModelProperty(value = "Número de documento del cliente", dataType = "String", example = "70057176", required = true)
    private String numeroDocumento;
    @ApiModelProperty(value = "Naturaleza jurídica del cliente", dataType = "String", example = "NATURAL", required = true)
    private String naturalezaJuridicaVinculado;
    @ApiModelProperty(value = "Número de teléfono del cliente", dataType = "String", example = "0")
    private String telefono;
    @ApiModelProperty(value = "Ciudad de vinculación del cliente", dataType = "String", example = "ARMENIA")
    private String ciudadVinculacion;
    @ApiModelProperty(value = "Dirección de email del cliente", dataType = "String", example = "JULIOGO@VALORCONSA.COM")
    private String email;
    @ApiModelProperty(value = "Envio Correspondencia", dataType = "String", example = "JULIOGO@VALORCONSA.COM")
    private String envioCorrespondencia;

    public String getEnvioCorrespondencia() {
        return envioCorrespondencia;
    }

    public void setEnvioCorrespondencia(String envioCorrespondencia) {
        this.envioCorrespondencia = envioCorrespondencia;
    }

    private double participacion;

    public String getTipoVinculacion() {
        return tipoVinculacion;
    }

    public void setTipoVinculacion(String tipoVinculacion) {
        this.tipoVinculacion = tipoVinculacion;
    }

    public String getEnviocorrespondenciaTipo() {
        return envioCorrespondenciaTipo;
    }

    public void setEnvioCorrespondenciaTipo(String enviocorrespondenciaTipo) {
        this.envioCorrespondenciaTipo = enviocorrespondenciaTipo;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public TipoDocumentoDto getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumentoDto tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public Date getFechaVinculacion() {
        return fechaVinculacion;
    }

    public void setFechaVinculacion(Date fechaVinculacion) {
        this.fechaVinculacion = fechaVinculacion;
    }

    public String getCiudadResidencia() {
        return ciudadResidencia;
    }

    public void setCiudadResidencia(String ciudadResidencia) {
        this.ciudadResidencia = ciudadResidencia;
    }

    public String getOficinaVinculacion() {
        return oficinaVinculacion;
    }

    public void setOficinaVinculacion(String oficinaVinculacion) {
        this.oficinaVinculacion = oficinaVinculacion;
    }

    public String getPaisResidencia() {
        return paisResidencia;
    }

    public void setPaisResidencia(String paisResidencia) {
        this.paisResidencia = paisResidencia;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getNaturalezaJuridicaVinculado() {
        return naturalezaJuridicaVinculado;
    }

    public void setNaturalezaJuridicaVinculado(String naturalezaJuridicaVinculado) {
        this.naturalezaJuridicaVinculado = naturalezaJuridicaVinculado;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCiudadVinculacion() {
        return ciudadVinculacion;
    }

    public void setCiudadVinculacion(String ciudadVinculacion) {
        this.ciudadVinculacion = ciudadVinculacion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getParticipacion() {
        return participacion;
    }

    public void setParticipacion(double participacion) {
        this.participacion = participacion;
    }
}
