package co.com.accion.negocios.dto;

public class ResponseMsg {
    private String code;
    private String message;
    private String description;

    public ResponseMsg(String code, String msg, String description){
        this.message = msg;
        this.code = code;
        this.description = description;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
