package co.com.accion.negocios.dto;

public interface BeneficiarioDto {

    long getCodigoFideicomiso();

    void setCodigoFideicomiso(long codigoFideicomiso);

    String getNombreFideicomiso();

    void setNombreFideicomiso(String nombreFideicomiso);

    long getNumeroDocumento();

    void setNumeroDocumento(long numeroDocumento);

}
