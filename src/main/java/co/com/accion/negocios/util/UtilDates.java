package co.com.accion.negocios.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UtilDates {
    public static Date formatStringToDate(String ddMMyyyyDate) throws ParseException {
        return new SimpleDateFormat("dd/MM/yyyy").parse(ddMMyyyyDate);
    }
    public static Date formatStringToDateWithScore(String ddMMyyyyDate) throws ParseException {
        return new SimpleDateFormat("dd-MM-yyyy").parse(ddMMyyyyDate);
    }
    public static String dateToSQLString(Date date) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        String strDate = dateFormat.format(date);
        return strDate;
    }

    public static String dateToCustomFormat(Date date, String format) {
        return new SimpleDateFormat(format).format(date);
    }
}
