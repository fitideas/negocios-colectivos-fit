package co.com.accion.negocios.util;

import co.com.accion.negocios.dto.ClientesBeneficiarioDto;
import co.com.accion.negocios.dto.ClientesDto;
import co.com.accion.negocios.dto.ClientesDtoFull;

public class ClientsDtoToFullConverter {

    public static ClientesDtoFull dtoToFull(ClientesDto cliente, String email) {
        ClientesDtoFull clientesFull = new ClientesDtoFull();
        clientesFull.setTipoVinculacion(cliente.getTipoVinculacion());
        clientesFull.setEnvioCorrespondenciaTipo(cliente.getEnviocorrespondenciaTipo().equals("0") ? " " : cliente.getEnviocorrespondenciaTipo().equals("1") ? "Direccion fisica" : "Correo Electronico");
        clientesFull.setNombreCompleto(cliente.getNombreCompleto());
        clientesFull.setRazonSocial(cliente.getRazonSocial());
        clientesFull.setFechaVinculacion(cliente.getFechaVinculacion());
        clientesFull.setCiudadResidencia(cliente.getCiudadResidencia());
        clientesFull.setOficinaVinculacion(cliente.getOficinaVinculacion());
        clientesFull.setPaisResidencia(cliente.getPaisResidencia());
        clientesFull.setNumeroDocumento(cliente.getNumeroDocumento());
        clientesFull.setNaturalezaJuridicaVinculado(cliente.getNaturalezaJuridicaVinculado().equals("NATURAL")  ? cliente.getNaturalezaJuridicaVinculado() : "JURIDICA");
        clientesFull.setTelefono(cliente.getTelefono());
        clientesFull.setCiudadVinculacion(cliente.getCiudadVinculacion());
        clientesFull.setEnvioCorrespondencia(cliente.getEnvioCorrespondencia());
        clientesFull.setEmail(email != null ? email : " ");

        return clientesFull;
    }

    public static ClientesDtoFull beneficiarioDtoToFull(ClientesBeneficiarioDto cliente) {
        ClientesDtoFull clientesFull = new ClientesDtoFull();
        clientesFull.setTipoVinculacion(cliente.getTipoVinculacion());
        clientesFull.setEnvioCorrespondenciaTipo(cliente.getEnviocorrespondenciaTipo().equals("0") ? " " : cliente.getEnviocorrespondenciaTipo().equals("1") ? "Direccion fisica" : "Correo Electronico");
        clientesFull.setNombreCompleto(cliente.getNombreCompleto());
        clientesFull.setRazonSocial(cliente.getRazonSocial());
        clientesFull.setFechaVinculacion(cliente.getFechaVinculacion());
        clientesFull.setCiudadResidencia(cliente.getCiudadResidencia());
        clientesFull.setOficinaVinculacion(cliente.getOficinaVinculacion());
        clientesFull.setPaisResidencia(cliente.getPaisResidencia());
        clientesFull.setNumeroDocumento(cliente.getNumeroDocumento());
        clientesFull.setNaturalezaJuridicaVinculado(cliente.getNaturalezaJuridicaVinculado().equals("NATURAL")  ? cliente.getNaturalezaJuridicaVinculado() : "JURIDICA");
        clientesFull.setTelefono(cliente.getTelefono());
        clientesFull.setCiudadVinculacion(cliente.getCiudadVinculacion());
        clientesFull.setEmail(cliente.getEmail());
        clientesFull.setParticipacion(cliente.getParticipacion());
        clientesFull.setParticipacion(cliente.getParticipacion());

        return clientesFull;
    }

}
