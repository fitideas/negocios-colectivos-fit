package co.com.accion.negocios.util;

import co.com.accion.negocios.dto.PaginationDTO;
import org.springframework.data.domain.Page;

import java.util.List;

public class Pagination {

    public static PaginationDTO crearPaginationDTO(Page page, String baseURL, String[] params, String[] paramsValues, List<Object> results, long limit, long start){
        PaginationDTO paginationDTO;
        String queryParams = "";
        if(params!=null) {
            for (int i = 0; i < params.length; i++) {
                if(params[i] != null && paramsValues[i] !=null)
                    queryParams += "&" + params[i] + "=" + paramsValues[i];
            }
        }
        PaginationDTO.HypermediaLinkDTO links = new PaginationDTO.HypermediaLinkDTO(
                "",
                !page.isLast() ? baseURL + "&limit=" + limit + "&start=" +  (start+1) + queryParams : "",
                !page.isFirst() ? baseURL + "&limit=" + limit + "&start=" +  (start-1) + queryParams : "",
                ""

        );
        paginationDTO = new PaginationDTO(
                links,
                results,
                page.getNumberOfElements(),
                page.getTotalPages(),
                page.getNumber(),
                (int) limit
        );
        return paginationDTO;
    }


}
